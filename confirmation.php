<?php 

session_start();
include('booking/lib/conexion.php');  

$bookingId = $_GET['producto'];
$transactionIds =  $_GET['transid'];
$transactionTotals = $_GET['total'];
$names = $_GET['nombreproducto'];
$prices = $_GET['precioproducto'];
$categorys = $_GET['categoria'];
$quantitys = $_GET['cantidad'];
$transactionAffiliations = $_GET['websitio'];
$currencyCodes = $_GET['moneda'];


?>
<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email and Phone Contact for CancunFoodTour.com</title>
    <meta name="description" content="Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
    <meta name="Keywords" content="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
    <link rel="canonical" href="https://cancunfoodtours.com/contact-us">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/contact-us">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/brands.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.css">
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>

<body>
    <?php
        include('includes/menu.html'); 
        include('includes/query/getBooking.php');
    ?>
<!--
    <div class="container text-center body">
        <div class="row justify-content-center">
        <div class="col-12  align-self-center" style="height:  595px; background-image:  url('images/Confirmation-background.png'); background-size: inherit; background-position: center; background-repeat: no-repeat; color:white">
            <br />
            <br />
            <br />
            <br />
            <br />
            <h1><strong>CONGRATULATIONS!</strong></h1>
            <br />
            <br />
            <p>We've sent your purchase receipt <br /> to your email address, <br /> please review this confirmation carefully <br /> as it includes important and helpful <br /> information about your tour.</p>
            <br />
            <p>Best regards,</p>
            
            <span class="h1">Cancun Food Tours</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12"  style="padding-top:20px">
            <img src="images/food-tours-icon.png">
            </div>
            <div class="col-12" style="color:#7DB03D;">
                <p> Remember you can always contact us at: <br /> <a href="mailto:taco@cancunfoodtours.com" style="text-decoration:none; color:#7DB03D;"><strong>taco@cancunfoodtours.com</strong></a> </p>
            </div>

        </div>
        <div class="row">
        <div class="col-12" style="padding-top:20px;">
            <a href="https://facebook.com" style="text-decoration:none; color:#7DB03D;"><i class="fab fa-facebook-f fa-3x fa-fw"></i></a>
            <a href="https://instagram.com" style="text-decoration:none; color:#7DB03D;"><i class="fab fa-instagram fa-3x"></i></a>
            </div>
        </div>
        <div class="row">
        <div class="col-12" style="color:#7DB03D">
            <p>Have a taste of what's coming! Follow us on social media <br /> <strong>cancunfoodtours</strong></p>
            </div>
        </div>
    </div>
-->
    
    <div style="position:relative; text-align:center;color:white;max-width:650px;margin-right:auto;margin-left:auto;">
    <div class=" justify-content-center confirm-div">
        

<!--        <div class="col-12">-->
        <img src="images/Confirmation-background.png" style="width:100%;height:100%;">
        <div style="position:absolute; top:55%; left:50%; transform: translate(-50%,-50%);">
<!--            <div class="col-12 text-center" style="color:white">-->
            <br/> <br/>
            <h1 style="font-size:2rem"><strong>CONGRATULATIONS!</strong></h1>
            <?php echo('<p>Dear Mr./Mrs. '.$queryresult["name"].' '.$queryresult["lname"].'.<br/> Your reservation number is: '.$bookingId.'</p>') ?>
                <p>We've sent your purchase receipt <br /> to your email address <?php echo '('.$queryresult["email"].')' ?>, <br /> please review this confirmation carefully <br /> as it includes important and helpful <br /> information about your tour <?php echo $queryresult["tourname"]?>.</p>
<!--            </div>-->
        </div>
<!--        </div>-->
        </div>
    </div>
    <?php include('includes/footer.html'); ?>
    
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || []
		dataLayer.push({
			'transactionId': '<?php echo $transactionIds ?>',
			'transactionAffiliation': '<?php echo $transactionAffiliations ?>',
			'transactionTotal': <?php echo $transactionTotals ?>,
			'transactionTax': 0,
			'transactionShipping': 0, 
			'currencyCode' : '<?php echo $currencyCodes ?>',
			'transactionProducts': [{
			    'sku': '<?php echo $bookingId ?>', 
			    'name': '<?php echo $names ?>',
			    'category': '<?php echo $categorys ?>',
			    'price': <?php echo $prices ?>,
			    'quantity': <?php echo $quantitys ?>
			}]
		});
</script>
</body>

</html>
