<?php 

session_start();
include('lib/conexion.php');  

$idproducto = $_GET['idproducto'];

?>

<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
    <meta name="description" content="Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
    <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css?4.0">
    <script src="../js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="../css/extra.css">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--    <script src="js/extra.js"></script>-->
    
    <?php include('lib/controlscrip.php'); ?>
    <!--SCRIPTS BEGIN-->
    <script type="text/javascript">
$( document ).ready(function() {
    timer()
});
	
	    function pagopaypal(){

	    	//event.preventDefault();

	    	var fecha = $('#bookfecha').val();
	    	var tiempo = $('#tiempo').val();
	    	var pax = $('#cantidad1').val();
	    	
	    	var nombre = $('#form_fname').val();
	    	var apellido = $('#form_lname').val();
	    	var nombrehotel = $('#form_hotelname').val();
	    	var numerocuarto = $('#form_room').val();
	    	var pais = $('#form_country').val();
	    	var estado = $('#form_state').val();
	    	var direccion = $('#form_address').val();
	    	var zipcode = $('#form_postalcode').val();
	    	var correoelectronico = $('#form_email').val();
	    	var telefono = $('#form_cellphone').val();
	    	var notasextra = $('#form_message').val();
	    	var total = $('#totalpri').val();

            var productid = <?php echo $idproducto ?>

	    	/*if( !(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(correoelectronico)) ) {

				$("#errorcorreo").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorcorreo').text('Invalid Email').addClass('booking_error'); 
                return false;
                
			}else{*/

	            if(tiempo == ""){
	                $("#errorcorreo").fadeIn("slow").delay(2000).fadeOut('slow');
	                $('#errorcorreo').text('Empty Email').addClass('booking_error'); 
	                return false;
	            }
			/*}*/


            if(tiempo == "Select Time"){
                $("#errortime").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errortime').text('Time Invalid').addClass('booking_error'); 
                return false;
            }

            if (fecha == "") {

                $("#errorfecha").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorfecha').text('Date Invalid').addClass('booking_error'); 
                return false;

            }

            if (pax == "0") {

                $("#errorpax").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorpax').text('Tickets Invalid').addClass('booking_error'); 
                return false;

            }

            if (nombre == "") {

                $("#errornombre").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errornombre').text('Empty Name').addClass('booking_error'); 
                return false;
            }

            if (apellido == "") {

                $("#errorlast").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorlast').text('Empty Last Name').addClass('booking_error'); 
                return false;
            }
            
            if (nombrehotel == "") {

                $("#errornombrehotel").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errornombrehotel').text('Empty Hotel Name').addClass('booking_error'); 
                return false;
            }

            /*if( !(/^\d{9}$/.test(telefono)) ) {
  					
  					$("#errortel").fadeIn("slow").delay(2000).fadeOut('slow');
                	$('#errortel').text('Invalidate Cell Phone').addClass('booking_error'); 

  				return false;

			}else{*/

  				if (telefono == "") {

                	$("#errortel").fadeIn("slow").delay(2000).fadeOut('slow');
                	$('#errortel').text('Empty Cell Phone').addClass('booking_error'); 
                	return false;
            	}
            /*}*/


            if (estado == "") {

                $("#errorestado").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorestado').text('Empty State/Province').addClass('booking_error'); 
                return false;
            }

            if (pais == "") {

                $("#errorpais").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorpais').text('Empty Country').addClass('booking_error'); 
                return false;
            }

            /*if (true) {}*/

        ventana();

        //$.post("https://cancunfoodtours.com/booking/lib/paypal.php", {"fecha":fecha, "tiempo":tiempo, "pax":pax, "nombre":nombre, "apellido":apellido, "nombrehotel":nombrehotel, "numerocuarto":numerocuarto, "pais":pais, "estado":estado, "direccion":direccion, "zipcode":zipcode, "correoelectronico":correoelectronico, "telefono":telefono, "notasextra":notasextra, "total":total }, function(data){ 
            $.post("lib/paypal.php", {"fecha":fecha, "tiempo":tiempo, "pax":pax, "nombre":nombre, "apellido":apellido, "nombrehotel":nombrehotel, "numerocuarto":numerocuarto, "pais":pais, "estado":estado, "direccion":direccion, "zipcode":zipcode, "correoelectronico":correoelectronico, "telefono":telefono, "notasextra":notasextra, "total":total, "productid":productid}, function(data){ 
        	$(location).attr('href',data); 
        });

    }   

	function controltotal(){
		
		var paxtotal = $('#cantidad1').val();
		var precio1 = $('#precio1').val();	

		var multiplica = paxtotal*precio1;
		
		console.log(paxtotal);
		console.log(precio1);
		//console.log(multiplica);

		$('#totalsuma').text(multiplica);
		$('#adulto').text(paxtotal);
		//$('#totalprice').text(multiplica);
		$('#totalpri').val(multiplica);



	}

    function ventana(){

        $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 

    }

</script>
    <!--SCRIPTS END-->
    
</head>

<body>
    <?php include('../includes/menu_booking.html');  ?>
    <div class="container text-center body">
        <div class="title_booking" style="border-bottom:3px solid #E51C5F;">
        <h1><strong>Tour Booking</strong></h1>
        </div>
        <form id="contact-form" method="POST" role="form">
            <div class="messages"></div>
            <div class="controls">
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-8">
                        <div class="row">
                        <div class="col-sm-6  order-lg-1">
                            <p style="color:#EA5B2D"><strong>Tickets:</strong></p>
                            <div class="form-group">

                                <?php 

							$result = mysql_query("SELECT * FROM tikect WHERE idproducto = $idproducto"); 
							while($row= mysql_fetch_array($result)){

								$cantidad = $row['numero'];
								$idtick = $row['idtikect'];
								$nombre = $row['nombre'];
								$precio = $row['precio'];
								$precio = number_format($precio, 2, '.', '');

							echo ' 
								<select name="numero'.$idtick.'" class="form-control"  id="cantidad1" onclick="controltotal()" onchange="controltotal()" required>';
								echo "<option value=''>Select ticket</option>";
								for ($i=1; $i <= $cantidad ; $i++) { 
									echo "<option value='".$i."'>".$i."</option>";
								}
								echo "</select>";
								echo '<input type="hidden" value="'.$precio.'" id="precio1">';
							}
						?>
                            </div>
                        </div>
                        <div class="col-sm-6  order-lg-2">
                            <p style="color:#EA5B2D"><strong>Select Date:</strong></p>
                            <div class="form-group">
                                <input id="datepicker" type="text" name="date" class="form-control" required="required" data-error="Date is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div style="visibility:hidden;position:fixed;">
                            <p style="color:#EA5B2D"><strong>Price:</strong></p>
                            <?php echo '<input id="price" type="text" name="aprice" class="form-control" placeholder="' .$precio.'" disabled="disabled">'; ?>
                            <select name="tiempo" class="form-control" style="display:none;" id="tiempo" onclick="timer()" onchange="timer()" required>
                                        <?php 
                                            $controltiempo = mysql_query("SELECT * FROM tiempo  WHERE n1 = $idproducto ");
                                            while($tiempo = mysql_fetch_array($controltiempo)){
                                                $malo = $tiempo['hora'];

                                                echo "<option select>".$malo."</option>";
                                            }
                                        ?>
                                    </select>
                        </div>
                        </div>
                        <hr />
                        
                        <div class="row">
                        <div class="col-sm-12 col-lg-6 order-lg-4">
                            <div class="form-group">
                                <input id="form_fname" type="text" name="fname" class="form-control" placeholder="First Name *" required data-error="First Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 order-lg-4">
                            <div class="form-group">
                                <input id="form_lname" type="text" name="lname" class="form-control" placeholder="Last Name *" required data-error="Last Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-lg-6 order-lg-5">
                            <div class="form-group">
                                <input id="form_hotelname" type="text" name="hotelname" class="form-control" placeholder="Hotel Name *" required data-error="Hotel Name is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-lg-6 order-lg-5">
                            <div class="form-group">
                                <input id="form_room" type="text" name="room" class="form-control" placeholder="Room No.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-6 order-lg-6">
                            <div class="form-group">
                                <input id="form_country" type="text" name="country" class="form-control" placeholder="Country *" required data-error="Country is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                            <div class="col-sm-12 col-lg-6 order-lg-6">
                            <div class="form-group">
                                <input id="form_state" type="text" name="state" class="form-control" placeholder="State/Province *"  required data-error="State is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 order-lg-7">
                            <div class="form-group">
                                <input id="form_address" type="text" name="address" class="form-control" placeholder="Address">
                            </div>
                        </div>
                            <div class="col-sm-12 col-lg-6 order-lg-7">
                            <div class="form-group">
                                <input id="form_postalcode" type="text" name="postalcode" class="form-control" placeholder="Zip / Postal Code">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 order-lg-8">
                            <div class="form-group">
                                <input id="form_email" type="email" name="email" class="form-control" placeholder="Email *" required data-error="Email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6 order-lg-8">
                            <div class="form-group">
                                <input id="form_cellphone" type="tel" name="cellphone" class="form-control" placeholder="Cellphone *" required data-error="Cellphone Number is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-12 order-lg-9">
                            <div class="form-group">
                                <textarea id="form_message" name="message" class="form-control" placeholder="If you have any special requests or dietary restrictions please type them here." rows="5" style="height:140px"  data-error="Please, leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4">
        <p style="color:black; font-size:1.26rem;"><strong> Payment Summary</strong></p>
                        <?php 

						$controlproduct = mysql_query("SELECT * FROM producto WHERE idproducto = $idproducto");
						while ($producto = mysql_fetch_array($controlproduct)) {
							# code...
							$nombre = $producto['nombre'];
                            $imgsrc = $producto['imgSrc'];
                            $comment = $producto['comment'];
						}

					  ?>
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="row justify-content-center">
                                    <div class="card">
                                        <img class="card-img-top" src='<?php echo $imgsrc;?>' alt="Tour imagen">
                                        <div class="card-title" style="background-color: #69358A; margin-bottom:0px;">
                                            <div class="row justify-content-center">
                                                <h5 class="card-title" style="color:white; margin:.5rem 0rem;text-align:center;font-size:1rem;"><strong><?php echo $nombre; ?></strong></h5>
                                            </div>
                                        </div>
                                        <div class="card-body" style="border-bottom: solid 2px #F8B133; padding:0 1.25rem;">
                                            <div class="row justify-content-center">
                                                <p> <?php echo $comment;?></p>
                                            </div>
                                        </div>
                                        <div class="card-body" style="padding: 0 1.25rem;">
                                            <div class="row justify-content-center">
                                                <p class="card-text">Payment Summary</p>
                                                <div class="row" style="width:100%;">
                                                    <div class="col-sm-4" style="padding:0px">
                                                        <p class="card-text">Date: </p>
                                                        <span><strong id="fecha">MM/DD/YYYY</strong></span>
                                                    </div>
                                                    <div class="col-sm-4" style="padding:0px">
                                                        <p class="card-text">Time: </p>
                                                        <span><strong id="vistatiempo"><?php echo $malo; ?></strong></span></div>
                                                    <div class="col-sm-4" style="padding:0px">
                                                        <p class="card-text">Adult: </p>
                                                        <span><strong id="adulto">0</strong><strong>x Adult</strong></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <p class="card-text price"><strong>TOTAL: $</strong><strong id="totalsuma">00.00</strong><strong>  USD</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        
                    <input type="hidden" name="totalpri" id="totalpri">
                    <input type="hidden" name="bookfecha" id="bookfecha">
                        <button type="submit" class="btn btn-success btn-send" onfocus="pagopaypal();" onClick="javascript:pagopaypal();"  value="Proceed to Payment"> PROCEED TO PAYMENT </button> 
<!--                        <input type="submit" class="btn btn-success btn-send" value="PROCEED TO PAYMENT">-->
                    </div>


                </div>
            </div>
        </form>

    </div>
    <?php include('../includes/footer.html'); ?>
    
<script src="https://cancunfoodtours.com/booking/lib/js/block.js"></script>
</body>

</html>
