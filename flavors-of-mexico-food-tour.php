<?php
   session_start();
   
  
	
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
    <meta name="description" content="Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
    <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">

    <link rel="canonical" href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">
    <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css?ver=1.0">
    <link rel="stylesheet" type="text/css" href="./css/new_style.min.css?ver=1">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <?php 
        include('includes/tagmanager.php'); 
        include('includes/schemas/productSchemaMexFlavours.php');
        ?>

</head>

<body>
    
    <?php include('includes/menu.html');  ?>
    <div class="aux-pb-5"></div>
    <div class="body-background">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-8 col-md-8-fixed">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-tostada.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-pescadillas.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-mezcal.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-panucho.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-papadzul.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-xtabentun.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-tacos.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-guacamole.jpg" alt="Third slide">
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
                    
        <div class="container-fluid visible-sm" style="display:none;">
            <div class="highlight pt-2">
                <div class="col-12">
                    <h1 class="single-tour-h1">Flavors of Mexico Food Tour</h1>
                    <p><strong>Duration:</strong> 3.5-4.5 hrs</p>
                    <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                    <div class="row text-center pt-3 mb-3">
                        <div class="col-4">
                            <span class="labels">ADULT</span>
                            <br/>
                            <h3 class="mb-0" style="font-size:1.3rem;">$79 USD</h3>
                            <p class="text-center currency"></p>
                        </div>
<!--
                        <div class="col-4">
                            <span class="labels">Adult W/Alcohol</span>
                            <br/>
                            <h3 class="mb-0">$80</h3>
                            <p class="text-center currency"></p>
                        </div>
-->
                        <div class="col-4">
                            <span class="labels">WHEN</span>
                            <br />
                            <h3 class="mb-0" style="font-size:1.3rem;">Wednesday &amp; Saturday</h3>
<!--                            <p class="text-center currency">Age 12-</p>-->
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <p class="additional-pricing-info text-center">
                                <small>Tickets must be purchased before 10am for the selected date.<br/><br/> **Round trip transportation included from any Cancun Hotel or Airbnb.</small>
                            </p>
                        </div>
                    </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/1" class="btn btn-primary">BOOK NOW</a>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
            <div class="section text-left">
        <div class="row">
            <div class="col-lg-12 text-left">
                <div class="mb-3 tour-stops">
            <h2>Discover Cancun’s Downtown hottest local restaurants as you taste 7 traditional Mexican dishes at 4 different spots full of history.</h2>
                <ul class="pl-20_bullets">
                    <li>Are you ready to try what real Mexican food tastes like? Take a stroll through 4 spots locals go every day.</li>
                    <li>Our partnerships will get you an insider’s look at Cancun’s most traditional places that defined the city’s gastronomy.</li>
                    <li>Cancun is more than just beaches, discover downtown’s history and gems that not all visitors are able to explore.</li>
                    <li>This experience is perfect for foodies or people who are undecided on where to go &amp; are eager to have a genuine foodie discovery.</li>
                    <li>On this tour, you will try 3 different refreshing alcoholic beverages that pair perfectly with the food at each spot.</li>
                    <li>Hear from a local’s point of view how Downtown Cancun changed from the early days &amp; how it transformed into a more visited place.</li>
                </ul>
            </div>
                <hr>
                <div class="row whatknow">
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-hourglass fa-fw"></i><div class="icon-content" ><p>Tour times and availability vary per tour. Please ask us if you need a date opened.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-umbrella fa-fw"></i><div class="icon-content" ><p>Experiences happen rain or shine. Umbrellas are offered by our guides.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-users fa-fw"></i><div class="icon-content" ><p>Small groups of no more than 10 guests per guide.</p></div>                    
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-flag fa-fw"></i><div class="icon-content" ><p>Vegetarian substitutions are available. Please notify us of serious allergies.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-check-circle fa-fw"></i><div class="icon-content" ><p>Restrooms are available during the experience.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-heartbeat fa-fw"></i><div class="icon-content" ><p>Get exclusive discounts to the best places to eat in Cancun.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                <hr>
            <div class="section text-left">
                <div class="mb-3 tour-stops">
                    <h2>What to expect from the Flavors of Mexico Food Tour</h2>
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/what_to_expect_flavors.jpg">
                    <p>Indulge in a delicious selection of 7 local dishes and 3 refreshing alcoholic drinks at restaurants locals love to go in Cancun. In the early years, Downtown Cancun’s easy access to local markets full of fresh meats, ingredients, condiments and fruits made it a target for local restaurants owners with knowledge from secrets and recipes from other parts of Mexico to open their doors.</p>
                    <p>Downtown Cancun has been transformed in several ways in the last few years. After struggling a bit earlier to attract tourism and even locals, Cancun’s central neighborhood has been repurposed with a new plan that included the opening of several hotels, hostels and restaurants that reactivated the once glorious activity of the beginnings of Cancun. When taking our food tour, you will be tasting a piece of Cancun’s history &amp; culture on each bite &amp; sip. Our tour starts with a comfortable hotel pick up, you will be greeted by our guide who will be the one in charge of sharing all the details &amp; information each dish, drink and every restaurant on the tour.</p>
                </div>
                <div class="mb-3 tour-stops">
                <h2>Discover the best recipes from restaurants that Cancun locals have continued to love for the last 40 years</h2>
                    </div>
                <div class="row location_gallery grid-third">
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors_thumb_1.jpg">
                        <h4>Delicious Caribbean seafood</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors_thumb_2.jpg">
                        <h4>Food made by Mayan ancestors</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors_thumb_3.jpg">
                        <h4>Traditional Mexico City tacos</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors_thumb_4.jpg">
                        <h4>Guacamole, pico de gallo etc.</h4>
                    </div>
<!--
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors_thumb_1.jpg">
                        <h4>Dessert at a local park</h4>
                    </div>
-->
                </div>
<!--
                <div class="locations mb-1">
                    <h4>Do-Rite Donuts</h4>
                    <p>Chef &amp; Co-owner, Francisco Brennan, uses his Michelin restaurant experience to create uniquely flavored, artisanal donuts in one of America's favorite donut shops.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Bonci Pizza</h4>
                    <p>Rome-based Italian import selling scissor-cut slices featuring distinctive toppings, sold by weight.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Kuma's Corner [Not on Fri-Sat Evening Tours]</h4>
                    <p>Owners Mike and Rick combined three of their favorite things: burgers, beer and metal. Their ethos is simple: Support Your Community. Eat Beef. Band Your Head.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Nonna's/Formento's</h4>
                    <p>Owner John Ross and Executive Chef Todd Stein bring a sense of warmth, family and unforgettable food that is reminiscent of Sunday family dinners at Ross' grandma's house.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Cemitas Puebla</h4>
                    <p>Family owned restaurant specializing in traditional Poblano foods, using recipes that can be traced back to Tony Anteliz’s family in Puebla, Mexico</p>
                </div>
-->
            </div>
            <hr/>
            </div>
                <div class="col-md-4 hidden-sm" style="z-index:9999;position:relative">
            <div id="sidebar-fx" class="position-fixed-sd fixed-sidebar affix-bottom" data-spy="affix" data-offset-top="160" data-offset-bottom="600">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="highlight bordered short">
                            <div class="col-md-12">
                                <h1 class="single-tour-h1">Flavors of Mexico Food Tour</h1>
                                <p><strong>Duration:</strong> 3.5-4.5 hrs </p>
                                <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                                <div class="row text-center pt-3 mb-3">
                                    <div class="col-md-4">
                                        <span class="labels">ADULT</span>
                                        <br/>
                                        <h3 class="mb-0" style="font-size:1.3rem;">$79 USD</h3>
                                        <p class="text-center currency"></p>
                                    </div>
<!--
                                    <div class="col-md-4">
                                        <span class="labels">Adult W/Alcohol</span>
                                        <br/>
                                        <h3 class="mb-0">$80</h3>
                                        <p class="text-center currency"></p>
                                    </div>
-->
                                    <div class="col-4">
                                        <span class="labels">WHEN</span>
                                        <br />
                                        <h3 class="mb-0" style="font-size:1.3rem;">Wednesday &amp; Saturday</h3>
<!--                                        <p class="text-center currency">Age 12-</p>-->
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-12">
                                        <p class="additional-pricing-info">
                                            <small>Tickets must be purchased by 11am for today's tour(s).<br/><br/> ** Round trip transportation included from any Cancun Hotel or Airbnb</small>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/1" class="btn btn-primary">BOOK NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div id="testimonial" class="highlight testimonial bordered">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-8 col-md-10 col-sm-push-4 col-md-push-2 col-sm-height col-sm-middle order-sm-2">
                            <div class="quote">
                                <blockquote class="homepage-lead mb-2">
                                    <p>All I can say is that this fun evening of food and drink was the highlight of my Cancun visit. These guys are friendly, prepared and professional. They took care of everything. All you need to do is bring your hearty appetite because you will leave full :).</p>
                                </blockquote>
                                <span class="labels">Explorer751521</span>
                            </div>
                        </div>
                        <div class="col-4 col-sm-4 col-md-2 col-sm-pull-8 col-md-pull-10 col-sm-height col-sm-middle order-sm-1" style="padding-top:4%;">
                            <img class="img-responsive border-radius" src="https://cancunfoodtours.com/img/Flavors/profile_1.jpg" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('includes/footer.html'); ?>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js?4.0"></script>
    <script src="./js/fixed.min.js?ver=1"></script>
    </body>
</html>