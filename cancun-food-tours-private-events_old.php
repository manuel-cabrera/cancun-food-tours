<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
        <meta name="description" content= "Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
        <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">
        
        <link rel="canonical" href="https://cancunfoodtours.com/">
        <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
        <script src="./js/bootstrap.min.js?4.0"></script>
        <link rel="stylesheet" type="text/css" href="./css/extra.css">
<!--        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/flexslider.min.css">-->
        <link rel="stylesheet" type="text/css" href="css/flexslider.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider-min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="js/extra.js"></script>
    </head>
    <body>
        <?php include('includes/menu.html');  ?>
        <div class="container text-center body">
            <br />
            <h1><strong>Private Groups &amp; Events</strong></h1>
            <br />
            
            <div class="row">
            <div class="col-lg-7 col-md-7 img-mobile"  style="align-self:center; ">
                <img src="https://cancunfoodtours.com/img/cancun-food-tours-private-events.jpg" class="img-responsive">
                </div>
                <div class="col-lg-5 col-md-5">
                <br />
                    <p class="h2" style="color:#EA5B2D"><strong>Benefits:</strong></p>
                    <br />
                    <ul class="list">
                    <li>
                        <strong>Private Group Tours</strong> are perfect for team building functions, corporate outings, networking events and happy hour socials.
                        </li>
                        <br />
                        <li>
                        <strong>Accommodate</strong> allergies, aversions and dietary restrictions of your group.
                        </li>
                        <br />
                        <li>
                        <strong>Incorporate additional food and drink</strong> pairings to maximize the fun.
                        </li>
                        <br />
                        <li>
                        <strong>Custom start and end times</strong> outside of normal hours.
                        </li>
                    </ul>
                    
                </div>
            </div>
            <br />
            <p class="h2" style="color:#EA5B2D"><strong>Private Events and Groups</strong></p>
                <br />
            <div class="row">
                <div class="col-lg-6 col-md-6">
                <p>
                    Mexico is a country which is very rich in terms of culture. Food is one of the best &amp; most exciting ways of getting to know Mexico since most of our traditions &amp; ways of life are strictly related to it.
                </p>
                <p>
                If you wish to get to know Mexican culture from a deeper perspective we can help you set a tour up that will cater for your specific needs. Private Transportation, tour guide and other services are at your disposal to make your experience something to remember. Food Tours are a great
                </p>
                </div>
                <div class="col-lg-6 col-md-6">
                <p>
                    experience that are good for celebrations, family bonding, company outings, incentives or maybe just the idea of sharing and enjoying the love for Mexican food is enough.
                </p>
                <p>
                Whatever the reason, contact us to quote and we will accommodate you. It doesn’t matter if you’re a group from 11 – 100, don’t hesitate in contacting us.
                </p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a style="background: #7DB03D;padding: 7px 24px;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;border: 0px solid #e20808;color:white !important;">INFO &amp; QUESTIONS CALL +1 800 481 4603</a><br><br><span style="color:#7DB03D">or fill this contact below:</span></div>
            <form id="contact-form" method="post" action="contact.php" role="form">

    <div class="messages"></div>

    <div class="controls">

        <div class="row">
            <div class="col-md-6">
                
            <p class="h2" style="color:#EA5B2D"><strong>Contact:</strong></p>
                <div class="form-group">
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Name *" required="required" data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            <div class="form-group">
                    <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Last Name *" required="required" data-error="Lastname is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input id="form_company" type="text" name="company" class="form-control" placeholder="Company *" required="required" data-error="Company is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Phone Number *" required="required" data-error="Phone Number is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <p class="h2" style="color:#EA5B2D"><strong>Date desired:</strong></p>
                 <div class="form-group">
                    <input id="form_date" type="text" name="date" class="form-control" placeholder="MM/DD/YY *" required="required" data-error="Date is required.">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
<!--
                    <select id="form_need" name="need" class="form-control" required="required" data-error="Please specify your need.">
                        <option value=""></option>
                        <option value="Request quotation">Request quotation</option>
                        <option value="Request order status">Request order status</option>
                        <option value="Request copy of an invoice">Request copy of an invoice</option>
                        <option value="Other">Other</option>
                    </select>
--><div class="form-group">
                    <input id="form_attendees" type="text" name="attendees" class="form-control" placeholder="Number of attendees *" required="required" data-error="Number of attendees is required.">
                    <div class="help-block with-errors"></div>
                </div>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="5" style="height:140px" required="required" data-error="Please, leave us a message."></textarea>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="SUBMIT">
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                
            </div>
        </div>
        <div class="row">
            
            
        </div>
    </div>

</form>
<!--
                <form action="https://cancunfoodtours.com/mailer.php" method="POST" id="contactus" style="display:inline">
            <div class="row">
            <div class="col-lg-6 col-md-6">
				   	<span class="title"></span>
				   	<input type="text" name="nombre" placeholder="Name" class="form-control" title="Introduce tu nombre porfavor" maxlength="50" required="" style=" margin-bottom: 10px;height: 26px;">
				   	<input type="text" name="lastnombre" placeholder="LastName" class="form-control" title="Last Name" maxlength="50" required="" style=" margin-bottom: 10px;height: 26px;">
				   	<input type="email" name="email" placeholder="Email" class="form-control" title="Introduce tu correo electronico" required="" style="margin-top: 19px; margin-bottom: 10px;height: 26px;">
				   	<input type="text" name="Company" placeholder="Company" class="form-control" title="Company" maxlength="50" required="" style=" margin-bottom: 10px;height: 26px;">
				   	<input type="text" name="Phone" placeholder="Phone Number" class="form-control" title="Phone Number" maxlength="50" required="" style=" margin-bottom: 10px;height: 26px;">
				   	
                </div>
                <div class="col-lg-6 col-md-6">
                    <label>Date Desired</label>
				   	<input type="date" name="date" placeholder="dd/mm/aaaa" class="form-control" title="date" style=" margin-bottom: 10px;height: 26px;">
				   	<input type="text" name="numbernose" placeholder="Number if Attendees" class="form-control" title="Number if Attendees" maxlength="50" required="" style=" margin-bottom: 10px;height: 26px;">
				   	<textarea placeholder="(Please provide start time preference, tour route desired, nature of event, and any other pertinent details we should know)" class="form-control"></textarea>
				   	<button class="btn btn-primary" name="mysubmit" type="submit" style="padding: 8px;margin-bottom: 17px;font-size: 17px;margin-top: 11px;background-color: #cc3300;border-color: #cc3300;font-family: 'Roboto', sans-serif;">Submit</button>
                </div>
            </div>
			    </form>
-->
          </div>
        <?php include('includes/footer.html'); ?>
    </body>
</html>