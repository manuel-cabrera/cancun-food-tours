<div class="todocont" >
	<div class="banner1" >
		<div id="main" role="main">
	      	      <section class="slider" style="background: #f3eedb;height: 731px;">
	        <div id="slider" class="flexslider" style="height: 314px;">
	          <ul class="slides">
	          	  	    	<li>
	  	    	    <img src="https://cancunfoodtours.com/img/banner-1.jpg" alt="food tours in cancun" title="food tours in cancun" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Welcome to Cancun Food Tours</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">In 4 hours or less, taste the best food and drink of Cancun as one our local food experts will offer you a unique perspective of the city while you enjoy a journey through Mexican Popular Culture, Folklore & Tradition. Click below to find your experience.</p>
	  	    	    			<!-- <div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div> -->
	  	    	    		</div> 
	  	    	    	</div>
					</div>
	  	    	</li>
	          	<li>
	  	    	    <img src="https://cancunfoodtours.com/img/salsa.jpg" alt="food tours in cancun" title="food tours in cancun" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Discover Flavors of Mexico in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">North, South, Center. No Food is the same in Mexico, recipes and ingredients change per location so be ready to travel around Mexico through your palate. Explore Food & Culture in Downtown Cancun.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li>
	          	<!-- <li>
	  	    	    <img src="https://cancunfoodtours.com/img/salsa.jpg" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Discover Flavors of Mexico in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Mexico’s variety of food changes per region. Nothing is the same in the North, South, and center of Mexico. If your thing is to have the opportunity to test a lot whole of Mexico on one sit just book your spot in this amazing food tour.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li> -->
				<!-- <li>
	  	    	    <img src="https://cancunfoodtours.com/img/grupo1.jpg" style="  float: left; width: 100%;"/>
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Discover Flavors of Mexico in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Mexico’s variety of food changes per region. Nothing is the same in the North, South, and center of Mexico. If your thing is to have the opportunity to test a lot whole of Mexico on one sit just book your spot in this amazing food tour.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li> -->
	  	    	 <!--<li>
	  	    	    <img src="https://cancunfoodtours.com/img/grupo1.jpg" style="  float: left; width: 100%;"/>
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Are you ready for the best Mexican Food?</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">CancunFoodTours.com is your gateway to the most authentic and traditional flavors from Mexico you will be able to find in Cancun and the Riviera Maya. Allow us to show you our culture and society through good food...</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li>-->


	          </ul>
	          <div style="float:left; width:100%;">
			<p style="float: left;margin-top: 27px;font-size: 21px;width: 100%;font-weight: bold;letter-spacing: 0px;text-transform: uppercase;font-family: free;color: #cc3300;">Mexican food is not just tacos, prepare to get mind blown by influences of different cultures in mexican gastronomy.</p>
		</div>
	        </div>
	       
	       <div id="carousel" class="flexslider dos">
	          <ul class="slides men" >

		            <li >
		         		
			            <div class="todo12">
			            <div class="todo13">
			           		
				  	    	    <img src="https://cancunfoodtours.com/img/flavors-of-mexico-tour.jpg" alt="taco tours in cancun" title="taco tours in cancun" />
				  	    	    <div class="tito" >
				  	    	    	<span >Flavors of Mexico</span>
				  	    	    </div>
								<script>
  									window.location = $("#mylink").attr("href");
								</script>
				  	    	    <div class="dieter">
					  	    	    <div class="tito2">
					  	    	    	<div class="contenido1" >
						  	    	    	<p>When: Monday & Fridays</p>
						  	    	    	<p>Time: 3.5 - 4 hrs</p>
						  	    	    	<p>Stops: 4 </p>
						  	    	    	<b>Price: $79.00 usd</b>
						  	    	    	<div class="boton" style="" >
						  	    	    		<a href="#"  onClick=" window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour' "> BOOK NOW</a>
						  	    	    	</div>
						  	    	    </div>
					  	    	    </div>
				  	    	    </div>
				  	    	
			  	    	</div>
						</div>

		  	    	</li>

		  	    	<li>
		  	    	 <div class="todo12">
		  	    	 <div class="todo13">
		  	    	    	<img src="https://cancunfoodtours.com/img/producto2.jpg" alt="taco tours in cancun" title="taco tours in cancun"/>
			  	    	    <div class="tito" >
			  	    	    	<span>Taco Tour Local</span>
			  	    	    </div>
			  	    	    <div class="dieter">
			  	    	    <div class="tito2">
			  	    	    	<div class="contenido1" >
				  	    	    	<p>When: Everyday</p>
				  	    	    	<p>Time: 2h</p>
				  	    	    	<p>Stops: 3 </p>
				  	    	    	<b>Price: $60.00 usd</b>
				  	    	    	<div class="boton" >
				  	    	    		<a href="" style="    background: #999999;"> BOOK NOW</a>
				  	    	    	</div>
				  	    	    </div>
			  	    	    </div>
			  	    	    </div>
		  	    	    </div>
		  	    	    </div>
		  	    	</li>

		  	    	<li>
		  	    	 <div class="todo12">
		  	    	 <div class="todo13">
			  	    	    <img src="https://cancunfoodtours.com/img/mexico-1.jpg" alt="taco tours in cancun" title="taco tours in cancun"/>
			  	    	    <div class="tito" >
			  	    	    	<span>COMING SOON</span>
			  	    	    </div>
			  	    	    <div class="dieter">
			  	    	    <div class="tito2">
			  	    	    	<div class="contenido1" >
				  	    	    	<p>When: Everyday</p>
				  	    	    	<p>Time: 2h</p>
				  	    	    	<p>Stops: 3 </p>
				  	    	    	<b>Price: $60.00 usd</b>
				  	    	    	<div class="boton" >
				  	    	    		<a href="" style="    background: #999999;"> BOOK NOW</a>
				  	    	    	</div>
				  	    	    </div>
				  	    	</div>
			  	    	    </div>
			  	    	    </div>
		  	    	    </div>
		  	    	</li>
		  	    	<li>
			  	    	<div class="todo12">
			  	    	<div class="todo13">
				  	    	    <img src="https://cancunfoodtours.com/img/mexico-2.jpg" alt="taco tours in cancun" title="taco tours in cancun"/>
				  	    	     <div class="tito" >
				  	    	    	<span>COMING SOON</span>
				  	    	    </div>
				  	    	    <div class="dieter">
				  	    	    <div class="tito2">
				  	    	    	<div class="contenido1" >
					  	    	    	<p>When: Everyday</p>
					  	    	    	<p>Time: 2h</p>
					  	    	    	<p>Stops: 3 </p>
					  	    	    	<b>Price: $60.00 usd</b>
					  	    	    	<div class="boton" >
					  	    	    		<a href="" style="    background: #999999;"> BOOK NOW</a>
					  	    	    	</div>
					  	    	    </div>
				  	    	    </div>
				  	    	    </div>
			  	    	   </div>
			  	    	   </div>
		  	    	</li>
		       
	          </ul>
	        </div>

	      </section>
	   </div>
</div>