<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Official Food Tours in Cancun | CancunFoodTours.com</title>
	<link rel="canonical" href="https://cancunfoodtours.com/">
	<link rel="stylesheet" href="https://m.cancunfoodtours.com/css/swiper.min.css">
	<?php include('include/nuevolib.php'); ?>
	<style type="text/css">
		    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
	</style>
</head>
<body>
<?php include('include/menu.php'); ?>
	
	<div class="contenedormovil">
		<div class="textodentro" style="text-align: center;"><h1 style="border-top: 1px solid black;border-bottom: 1px solid black;    margin-bottom: 10px;">Your Access to the best food in Cancun</h1>
		</div>
	<div class="dentrode">
		<div class="dentrode">
			<div class="swiper-container">
			    <div class="swiper-wrapper">
			      <div class="swiper-slide">		
				      	<div class="dentrode1" style="background: #f3eedb;padding: 21px 35px;">
							<div class="dentrode" style="background: white;">
								<div style="float: left;padding: 10px;">
								<a href="https://m.cancunfoodtours.com/flavors-of-mexico-food-tour" style="color: black;">	
									<div class="imagen">
										<img src="https://cancunfoodtours.com/img/flavors-of-mexico-tour.jpg">	
									</div>
									<div class="textodentro">
										<p style="float: left;width: 100%;  font-size: 22px; margin-top: 12px;    font-weight: 600;">Flavors of Mexico Premium Experience</p>
										<p>The team at CancunFoodTours.com wanted all Mexican food lovers to try an amazing selection & variety of local eats in Cancun Mexico. While experiencing this amazing culinary tour, you will have the opportunity to taste a piece of history & culture on each drink & dish.</p>
									</div>
								</a>
									<div class="boton">
									  	<a href="https://m.cancunfoodtours.com/flavors-of-mexico-food-tour"> BOOK NOW</a>
									</div>
								</div>
							</div>
						</div>	
					</div>
			      	<div class="swiper-slide">		
				      	<div class="dentrode1" style="background: #f3eedb;padding: 21px 35px;">
							<div class="dentrode" style="background: white;">
								<div style="float: left;padding: 10px;">
									<a href="https://m.cancunfoodtours.com/taco-tour-in-cancun" style="color: black;">		
										<div class="imagen">

											<img src="https://cancunfoodtours.com/img/tacotourportada.png">	
										</div>
										<div class="textodentro">
											<p style="float: left;width: 100%;  font-size: 22px; margin-top: 12px;    font-weight: 600;">Taco Tour in Cancun</p>
											<p>Forget about hard shells, cheddar cheese and sour cream. Deep dive into what has shaped one of the most iconic dishes from Mexico, “The Taco”. Get to know why the tortillas pair up with almost everything when trying the most delicious tacos from the best “Taquerias in Cancun”</p>
										</div>
									</a>
									<div class="boton">
									  	<a href="https://m.cancunfoodtours.com/taco-tour-in-cancun"> BOOK NOW</a>
									</div>
								</div>
							</div>
						</div>	
					</div>
			    </div>
			    <!-- Add Arrows -->
			    <div class="swiper-button-next"></div>
			    <div class="swiper-button-prev"></div>
			</div>
		</div>

		<!--<div class="dentrode1" style="background: #f3eedb; padding: 0px 14px;">
			<div class="dentrode" style="background: white;margin-bottom: 15px;">
				<div style="float: left;padding: 10px;">
				<div class="imagen">
					<img src="https://cancunfoodtours.com/img/flavors-of-mexico-tour.jpg">	
				</div>
				<div class="textodentro">
					<h1>Flavors of Mexico Premium Experience</h1>
					<p>The team at CancunFoodTours.com wanted all Mexican food lovers to try an amazing selection & variety of local eats in Cancun Mexico. While experiencing this amazing culinary tour, you will have the opportunity to taste a piece of history & culture on each drink & dish.</p>
				</div>
				</div>
			</div>
		</div>-->

		<div class="dentrode">
			<div style="float: left;width: 100%;">
				<h2 style="color: #ff6633;text-align: center;float: left;width: 100%;font-size: 23px;    margin-top: 10px;"><b style="color: black;font-weight: 400;margin-right: 10px;">Why eat with</b>Cancunfoodtours.com?</h2>
				<div style=" padding: 14px;float: left;">
					<p>Welcome to Cancunfoodtours.com, the home to discover one of the most incredible mexican gastronomic experiences in Cancun. Cancunfoodtours.com is a professionally guided tour of the local food and beverage scene which has become one of the best things to do in Cancun. Made for foodies & food enthusiasts (or everyone, really) that are looking to indulge in Mexican flavors and cultural experiences.<br><br>
					Wonderful Cancun gastronomy is Mexico's gift to the world, and Mexicans take cooking very seriously, our goal is to provide Cancun’s local cuisine as part of your travel itinerary so you can experience mexican food as an insider, and to explore food safely and with convenience. Our restaurants have the highest standards of quality and service so you’re guaranteed the best dining experience in Cancun. Save your spot, Book Now!

					</p>
					<iframe src="https://www.youtube.com/embed/UYhRLqhnF-k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="float: left;width: 100%;height: 269px;"></iframe>
				</div>
			</div>
		</div>
			<div class="dentrode">
				<!--<div class="textodentro"><h2>COMPARE TOURS</h2></div>-->
				<div class="dentrode">
				<!--<div>
					<div class="foot1">
						<div class="footer1">
							<h3> Flavors of Mexico</h3>
						</div>
						<div class="contenido1">
							<div class="trio">
								<p>When: Everyday but Saturday</p>
						  	   	<p>Time: 3.5 – 4 hrs</p>
						  	    <p>Stops: 4 </p>
						  	    <b>Price: $79.00 usd</b>
						  	    <div class="boton">
						  	    	<a href="https://m.cancunfoodtours.com/flavors-of-mexico-food-tour"> BOOK NOW</a>
						  	    </div>
						  	</div>
					  	</div>
					</div>

					<div class="foot1">
						<div class="footer1">
							<h3>TACO TOUR LOCAL</h3>
						</div>
						<div class="contenido1">
							<div class="trio">
								<p>When: Everyday</p>
						  	   	<p>Time: 2h</p>
						  	    <p>Stops: 3 </p>
						  	    <b>Price: $60.00 usd</b>
						  	    <div class="boton">
						  	    	<a href="https://m.cancunfoodtours.com/flavors-of-mexico-food-tour" style="background: #999999;"> BOOK NOW</a>
						  	    </div>
						  	</div>
					  	</div>
					</div>
				</div>-->
					<div class="dentrode" style="background: white;margin-top: 10px;">
					<div style="float: left; padding: 0px 14px;">
						<a href="https://m.cancunfoodtours.com/cancun-food-tours-private-events" style="color: black;">
							<div class="textodentro" style="text-align: center;"><span>PRIVATE GROUP EVENTS</span></div>
							<div class="priva" style="float: left;width: 100%;">
								<img src="https://cancunfoodtours.com/img/private-group-events-food-tour.jpg" style="float: left;width: 100%;">
							</div>
							<div class="aquiesta" style="    margin-top: 20px;">
								<div style=" padding: 0px 10px;">
								<p style="float: left;width: 100%;margin-top: 10px;">Try our experience privately and exclusively for your group members. These private food tours are perfect for teams, corporate events or large friend outings.</p>
						</a>	
							<div class="venado" style="margin-top: 33px;    text-align: center;">
								<a href="https://m.cancunfoodtours.com/cancun-food-tours-private-events">LEARN MORE</a>
							</div>
							</div>
						</div>
					</div>

					</div>
					<div style=" padding: 0px 14px;">
						<?php 
							include('../includes/comentarios.php');
						?>
					</div>
				<div class="dentrode">
					<div style="float: left;width: 100%;background: #ededed;">
						<h4 style="  float: left;  width: 100%; text-align: center; font-size: 22px; text-transform: uppercase;  color: #333;  padding: 15px 0px; ">WHY BOOK WITH<b style=" color: #ff6633;"> CANCUN FOOD TOURS?</b></h4>
						<div class="coment1" style="float: left;width: 50%; text-align: center;">
							<div style="float: left;width: 100%;">
								<div style="padding: 0px 10px;float: left;">
								<img src="https://cancunfoodtours.com/img/cuatro4.png" style=" width: 30%;">
								<span style="float: left;color: #ff6633; width: 100%; font-size: 21px; font-weight: 700;">CONVENIENT</span>
								<p style="float: left; width: 100%; font-size: 13px;">Just book. Then relax, we’ll take of the rest, even the transportation...</p>
								</div>
							</div>
						</div>
						<div class="coment1" style="float: left;width: 50%; text-align: center;">
							<div style="float: left;width: 100%;">
								<div style="padding: 0px 10px;float: left;">
								<img src="https://cancunfoodtours.com/img/ok-2.png" style="    width: 36%;">
								<span style="float: left;color: #ff6633; width: 100%; font-size: 21px; font-weight: 700;">GREAT QUALITY</span>
								<p style="float: left; width: 100%; font-size: 13px;">Handpicked high-standard restaurants are only invited to be our partners</p>
								</div>
							</div>
						</div>
						<div class="coment1" style="float: left;width: 50%;text-align: center;margin-top:  20px;margin-bottom:  10px;">
							<div style="float: left;width: 100%;">
								<div style="padding: 0px 10px;float: left;">
								<img src="https://cancunfoodtours.com/img/indulge.png" style="    width: 36%;">
								<span style="float: left;color: #ff6633; width: 100%; font-size: 21px; font-weight: 700;">INDULGE</span>
								<p style="float: left; width: 100%; font-size: 13px;">Get ready to immerse yourself in a one of a kind mexican culinary experience</p>
								</div>
							</div>
						</div>
						<div class="coment1" style="float: left;width: 50%;text-align: center;margin-top:  20px;margin-bottom:  10px;">
							<div style="float: left;width: 100%;">
								<div style="padding: 0px 10px;float: left;">
								<img src="https://cancunfoodtours.com/img/seis6.png" style=" width: 50%;">
								<span style="float: left;color: #ff6633; width: 100%; font-size: 21px; font-weight: 700;">TRUST</span>
								<p style="float: left; width: 100%; font-size: 13px;">Hundreds of foodies have been delighted with our Cancun Food Tours</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
		</div>
		</div>
		</div>
<?php 
	include('include/footer.php');
?>
<script src="https://m.cancunfoodtours.com/css/swiper.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>