<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>About Us for CancunFoodTour.com</title>
	<link rel="canonical" href="https://cancunfoodtours.com/about-us">
	<meta name="description" content= "Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
	<link rel="stylesheet" href="https://m.cancunfoodtours.com/css/swiper.min.css">
	<?php include('include/nuevolib.php'); ?>
</head>
<body>
<?php include('include/menu.php'); ?>

	<div class="contenedormovil">
		<div class="dentrode">
			<div class="dentrode">
				<div style="float:left;width: 100%;"><img src="https://cancunfoodtours.com/img/imagen-foot.png" style="float: left;width: 100%;"></div>
					<div style="float:left;width: 100%;">
						<div style=" padding: 0px 10px;">	
						<h1 style="font-size: 23px;margin-top:  10px;margin-bottom:  10px;">Food and tradition is in our heart</h1>
						<p style="font-size: 17px;margin-bottom: 5px;">Mexican food is a gift provided by our ancestors that have passed from generation to generation. We believe that this gift has served as a universal language that has shaped the way Mexican culture is today.</p>
						<p>As receivers of this gift, our duty is to present it to the world & use it as a bridge to connect with people and other cultures a using the same language our ancestors used. All our tours and experiences have been created to connect you with the greatest food, drinks, tastes and experiences in Cancun & in Mexico. Our goal is to introduce the Mexican Food in Cancun as one of the greatest in the world.<br><br>
						We’ve shared this vision with our staff so that they, as we are proud to provide you with all the knowledge and culinary information that will take you into a unique Journey. We can’t wait to show you and see you indulge with our creations.</p>
						<p><strong>- Your Friends Cancun Food Tours</strong></p>
						</div>
					</div>
			</div>
		</div>
	</div>
<?php 
	include('include/footer.php');
?>
</body>
</html>