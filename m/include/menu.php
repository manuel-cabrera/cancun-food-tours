<!-- Fuentes de Google Fonts --> 
<link href="//fonts.googleapis.com/css?family=Lobster:400" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<!-- Fin Fuentes de Google Fonts --> 
<style type="text/css">
	
	/*Damos estilos al menú*/
.nav {
    position: absolute;
    width: 200px;
    height: 100vh;
    top: 0;
    left: -220px;
    padding-top: 47px;
    background-color: rgb(243, 238, 219);
    border-right: 2px solid rgb(135, 96, 57);
    transition: all .5s ease;
}
   /*Estilos de los li del menú*/
   .nav__item {
    display: block;
    width: 100%;
    margin: 0 auto;
    line-height: 2;
    position: relative;
    border-bottom: .5px solid rgb(135, 96, 57);
    border-top: .5px solid rgb(135, 96, 57);
    background-color: transparent;
    /* font-family: 'Lato', sans-serif; */
    text-align: center;
    color: #876039;
    font-size: 17px;
    font-weight: bold;
    text-decoration: none;
   }

  .menu-toogle {
    position: absolute;
    width: 35px;
    line-height: 1.1;
    text-align: center;
    top: 2.5px;
    left: 8px;
    border-radius: 3px;
    transition: all .5s ease;
    z-index: 10;
  }

    .menu-toogle::before {
    content: "☰";
    font-size: 37px;
    color: #876039;
    }

/* Le damos unos estilos al contenedor principal*/
.main-w {
  width:100%;
  height:100vh;
  transition: all .5s ease;
}   
    .header-w figure {
      width:100%;
      height:150px;
      text-align:center;
      position:relative;
    }
        .header-w figure:before {
          content:"O";
          width:15px;
          height:15px;
          position:absolute;
          background-color:white;
          border-radius:100%;
          color:#A9BC00;
          font-size:120%;
          font-wheight:bolder;
          font-family:verdana;
          left:42vw;
          top:9vw;     
        }
        .header-w figure:after {
          content:"O";
          width:15px;
          height:15px;
          position:absolute;
          background-color:white;
          border-radius:100%;
          color:#A9BC00;
          font-size:120%;
          font-wheight:bolder;
          font-family:verdana;
          left:50.8vw;
          top:9vw; 
        }
    .featured-img {
      margin:3em 6vw 0 0;
      border:4px solid white;
      border-radius:100%;
    }
    .header-w__description {
      width:80%;
      height:auto;
      margin:0 auto;
      padding:1em;
      line-height:2;
      text-align:center;
      color:rgba(33, 36, 8, 1);
    }
        .header-w__title
        {
          font-size:300%;
          font-family:Lobster;
          margin:1em 0 0 0;
        }
        .header-w__title2 {
          font-size:250%;
          font-family:Lobster;
          margin:0;
        }
    .paragraph {
      width:80%;
      margin:0 auto;
      text-align:center;
      font-family:'Source Sans Pro', sans-serif;
    }

    /*Estos son los estilos que le dan la funcionalidad al menú*/
    /*El checkbox según esté activado o no hará que el menú aparezca o desaparezca*/
    .checkbox {
      display:none; /*Ocultamos el checkbox pues no nos interesa que se vea*/
    }
    .checkbox:checked ~ .nav {
      left:0px; /*Le quitamos el left negativo para que el menú vuelva a su posición original*/
      position:fixed; /* Fijamos el menú lateral para que se desplace cuando hagamos scroll*/
    }
    .checkbox:checked ~ .main-w {
      margin-left:200px; /*Cuando el checkbox asigna un margen al contenedor principal para que se desplace junto con el menú*/
    }
    .checkbox:checked ~ .menu-toogle { 
      left:208px;
      position:fixed; /*Fijamos la hamburgues para que se desplace junto con el menú*/
    }


</style>
<div style="float:left;width: 100%;height: 48px;background: #f3eedb;position: fixed;z-index: 9999999999;">
	<!-- Hack Para Desplegar el Menú activando un checkbox -->
	<input type="checkbox" class="checkbox" id="menu-toogle"/>
	<label for="menu-toogle" class="menu-toogle"><span style="font-size: 21px;color: #876039;top: 8px;position: absolute;    left: 38px;"></span></label>

	<nav class="nav">
		<a href="https://m.cancunfoodtours.com/" class="nav__item current" style="font-weight: 100;">Home</a>
	    <a href="https://m.cancunfoodtours.com/about-us" class="nav__item" style="font-weight: 100;">About Us</a>
		<a href="https://m.cancunfoodtours.com/flavors-of-mexico-food-tour" class="nav__item" style="font-weight: 100;">Flavors of Mexico</a>
    <a href="https://m.cancunfoodtours.com/taco-tour-in-cancun" class="nav__item" style="font-weight: 100;"> Taco Tour Local</a>
		<a href="https://m.cancunfoodtours.com/cancun-food-tours-private-events" class="nav__item" style="font-weight: 100;">Private Groups</a>
	    <a href="https://m.cancunfoodtours.com/faq" class="nav__item" style="font-weight: 100;">F.A.Q</a>

	    <a href="https://m.cancunfoodtours.com/contact-us" class="nav__item" style="font-weight: 100;">Contact Us</a>
	    <a href="tel:18004814603 " class="nav__item"><img class="noimporta2" src="https://cancunfoodtours.com/img/telefono.png" style="float: left;width: 41px;"><span style="float: left;">Tel: 1-800-481-4603</span></a>
	     <!--<a href="tel:+529981474390" class="nav__item"><img class="noimporta2" src="https://cancunfoodtours.com/img/telefono.png">When in Mexico: <br>9981-47-43-90</a>-->
	</nav>
</div>
<div style="float:left;width: 100%;height: 48px;">
	
</div>

  <div class="contenedormovil">
    <div class="dentrode">
      <div class="dentrode1" style="padding: 0px 17%;width: 66%;">
        <div class="dentrode">
          <div style="float: left;width: 100%;"><a href="https://m.cancunfoodtours.com/"><img src="https://cancunfoodtours.com/img/logo-cancunfoodtours.png" style="float: left;width: 100%;"></a></div>
        </div>
      </div>
    </div>
  </div>