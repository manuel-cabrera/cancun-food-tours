<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Private Events & Groups</title>
	<link rel="canonical" href="https://cancunfoodtours.com/cancun-food-tours-private-events">
	<?php include('include/nuevolib.php'); ?>

	<style type="text/css">
	.form-control {
display: block;
    width: 100%;
    height: 34px;
    /* padding: 6px 12px; */
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #dad4d4;
    background-image: none;
    border: 1px solid #ccc;
    /* border-radius: 4px; */
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    font-family: 'Roboto', sans-serif !important;
}

	</style>
</head>
<body>
<?php include('include/menu.php'); ?>
	<div class="contenedormovil">
		<div class="dentrode">
			<div class="dentrode1">
				<div class="dentrode">
					<div style="margin-bottom: 20px;float: left;width: 100%;"><img src="https://cancunfoodtours.com/img/cancun-food-tours-private-events.jpg" style="float: left;width: 100%;"></div>
				</div>
			</div>
		</div>
		<div style="float:left; width:100%;">
			<div style=" padding: 0px 10px;">
			<div class="contactsd" >
				<h1 style="font-size: 22px;text-align: center; margin-bottom: 10px;">Cancun Food Tour<b style="color: #f1592a;font-weight: 400;"> Private Events & Groups</b></h1>
				<p style="margin-bottom: 10px;">Mexico is a country which is very rich in terms of culture. Food is one of the best & most exciting ways of getting to know Mexico since most of our traditions & ways of life are strictly related to it.<br><br>

				If you wish to get to know Mexican culture from a deeper perspective we can help you set a tour up that will cater for your specific needs. Private Transportation, tour guide and other services are at your disposal to make your experience something to remember.<br><br>

				Food Tours are a great experience that are good for celebrations, family bonding, company outings, incentives or maybe just the idea of sharing and enjoying the love for Mexican food is enough. Whatever the reason, contact us to quote and we will accommodate you. It doesn’t matter if you’re a group from 11 – 100, don’t hesitate in contacting us.</p>
			</div>
			<div class="contform" >
				<form action="https://cancunfoodtours.com/mailer.php" method="POST" id="contactus" class="form-cont">
				   	<span class="title"></span>
				   	<input type="text" name="nombre" placeholder="Name" class="form-control" title="Introduce tu nombre porfavor" maxlength="50" required="" style=" margin-bottom: 10px; height: 47px;">
				   	<input type="text" name="lastnombre" placeholder="LastName" class="form-control" title="Last Name" maxlength="50" required="" style=" margin-bottom: 10px; height: 47px;">
				   	<input type="email" name="email" placeholder="Email" class="form-control" title="Introduce tu correo electronico" required="" style="margin-top: 19px; margin-bottom: 10px; height: 47px;">
				   	<input type="text" name="Company" placeholder="Company" class="form-control" title="Company" maxlength="50" required="" style=" margin-bottom: 10px; height: 47px;">
				   	<input type="text" name="Phone" placeholder="Phone Number" class="form-control" title="Phone Number" maxlength="50" required="" style=" margin-bottom: 10px; height: 47px;">
				   	<label>Date Desired</label>
				   	<input type="date" name="date" placeholder="dd/mm/aaaa" class="form-control" title="date" style=" margin-bottom: 10px; height: 47px;">
				   	<input type="text" name="numbernose" placeholder="Number if Attendees" class="form-control" title="Number if Attendees" maxlength="50" required="" style=" margin-bottom: 10px; height: 47px;">
				   	<textarea placeholder="(Please provide start time preference, tour route desired, nature of event, and any other pertinent details we should know)" class="form-control"></textarea>
				   	<button class="btn btn-primary" name="mysubmit" type="submit" style="padding: 8px;margin-bottom: 17px;font-size: 17px;margin-top: 11px;background-color: #ff3d00;border-color: #ff3d00;padding: 9px 0px;float: left;width: 100%;/* font-family: free; */background: #cc3300;color: white;text-align: center;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;font-family: 'Roboto', sans-serif;">Submit</button>
			    </form>
			</div>

	
		<div class="imgmap contactsd" style="float:left;width: 100%;">
				<h1>The<b style="color: #f1592a;font-weight: 400;"> Team</b></h1>
				
				<p><b>Ticket & Tour Questions:</b> <br>Email: customerservice@cancunfoodtours.com <br>Tel: +52 1 998-887-42-42<br><br>


				<img src="https://cancunfoodtours.com/img/mapa14.jpg" style="float:left;width: 100%;">
				<p><b>ADDRESS:</b> Cancun Food Tours Carretera a Punta Sam Mza 2 34, SM 86 Punta Sam, Cancun, C.P. 77520</p>
			</p>
				
		</div>
		</div>
			</div>
	</div>
	</div>
<?php 	include('include/footer.php'); ?>
</body>
</html>