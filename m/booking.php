<?php 

session_start();
include('../booking/lib/conexion.php');  

$idproducto = $_GET['idproducto'];


?>

<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="https://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Booking Information | CancunFoodTours.com</title>

	<link rel="alternate" hreflang="x-default" href="https://cancunfoodtours.com/">	
	<link rel="canonical" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en-US" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
 
<?php include('include/nuevolib.php'); ?>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  	<!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
  	<script src="https://cancunfoodtours.com/js/jquery-2.1.1.min.js"></script>
  	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cancunfoodtours.com/js/modernizr.js"></script>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<style type="text/css">
	.cont1{float: left;width: 100%;}
	.book{width: 100%;float: left;}
	.intform{float: left;width: 100%;}
	.intform1{float: left;}
	.numerot{float: left;width: 100%;margin-bottom: 10px;}
	.numeros{float: left;}
	.medio{float: left;	width: 100%; margin-bottom: 10px;}
	label{	color: #31708f;    font-weight: 600;	}
	.titulo{ float: left; width: 100%;background-color: #e5e5e5;   margin-bottom: 15px;}
	.controlinput{float: left;width: 100%;    margin-bottom: 10px;}
	.peque{    float: left;  width: 100%;  margin-bottom: 10px;}
	.book2{    float: left;  width: 100%; background: #e5e5e5;}
	.bookcont{ float: left;     width: 100%;    background: #f2f2f2;  box-shadow: 0px 0px 3px 0px #7C7473; }
    .resumencontrol{float: left; width: 100%;}

    .contenido{	float: left;  	width: 100%;   	margin-bottom: 14px;   }
    .contieneotro{ 	float: left;  	width: 100%;   	margin-bottom: 20px;    }
    .control1{float: left;width: 100%;     margin-top: 25px;}
    .booking_error{
    	float: left;
    	font-size: 19px;
    	color: red;
    }
    .form-control{
    	float: left;
    	width: 100%;
    	padding: 4px 0px;
    }
    .mod-normal{
    	float: left;width: 100%;
    }

    .form-control {
	    display: block;
	    width: 100%;
	    /* height: 37px; */
	    padding: 7px 0px;
	    font-size: 14px;
	    line-height: 1.42857143;
	    color: #555;
	    background-color: #dad4d4;
	    background-image: none;
	    border: 1px solid #ccc;
	    border-radius: 4px;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	        font-family: 'Roboto', sans-serif;
}
	</style>

<?php include('../booking/lib/controlscrip.php'); ?>

	<?php include('../includes/tagmanager.php'); ?>

<script type="text/javascript">

$( document ).ready(function() {

    timer()

});
	
	    function pagopaypal(){

	    	var fecha = $('#bookfecha').val();
	    	var tiempo = $('#tiempo').val();
	    	var pax = $('#cantidad1').val();
	    	
	    	var nombre = $('#primero').val();
	    	var apellido = $('#segundo').val();
	    	var nombrehotel = $('#nombreh').val();
	    	var numerocuarto = $('#numero4').val();
	    	var pais = $('#pai').val();
	    	var estado = $('#esta').val();
	    	var direccion = $('#dress').val();
	    	var zipcode = $('#codigos').val();
	    	var correoelectronico = $('#email').val();
	    	var telefono = $('#tele').val();
	    	var notasextra = $('#extra').val();
	    	var total = $('#totalpri').val();





	            if(tiempo == ""){
	                $("#errorcorreo").fadeIn("slow").delay(2000).fadeOut('slow');
	                $('#errorcorreo').text('Empty Email').addClass('booking_error'); 
	                return false;
	            }



            if(tiempo == "Select Time"){
                $("#errortime").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errortime').text('Time Invalid').addClass('booking_error'); 
                return false;
            }

            if (fecha == "") {

                $("#errorfecha").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorfecha').text('Date Invalid').addClass('booking_error'); 
                return false;

            }

            if (pax == "0") {

                $("#errorpax").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorpax').text('Tickets Invalid').addClass('booking_error'); 
                return false;

            }

            if (nombre == "") {

                $("#errornombre").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errornombre').text('Empty Name').addClass('booking_error'); 
                return false;
            }

            if (apellido == "") {

                $("#errorlast").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorlast').text('Empty Last Name').addClass('booking_error'); 
                return false;
            }
            
            if (nombrehotel == "") {

                $("#errornombrehotel").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errornombrehotel').text('Empty Hotel Name').addClass('booking_error'); 
                return false;
            }

  				if (telefono == "") {

                	$("#errortel").fadeIn("slow").delay(2000).fadeOut('slow');
                	$('#errortel').text('Empty Cell Phone').addClass('booking_error'); 
                	return false;
            	}



            if (estado == "") {

                $("#errorestado").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorestado').text('Empty State/Province').addClass('booking_error'); 
                return false;
            }

            if (pais == "") {

                $("#errorpais").fadeIn("slow").delay(2000).fadeOut('slow');
                $('#errorpais').text('Empty Country').addClass('booking_error'); 
                return false;
            }

            

        ventana();

        $.post("https://m.cancunfoodtours.com/lib/paypal.php", {"fecha":fecha, "tiempo":tiempo, "pax":pax, "nombre":nombre, "apellido":apellido, "nombrehotel":nombrehotel, "numerocuarto":numerocuarto, "pais":pais, "estado":estado, "direccion":direccion, "zipcode":zipcode, "correoelectronico":correoelectronico, "telefono":telefono, "notasextra":notasextra, "total":total }, function(data){ 
        	
        	$(location).attr('href',data); 
        	//window.location.replace(""+data+"");
        	//altert(data);

        });

    }   

	function controltotal(){
		
		var paxtotal = $('#cantidad1').val();
		var precio1 = $('#precio1').val();	

		var multiplica = paxtotal*precio1;
		
		$('#totalsuma').text(multiplica);
		$('#adulto').text(paxtotal);
		$('#totalprice').text(multiplica);
		$('#totalpri').val(multiplica);



	}

    function ventana(){

        $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 

    }

</script>
</head>
<body>

	<?php include('include/menu.php'); ?>

	<style type="text/css">
		.noimporta{float: left;width: 37px;margin-top: -3px;}
		.noimporta1{float: left;width: 37px;margin-top: -3px;}
		.noimporta2{float: left;width: 37px;margin-top: -3px;}
	</style>
	
<div class="cont1">
	<div style=" padding: 0px 8px;">

	<div class="contieneotro">
	<div class="book">
		<div class="titulo">
			<span style="float: left;width: 100%;text-align: center;font-size: 20px;padding: 5px 0px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;color: #000000;">New Food Tour Booking</span>
		</div>
			<div class="intform">
				<div class="inform1">
					
					<div class="controlinput" >
						<form action="" method="POST" id="pago-con-paypal">
							<div class="mod-normal">
								<label style="float:left; width:100%; font-size: 17px;">Select Date</label>
								<div class="medio" >	
									<input type="text" name="datosfecha" id="datepicker" class="form-control"  placeholder="Select date" required>
								</div>
								<span style="display: none;" id="errorfecha"></span>
							</div>
							<div class="mod-normal">
								<label style="float:left; width:100%; font-size: 17px;">Select Time</label>
								<div class="medio" >	
									<select name="tiempo" class="form-control" style=" width: 100%; float:left;" id="tiempo" onclick="timer()" onchange="timer()" required>
									
										<?php 
											$controltiempo = mysql_query("SELECT * FROM tiempo WHERE n1 = $idproducto");
											while($tiempo = mysql_fetch_array($controltiempo)){
												$malo = $tiempo['hora'];

												echo "<option select>".$malo."</option>";
											}

										?>
										
									</select>
									<span style="display: none;" id="errortime"></span>
								</div>
							</div>
						</div>


					<label style="font-size: 17px;" >Tickets</label>
					<span style="display: none;" id="errorpax"></span>
						<?php 

							$result = mysql_query("SELECT * FROM tikect WHERE idproducto = $idproducto"); 
							while($row= mysql_fetch_array($result)){

								$cantidad = $row['numero'];
								$idtick = $row['idtikect'];
								$nombre = $row['nombre'];
								$precio = $row['precio'];
								$precio = number_format($precio, 2, '.', '');

							echo '<div class="numerot"> 
								<select name="numero'.$idtick.'" class="form-control" style=" width: 100%; float:left;" id="cantidad1" onclick="controltotal()" onchange="controltotal()" required>';
								echo "<option value='0'>Select ticket</option>";
								for ($i=1; $i <= $cantidad ; $i++) { 
									echo "<option value='".$i."'>".$i."</option>";
								}
								echo "</select>";
								echo '<input type="hidden" value="'.$precio.'" id="precio1">';

								echo '<p style=" float: left;width: 100%;text-transform: capitalize;font-size: 18px;margin-top: 10px;">'.$nombre.' $<b id="calcula">'.$precio.'</b> USD </p></div>';
							}
						?>
							

						<div class="form2" >
								<div class="titulo" >
									<span style="float: left;width: 100%;text-align: center;font-size: 20px;padding: 5px 0px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;color: #000000;">Booking Information</span>
								</div>
								<div class="controlinput" >
									<div class="peque">
									<label style="float:left; width:100%; font-size: 17px;">First Name(*)</label>
										<input type="text" class="form-control"  placeholder="First Name" name="primernombre" id="primero"  required>
										<span style="display: none;" id="errornombre"></span>
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">Last Name(*)</label>
										<input type="text" class="form-control" name="segundonombre"  placeholder="Last Name" id="segundo"  required>
										<span style="display: none;" id="errorlast"></span>
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque">
									<label style="float:left; width:100%; font-size: 17px;">Hotel Name(*)</label>
										<input type="text" class="form-control" name="nombrehotel"  placeholder="Hotel Name" id="nombreh">
										<span style="display: none;" id="errornombrehotel"></span>
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">Room Number(Optional)</label>
										<input type="text" class="form-control" name="numerocuarto"  placeholder="Room Number" id="numero4" >
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque">
										<label style="float:left; width:100%; font-size: 17px;">Country(*)</label>
										<!--<input type="text" class="form-control" name="pais" id="pai" >-->
										<span style="display: none;" id="errorpais"></span>

										<select class="form-control" id="pai">
											<?php 

												$controlpais = "SELECT * FROM paises";
												$controlconsulta = mysql_query($controlpais);
													while($rowspais = mysql_fetch_array($controlconsulta)){

														echo "<option value='".$rowspais['nombre']."'>".$rowspais['nombre']."</option>";		
													}
											?>
										  <option>Mustard</option>
										  <option>Ketchup</option>
										  <option>Relish</option>
										</select>
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">State/Province(*)</label>
										<input type="text" class="form-control" name="estado" id="esta"  placeholder="State/Provence">
										<span style="display: none;" id="errorestado"></span>
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque">
										<label style="float:left; width:100%; font-size: 17px;">Address</label>
										<input type="text" class="form-control" name="address" id="dress"  placeholder="Addres">
									</div>
									<div class="peque" >
										<label style="float:left; width:100%; font-size: 17px;">Zip/Postal Code</label>
										<input type="text" class="form-control" name="codigopostal" id="codigos"  placeholder="Zip Code">
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque">
										<label style="float:left; width:100%; font-size: 17px;">Email(*)</label>
										<input type="text" class="form-control" name="correo" id="email"  placeholder="Email" required>
										<span style="display: none;" id="errorcorreo"></span>
									</div>
									<div class="peque" >
										<label style="float:left; width:100%; font-size: 17px;">Cell Phone(*)</label>
										<input type="text" class="form-control" name="telefono" id="tele"  placeholder="Cell Phone">
										<span style="display: none;" id="errortel"></span>
									</div>
										
									<div class="control1" >
									<label>Extra Comments</label>
										<textarea class="form-control" name="extra" id="extra"  placeholder="Extra Comments"></textarea>
									</div>
								
								</div>
						</div>
				</div>
			</div>
		</div>
		<!-- Aqui esta otro -->
	</div>
</div>
		<div class="book2" style="position: fixed;bottom: 0px;">
			<div class="bookcont">
				<div style="padding: 0px 8px;">
				<?php 

						$controlproduct = mysql_query("SELECT * FROM producto WHERE idproducto = $idproducto");
						while ($producto = mysql_fetch_array($controlproduct)) {
							# code...
							$nombre = $producto['nombre'];
						}

					  ?>
						<label style="font-size: 17px;width: 100%;/* text-align: center; */float: left;margin-top: 5px;">Payment Summary</label>

						<div class="resumencontrol">
							<div class="contenido" style="margin-bottom: 0px;">
								<span style="float: left;width: 100%;font-size: 18px;text-align: center;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;border-bottom: 1px solid #0e4c82;margin-bottom: 11px;"><?php echo $nombre; ?></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;" >Date: <b id="fecha">YYYY/MM/DD</b></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;">Time: <b id="vistatiempo">00:00 PM</b></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;">Tickets: <b id="adulto">0</b>x Adult</span>
								<span style="float: left;width: 100%;font-size: 15px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;margin-top: 0px;text-align: right;font-weight: 700;">Total: $<b id="totalsuma">00.00</b> USD</span>
							</div>
						</div>
					<div style="float: left;width: 100%; text-align: center;">		
						<button type="button" class="btn btn-primary" style="padding: 7px 24%;margin-bottom: 6px;font-size: 21px;margin-top: 11px;background-color: #ff3d00;border-color: #ff3d00;    font-family: 'Roboto', sans-serif;color: white;" onfocus="pagopaypal()" onclick="javascript:pagopaypal()">Proceed to Payment</button>
					</div>
				</div>
			</div>
		</div>
		<div class="book2" style="margin-top: 20px;">
			<div style="padding: 0px 8px;">
				<label style="font-size: 21px;width: 100%;text-align: center;margin-top: 10px;float: left;margin-bottom: 10px;">You can pay with:</label>
				<img src="https://cancunfoodtours.com/img/icono.png" style="float: left;width: 92%;padding: 1px 3%;margin-bottom: 10px;">
			</div>
		</div>
		<div class="book2" style="margin-top: 20px;margin-bottom: 28px;">
			<div style="padding: 0px 8px;">
				<label style="font-size: 17px;width: 100%;text-align: center;margin-top: 10px;float: left;">Data Security:</label>
				<p style="float: left;width: 100%;margin-bottom: 10px;">Your info's safe with us. All data is encrypted and transmitted securely with an SSL protocol.<br><br>
					CancunFoodTours respects your privacy. We do not sell your personal information to anyone.</p>
			</div>
		</div>

		<div class="book2" style="margin-top: 20px;">
			<div style="padding: 0px 8px;">
				<label style="font-size: 17px;width: 100%;text-align: center;margin-top: 15px;float: left;">Book with Confidence:</label>
				<p style="float: left;width: 100%;margin-bottom: 10px;">Peace of mind. Book ahead to skip the lines and reserve your spot.</p>
			</div>
		</div>

		<div class="book2" style="margin-top: 20px;text-align: center;" >
			<div style="padding: 0px 8px;">
				<label style="font-size: 17px;width: 100%;text-align: center;margin-top: 15px;color: #070d3a;font-weight: 700;float: left;">Total Price: $<b id="totalprice">00.00</b> USD</label>
				
				<input type="hidden" name="totalpri" id="totalpri">
				<input type="hidden" name="bookfecha" id="bookfecha">
					
				<button type="button" class="btn btn-primary" style="padding: 7px 24%;margin-bottom: 6px;font-size: 21px;margin-top: 11px;background-color: #ff3d00;border-color: #ff3d00;    font-family: 'Roboto', sans-serif;color: white;" onfocus="pagopaypal()" onclick="pagopaypal()"> Proceed to Payment </button>
			</div>
			</form>
			<form class="personal_info top"> 
				<div id="messageBox"></div> 
			</form>
			 
		</div>
	</div>

</div>

<?php include('includes/footer.php');  ?>

<script src="https://cancunfoodtours.com/booking/lib/js/block.js"></script>

</body>
</html>