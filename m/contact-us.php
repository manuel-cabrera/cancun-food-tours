<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Email and Phone Contact for CancunFoodTour.com</title>
	<meta name="description" content= "Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
	<?php include('include/nuevolib.php'); ?>

	<style type="text/css">
	.form-control {
display: block;
    width: 100%;
    height: 34px;
    /* padding: 6px 12px; */
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #dad4d4;
    background-image: none;
    border: 1px solid #ccc;
    /* border-radius: 4px; */
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    font-family: 'Roboto', sans-serif;
}

	</style>
</head>
<body>
<?php include('include/menu.php'); ?>
	<div class="contenedormovil">
		<!--<div class="dentrode">
			<div class="dentrode1">
				<div class="dentrode">
					<div style="margin-bottom: 20px;float: left;width: 100%;"><img src="https://cancunfoodtours.com/img/cancunfoot.png" style="float: left;width: 100%;"></div>
				</div>
			</div>
		</div>-->
		<div style="float:left; width:100%;">
			<div style=" padding: 0px 10px;">
			<div class="contactsd" >
				<h1>Cancun Food Tour<b style="color: #f1592a;font-weight: 400;"> Contact</b></h1>
				<p style="margin-bottom: 10px;">We’re more than willing to answer all your questions & inquiries. We kindly invite you to look at our F.A.Q.’s. Use the form below to ask us about any additional support you need.</p>
			</div>
			<div class="contform" >
				<form action="https://cancunfoodtours.com/mailer.php" method="POST" id="contactus" class="form-cont">
				   	<span class="title"></span>
				   	<input type="text" name="nombre" placeholder="Name" class="form-control" title="Introduce tu nombre porfavor" maxlength="50" required="">
				   	<input type="email" name="email" placeholder="Email" class="form-control" title="Introduce tu correo electronico" required="" style="margin-top: 19px;">
				   	<textarea name="mensaje" placeholder="Write your message" class="form-control" title="Introduce tu mensaje" required="" style="margin-top: 19px;height: 113px;"></textarea>
				   	
				   	<button class="btn btn-primary" name="mysubmit" type="submit" style="padding: 8px;margin-bottom: 17px;font-size: 17px;margin-top: 11px;background-color: #ff3d00;border-color: #ff3d00;padding: 9px 0px;float: left;width: 100%;background: #cc3300;color: white;text-align: center;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;font-family: 'Roboto', sans-serif;">Submit</button>
			    </form>
			</div>

	
		<div class="imgmap contactsd" style="float:left;width: 100%;">
				<h1>The<b style="color: #f1592a;font-weight: 400;"> Team</b></h1>
				
				<p><b>Ticket & Tour Questions:</b> <br>Email: taco@cancunfoodtours.com <br>Tel: +52 1 998-887-42-42<br><br>


				<img src="https://cancunfoodtours.com/img/mapa14.jpg" style="float:left;width: 100%;">
				<p><b>ADDRESS:</b> Cancun Food Tours Carretera a Punta Sam Mza 2 34, SM 86 Punta Sam, Cancun, C.P. 77520</p>
			</p>
				
		</div>
		</div>
			</div>
	</div>
	</div>
<?php 	include('include/footer.php'); ?>
</body>
</html>