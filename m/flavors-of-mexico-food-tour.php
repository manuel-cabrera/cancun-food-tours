<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Flavors of Mexico Food Tour in Cancun | CancunFoodTours.com</title>
	<style type="text/css">
		.eeee {
			float: left;
			color: #000;
			width: 90%;
			list-style-type: none;
			margin: 0;
			margin-left: 0;
			-webkit-margin-after: 0em;
			-webkit-margin-before: 0em;
			-webkit-padding-start: 28px;
			line-height: 27px;
			font-size: 16px;
		}
	</style>
	<?php include('include/nuevolib.php'); ?>
	<link rel="stylesheet" href="https://m.cancunfoodtours.com/css/swiper.min.css">
</head>
<body>
<?php include('include/menu.php'); ?>
	<div class="contenedormovil">
		<div style="float:left; width:100%;">
			<div class="textodentro" style="text-align: center;"><h1 style="border-top: 1px solid black;border-bottom: 1px solid black;    margin-bottom: 10px;">Flavors of Mexico Premium Experience</h1></div>
			<div class="priva" style="float: left;width: 100%;">
							<div class="swiper-container">
			    <div class="swiper-wrapper">
			      <div class="swiper-slide"><img src="https://cancunfoodtours.com/img/cancunfoodtour.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/cancunfoodtour2.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/cancunfoodtour3.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/cancunfoodtour4.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/cancunfoodtour5.jpg" style="float: left;width: 100%;"></div>
			      	
			    </div>
			    <!-- Add Arrows -->
			    <div class="swiper-button-next"></div>
			    <div class="swiper-button-prev"></div>
			</div>
				
			</div>
			<div style="float:left;width:100%;background: #f3eedb;">
				<div style="float:left;width:100%;"><p style=" font-size: 18px;color: #ff9966;float: left;width: 100%;font-size: 26px;
	    margin-bottom: 8px;margin-top: 8px;">HIGHLIGHTS</p></div>
				<p style="float: left;width: 100%;margin-bottom: 10px;font-size: 19px;">

					<ul class="eeee" style="list-style-type: square;">
						<li>Roundtrip transportation included.</li>
						<li>Friendly english speaking local guide.</li>
						<li>Savour delicious food and drink that will get the best out of your inner mexican foodie right away.</li>
						<li>Enjoy 8 handpicked plates chosen by our staff.</li>
						<li>3 Tequila, mezcal and Xtabentun (mayan drink) tasting.</li>
						<li>Get access to the best restaurants skipping the line to a private reserved table.</li>
						<li>Get excluvise discounts to the best places to eat in Cancun throughout our partnerships.</li>
					</ul>
				</p>

			</div>
			<div class="boton">
					  	<a href="https://m.cancunfoodtours.com/bookings/1"> BOOK NOW</a>
			</div>
		</div>
		<div style="float:left; width:100%;font-size: 18px;">
			<div style=" padding: 0px 10px;">
				<h2 style=" margin-bottom: 15px;text-align: center;">TOUR <b style="color: #ff6633;">INFORMATION</b></h2>
				<div style="float: left;width: 100%;margin-bottom: 27px;">
				<ul class="eeee" style="list-style-type: square; -webkit-padding-start: 21px;">
					<li>When:<br> Wednesdays & Saturdays at 5:00pm</li>
					<li>How Much:<br>$79.00 USD</li>
					<li>Sales Deadline:<br>Tickets must be purchased before 10am for the selected date</li>
					<li>Capacity:<br>10 people per tour.</li>
					<li>What to wear:<br>Comfortable clothing and shoes.</li>
					<li>Who: <br>Minimum age 12+. For Alcohol tasting adult must be 18+ with a valid ID.</li>
				</ul>
				</div>
			</div>
		</div>
		<div style="float:left; width:100%;">
		<div class="todotres">
			<div style=" padding: 0px 10px;">
				<h3 style=" margin-bottom: 10px;">WHAT TO EXPECT FROM THE AUTHENTIC MEXICAN FOOD TOUR:</h3>
				<p style="float: left;width: 100%">Try an amazing selection of 7 local plates and 3 Mexican spirits tasting in the best local restaurants
					in Cancun Mexico. Taste a piece of history &amp; culture on each dish &amp; drink! Feed your inner foodie
					visiting 4 unique local places. and get to know the real taste of Mexico. Skip the line and expect a
					reserved table in every stop.<br><br>

					Start your experience with a prompt and comfortable hotel pick up. Meet our tour guide, who will be
					the ambassador for everything related to the food, drinks and the story behind each spot where the
					tour will take place. During the tour, we will ride through the main streets of Downtown Cancun to
					catch a glimpse and learn about culture, history and architecture of Cancun.<br><br>
					Our 4 different stops:</p>
				<ul class="eeee" style="list-style-type: square;">
					<li> Caribbean Sea food: Fresh fish ceviche &quot;tostada&quot; and regional style shrimp &quot;empanada&quot;.</li>
					<li>Local food from Yucatan Peninsula: Pibil style pork &quot;Panucho&quot; and delicious pumpkin seed and
egg &quot;Papadzul&quot;.</li>
					<li> Tacos &amp; food from Central Mexico: The world famous taco al pastor with cheese &quot;Gringa&quot;, and
juicy Arrachera steak Taco.</li>
					<li> Dessert at a local mexican park: Mexican creppe &quot;Marquesita&quot; with your favourite topping</li>
				</ul>
				<p style="float: left;width: 100%">
					Beside the main dishes, a wide variety of sides and sauces such as Guacamole, pico de gallo &amp;
					chips and nopales (for listing some) will be offered.<br><br>
					We have selected a traditional drink for each stop (the best match for your food): Refreshing
					cocktails, each one prepared with local fruits and Tequila, Mezcal and Xtabentun liquor. You are free
					to pass on our recommendations and choose your favourite drink. 1 drink is included in every stop.<br><br>
					Besides the culinary experience, live memorable moment like being sang by the mariachi, life at a
					traditional park and mexican games.
				</p>
			</div>
			</div>
			<div class="todotres">
				<div style=" padding: 0px 10px;">
					<h3 style="margin-bottom: 10px;float: left;width: 100%;margin-top:  10px;text-align: center;">WHY TAKE A TOUR WITH CANCUNFOODTOURS.COM?</h3>
					<p>Food is all about sharing the experience, our tours will allow you to connect people & food on a deeper level, by tasting new flavours & experiences, learning about the diverse plates that mexican chef’s have created influenced by our ancestor’s recipees.</p>
				</div>
			</div>

			<div class="boton">
				<a href="https://m.cancunfoodtours.com/bookings/1"> BOOK NOW</a>
			</div>
		</div>
		<div style=" padding: 0px 14px;">
			<?php 
				include('../includes/comentarios.php');
			?>
		</div>
	</div>
	
<?php 
	include('include/footer.php');
?>
<script src="https://m.cancunfoodtours.com/css/swiper.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>