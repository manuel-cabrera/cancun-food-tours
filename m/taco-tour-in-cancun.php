<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>The Most Authentic Taco Tour in Cancun | CancunFoodTours.com</title>
	<style type="text/css">
		.eeee {
			float: left;
    		color: #000;
    		width: 90%;
    		list-style-type: none;
    		margin: 0;
    		margin-left: 0;
    		-webkit-margin-after: 0em;
    		-webkit-margin-before: 0em;
    		-webkit-padding-start: 28px;
    		    line-height: 27px;
    		    font-size: 16px;
		}

	</style>
	<?php include('include/nuevolib.php'); ?>
	<link rel="stylesheet" href="https://m.cancunfoodtours.com/css/swiper.min.css">
</head>
<body>
<?php include('include/menu.php'); ?>
	<div class="contenedormovil">
		<div style="float:left; width:100%;">
			<div class="textodentro" style="text-align: center;"><h1 style="border-top: 1px solid black;border-bottom: 1px solid black;    margin-bottom: 10px;">The Most Authentic Taco Tour in Cancun</h1></div>
			<div class="priva" style="float: left;width: 100%;">
							<div class="swiper-container">
			    <div class="swiper-wrapper">
			      <div class="swiper-slide"><img src="https://cancunfoodtours.com/img/taco-tour-cancun-1.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/taco-tour-cancun-2.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/taco-tour-cancun-3.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/taco-tour-cancun-4.jpg" style="float: left;width: 100%;"></div>
			      	<div class="swiper-slide"><img src="https://cancunfoodtours.com/img/taco-tour-cancun-5.jpg" style="float: left;width: 100%;"></div>
			      	
			    </div>
			    <!-- Add Arrows -->
			    <div class="swiper-button-next"></div>
			    <div class="swiper-button-prev"></div>
			</div>
				
			</div>
			<div style="float:left;width:100%;background: #f3eedb;">
				<div style="float:left;width:100%;"><p style=" font-size: 18px;color: #ff9966;float: left;width: 100%;font-size: 26px;
	    margin-bottom: 8px;margin-top: 8px;">HIGHLIGHTS</p></div>
				<p style="float: left;width: 100%;margin-bottom: 10px;font-size: 19px;">

					<ul class="eeee" style="list-style-type: square;">
						<li>Skip the line, best taco joints are crowded. Not if you ride with us!</li>
						<li>Enjoy 5 different kind of tacos. We have handpicked only the best for you</li>
						<li>Beer tasting! Nothing goes better with tacos than Mexican (local) beers</li>
						<li>Roundtrip transportation included</li>
						<li>Friendly English speaking local guide.</li>
						<li>Get exclusive discounts with other restaurants in Cancun</li>
					</ul>
				</p>

			</div>
			<div class="boton">
					  	<a href="https://m.cancunfoodtours.com/bookings/2"> BOOK NOW</a>
			</div>
		</div>
		<div style="float:left; width:100%;font-size: 18px;">
			<div style=" padding: 0px 10px;">
				<h2 style=" margin-bottom: 15px;text-align: center;">TOUR <b style="color: #ff6633;">INFORMATION</b></h2>
				<div style="float: left;width: 100%;margin-bottom: 27px;">
				<ul class="eeee" style="list-style-type: square; -webkit-padding-start: 21px;">
						<li>When:<br> Tuesdays & Friday at 5:00pm</li>
						<li>How Much:<br>$60.00 USD</li>
						<li>Sales Deadline:<br>Tickets must be purchased before 10am for the selected date</li>
					<li>Capacity:<br>10 people per tour.</li>
					<li>What to wear:<br>Comfortable clothing and shoes.</li>
						<li>Who: <br>Minimum age 12+. For Alcohol tasting adult must be 18+ with a valid ID.</li>
				</ul>
				</div>
			</div>
		</div>
		<div style="float:left; width:100%;">
		<div class="todotres">
			<div style=" padding: 0px 10px;     line-height: 22px;">
				<h3 style=" margin-bottom: 10px;    font-size: 17px;">We know we had you from "taco tour", which is the closest to what food heaven would look like, but keep reading to get the complete tour details.</h3>
				<p>Prepare yourself to devour delicious traditional tacos, enjoy refreshing local beers, meet foodies such as you from all over the world, delight beautiful bright colors from the real Cancun, learn to do hand-made tortillas and listen an authentic “Mariachi” band live.<br>
				If you have only tried “taco bells”, you better be ready to get mind-blown with our personal selection of the best Cancun tacos.<br><br>
				Start your experience with a prompt and comfortable hotel pick up.<br>
				Meet your friendly English speaking local guide, who will escort you through the whole tour.<br><br>
				Our tour feature 5 stops (expect to skip the line and a reserved table in each spot):<br>

				<ul class="eeee" style="list-style-type: square;">
						<li>Trendy "street style" taco joint: "Mexico City" style taco.</li>
						<li>Yucatecan cuisine restaurant: Pulled pork marinated "Pibil" style.</li>
						<li>Family owned and run taco shop: "Surtido" style taco, the perfect beef mix.</li>
						<li>Iconic Mexican restaurant (+40 years open): The world famous "Taco al pastor" and steak taco.</li>
						<li>Local park: Try the most traditional Mexican dessert, the "Churro".</li>
				</ul>
				</p>

				<p style="float: left;width: 100%;"> <b>Guacamole,</b> beans, pico de gallo, nopales (cactus) and a wide variety of sauces are offered as sides. It’s worth to mention that you should come hungry, there’s so much to taste!</p><br><br>
				<p style="float: left;width: 100%;">4 refreshing beers are included (1 in each stop). Select yours between more than 15 types of mexican beer. If you’re not into beer, natural flavored wáter and soft drinks are also available.</p><br>
				<p>Learn about the city’s history, the story behind each spot we’re visiting and interesting facts about prehispanic ingredients we still use in Mexican cuisine (and that you’ll try).<br><br>
				With more than 80 different “taco shops” around Cancun, it’s very easy to get stuck in a regular to bad place, or even worse, it’s too easy to get discouraged after realizing the ton of time you need to spend to plan and prepare to make sure that going out from your resort it’s going to be worth it.<br>
				For your luck, we’ve done the hard work for you and we have everything covered to guarantee you a memorable experience.
				</p>
				<p style="float: left;width: 100%; margin-top: 5px; margin-bottom:5px; font-weight: 600;">The glory of a good taco is worth its weight in gold, dare to weigh it yourself, book your tour now!</p>
			</div>
			</div>
			<!--<div class="todotres">
				<div style=" padding: 0px 10px;">
					<h3 style="margin-bottom: 10px;float: left;width: 100%;margin-top:  10px;text-align: center;">WHY TAKE A TOUR WITH CANCUNFOODTOURS.COM?</h3>
					<p>Food is all about sharing the experience, our tours will allow you to connect people & food on a deeper level, by tasting new flavours & experiences, learning about the diverse plates that mexican chef’s have created influenced by our ancestor’s recipees.</p>
				</div>
			</div>-->

			<div class="boton">
				<a href="https://m.cancunfoodtours.com/bookings/2"> BOOK NOW</a>
			</div>
		</div>
	</div>
						<div style=" padding: 0px 14px;">
						<?php 
							include('../includes/comentarios.php');
						?>
					</div>
<?php 
	include('include/footer.php');
?>
<script src="https://m.cancunfoodtours.com/css/swiper.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>