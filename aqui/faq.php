<!DOCTYPE html>
<html>
<head>
	<title>Email and Phone Contact for CancunFoodTour.com</title>
<meta name="description" content= "Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
<META NAME="Keywords" CONTENT="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
	<link rel="alternate" hreflang="x-default" href="https://cancunfoodtours.com/">	
	<link rel="canonical" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en-US" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
	<?php include('booking/lib/controllib.php'); ?>
	<script src="https://cancunfoodtours.com/aqui/js/modernizr.js"></script>
	<style type="text/css">
	body{
		font-size: 16px;
	}
		.faqs{
			float: left;
			width: 100%;
		}
		.primeroh1{
			float: left;width: 100%;
		}
		.primeroh1 h1{
			font-size: 22px;
		}
		.contfaq{
			float: left;width: 100%;
		}

		details{
    		/* border: 1px solid #CC0; */
    		padding: 7px;
    		/* font-family: Arial; */
    		margin-bottom: 5px;
    		/* width: 500px; */
    		background-color: #EEE;
		}
		summary{
		  cursor: pointer;
		}
		p, a, span{
			color: black;
    		padding-left: 22px;
    		margin-top: 10px;
		}
		details summary::-webkit-details-marker {
		  color: #CC0;
		}
	</style>
</head>
<body>
<?php include('includes/menu.php');  ?>
<div class="todocont" >
	<div class="contenedor" >
		<div class="faqs" >
			<div class="primeroh1" ><h1>Frequently Asked Questions</h1></div>
			<div class="contfaq" >
				<details>
				   <summary>1. Is transportation included in the price of the tour?</summary>
				   <p>Yes, all our food tours include pick up/drop off from your hotel to the different restaurants that will be included in your tour. We provide private vans with A/C, they’re comfortable enough for the whole party, and are driven by a certified driver.</p>
				</details>

				<details>
				   <summary>2. Can I cancel/reschedule my food tour?</summary>
				   <p>We allow to schedule changes only if<a href="https://cancunfoodtours.com/">Cancunfoodtours.com</a> is provided with a 24-hour notice before the date of your tour. For full refund cancellations, we require <a href="https://cancunfoodtours.com/">Cancunfoodtours.com</a> to be notified 7 days in advance. If the tour is cancelled before a 24-hour notice, only a 50% refund will take place. Unfortunately, if the tour is cancelled after that 24-hour mark, we can’t provide a refund.</p>
				</details>

								<details>
				   <summary>3. Is alcohol served during any of the Cancun Food Tours?</summary>
				   <p>Yes, alcohol is part of Mexican gastronomy and we feel it should be considered as part of the experience. However, for underage and people who do not wish to drink alcohol, we have non-alcoholic options on all the beverages. Additionally, water is served throughout all our tours.</p>
				</details>

								<details>
				   <summary>4. Are kids allowed to go to the tours?</summary>
				   <p>Our tours are mostly made for adults 18+ and we strongly recommend to respect the restriction since we want the tour to be pleasant to all our guests. Usually kids can have shot attention span and maybe a food tour may not be appropriate for them.</p>
				</details>

								<details>
				   <summary>5. Are some of the plates spicy?</summary>
				   <p>Mexican food is full of flavor and some of our plates are made with some spices that might be considered strong. However, our chefs have prepared less-mild versions of each plate to avoid any unpalatable flavors that can be enjoyable for everyone without any risk.</p>
				</details>

				<details>
				   <summary>6. Are there any vegan options?</summary>
				   <p>Yes, when booking your tour, please select how many vegan menus you would like to book so the plates are considered in the mix.</p>
				</details>

								<details>
				   <summary>7. What allergies and preferences are to be considered in the cancun food tours?</summary>
				   <p>We can provide substitutions for vegetarians & spicy food considerations as we’ve explained. However, we cannot provide substitutions for any other allergies or dietary preferences. Allergies and preferences must be indicated at the time of ticket purchase. Please notify us of any serious allergies so we can tell you which food(s) to avoid.</p>
				</details>

								<details>
				   <summary>8. Are the restaurants within Walking Distance from each other?</summary>
				   <p>Some of the restaurants are between a 15-min walking distance of each other. Which is why we providing the transportation for the entirety of the tour. This will make the experience more comfortable.</p>
				</details>

								<details>
				   <summary>9. What are the current payment methods?</summary>
				   <p>Currently we accept Paypal payments, money requests, and credit cards</p>
				</details>

						<details>
				   <summary>10. How much food is provided in the tour?</summary>
				   <p>All our tours are designed and tested to provide a complete meal for an average adult. Most participants will feed full after our combination of drinks, and food tastings along with breaks and introductions from our tour guide. Our tours are mostly made of 8 different small plates and also + 2 Alcohol tasting</p>
				</details>

				<details>
				   <summary>11. Are cameras allowed on the tour?</summary>
				   <p>Yes, cameras are allowed on the tour. We encourage foodies to share the experience and take photos during our tour. We would love you sharing the experience on networks as TripAdvisor, Facebook or Yelp.</p>
				</details>
			</div>

		</div>
	</div>
</div>
<?php include('includes/footer.php');  ?>
</body>
</html>