<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<title> Official Taco Tour in Cancun | CancunFoodTours.com</title>
	<meta name="description" content= "Are tacos your thing? Prepare yourself to eat authentic mexican tacos with our tour. We’ve selected the best taco joints in Cancun for you to dive deep into delicious flavors.">
	<META NAME="Keywords" CONTENT="Taco tours, best tacos in cancun, street tacos in cancun, best taco stand in cancun, best tacos in hotel zone, taco factory cancun">

	<link rel="stylesheet" type="text/css" href="https://cancunfoodtours.com/aqui/css/estilo.css">
	<link rel="stylesheet" href="https://cancunfoodtours.com/aqui/css/flexslider.css" type="text/css" media="screen" />
	<script src="https://cancunfoodtours.com/aqui/js/modernizr.js"></script>

		<script type="text/javascript" src="https://cancunfoodtours.com/aqui/js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="https://cancunfoodtours.com/aqui/js/jquery.opacityrollover.js"></script>
		<!-- We only want the thunbnails to display when javascript is disabled -->
		<script type="text/javascript">
			document.write('<style>.noscript { display: none; }</style>');
		</script>

</head>
<body>
<?php include('includes/menu.php');  ?>
<div class="todocont" >
	<div class="contenedor" >
		<div class="fotos1" >
			<h1>The Most Authentic Taco Tour in Cancun</h1>
			<p>Lorem ipsum dolor sit amet, gravida ante dolor vestibulum.</p>
				<div id="gallery" class="content">
					<div class="slideshow-container">
						<div id="loading" class="loader"></div>
						<div id="slideshow" class="slideshow"></div>
					</div>
					<div id="caption" class="caption-container"></div>
				</div>
				<div id="thumbs" class="navigation">
					<ul class="thumbs noscript">
						<li>
							<a class="thumb" name="leaf" href="https://cancunfoodtours.com/aqui/img/taco1.jpg" title="Title #0">
								<img src="https://cancunfoodtours.com/aqui/img/taco1.jpg" alt="Title #0" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #0</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" name="drop" href="img/taco2.jpg" title="Title #1">
								<img src="img/taco2.jpg" alt="Title #1" />
							</a>
							<div class="caption">
								Any html can be placed here ...
							</div>
						</li>

						<li>
							<a class="thumb" name="bigleaf" href="https://cancunfoodtours.com/aqui/img/taco3.jpg" title="Title #2">
								<img src="https://cancunfoodtours.com/aqui/img/taco3.jpg" alt="Title #2" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #2</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" name="lizard" href="https://cancunfoodtours.com/aqui/img/taco4.jpg" title="Title #3">
								<img src="https://cancunfoodtours.com/aqui/img/taco4.jpg" alt="Title #3" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #3</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco5.jpg" title="Title #4">
								<img src="https://cancunfoodtours.com/aqui/img/taco5.jpg" alt="Title #4" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #4</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco6.jpg" title="Title #5">
								<img src="https://cancunfoodtours.com/aqui/img/taco6.jpg" alt="Title #5" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #5</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco7.jpg" title="Title #6">
								<img src="img/taco7.jpg" alt="Title #6" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #6</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco8.jpg" title="Title #7">
								<img src="https://cancunfoodtours.com/aqui/img/taco8.jpg" alt="Title #7" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #7</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco9.jpg" title="Title #8">
								<img src="https://cancunfoodtours.com/aqui/img/taco9.jpg" alt="Title #8" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #8</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						<li>
							<a class="thumb" href="https://cancunfoodtours.com/aqui/img/taco10.jpg" title="Title #9">
								<img src="img/taco10.jpg" alt="Title #9" />
							</a>
							<div class="caption">
								<div class="download">
									<a href="#">Download Original</a>
								</div>
								<div class="image-title">Title #9</div>
								<div class="image-desc">Description</div>
							</div>
						</li>

						
					</ul>
				</div>
		</div>
		<div class="intour1" >
			<div class="intour2" >
				<div class="intour4" >
					<div class="intour3" >
						<span>HIGHLIGHTS</span>
						<p><img src="img/cuca1.png" class="cuca1" >Indulge in a variety of delicious food and drink that will satisfy both your inner foodie & mixologist.</p>
						<p><img src="img/cuca1.png" class="cuca1" >Explore the variety of food cultures present in Chicago. Eat around the world while only walking a few blocks.</p>
						<p><img src="img/cuca1.png" class="cuca1" >Beat the lines and skip the wait at popular Chicago restaurants with our VIP access.</p>
						<p><img src="img/cuca1.png" class="cuca1" >Get exclusive restaurant discounts only available through Chicago Food Planet.</p>
					</div>
					<div class="intbajo" >
						<p>ADULT PRICE (18+ - NO ALCOHOL/MOCKTAILS): $59.00 USD</p>
						<p>ADULT PRICE (21+ - ALCOHOL INCLUDED): $59.00 USD</p>
						<p>MONTHS OFFERED:</p>
						<p>December-March</p>
						<p>DEPARTURE TIMES:</p>
						<p>Thurs-Fri at 5:30pm & 4pm Sat & Sun</p>
						<p>DURATION & DISTANCE:</p>
						<p>2 Hours covering .4 miles</p>
						
						<div class="boton" style="margin-top: 13px;margin-bottom: 18px;float: left;width: 100%;">
					  	    	<a href="">BUY TICKETS</a>
					  	</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tourinfo" >
			<h2>TOUR INFORMATION</h2>
			<div class="tourinfo1" >
				<span>WHEN:</span>
				<p>Thursday-Sunday, December through March.</p>
				<span>TIME:</span>
				<p>Thurs-Fri; 5:30pm, 6pm | Sat-Sun; 4pm, 4:30pm, 5pm & 5:30pm</p>
				<span>HOW MUCH:</span>
				<p>$59.00 USD</p>
				<span>SALES DEADLINE:</span>
				<p>Tickets must be purchased before 10am for today's tour(s).</p>
				<span>WHO:</span>
				<p>Adults only. 18+ with non-alcoholic tastings. 21+ with alcohol.</p>
				
				<div class="boton" style="margin-top: 13px;margin-bottom: 18px;float: left;width: 257px;">
					<a href="">BUY TICKETS</a>
				</div>
			</div>
			<!-- Primero de los Primeros -->
			<div class="tourinfo1" >
				<span>CAPACITY:</span>
				<p>12 people per tour.</p>
				<span>WHAT TO WEAR:</span>
				<p>Warm, comfortable clothing and shoes.</p>
				<span>WEATHER CONDITIONS:</span>
				<p>Tour takes place rain, snow or shine.</p>
				<span>MEETING POINT:</span>
				<p>Meets near North & Damen Ave (Exact meeting location provided with ticket purchase).</p>
			</div>
		</div>
		<div class="todoinfo1" >
			<div class="todotres" >
				<h3>WHAT TO EXPECT FROM THE WORLD’S FARE:</h3>
				<p>Welcome to Cancun Food Tour, the home of the Taco Tour. Explore downtown in search of the best street tacos in Cancun for a unique Taco experience. We will take you to the most delicious tacos stands in town. Local´s have identified these spots as true taco joints for an authentic mexican dinner. </p>
			</div>
			<div class="todotres" >
				<h4>Why enjoy Tacos with Cancunfoodtours.com?</h4>
				<p>Downtown Cancun is where the locals eat, so we’ll take you there to eat authentic Tacos. Tacos are one of the best foods you can experience in Cancun Mexico. Our Cancun Taco Tour will allow you to connect people & food on a deeper level, by tasting and learning about the plate that most residents eat and are proud to share. Get ready for a unique experience, save your spot & Book Now!</p>
				<div class="boton" style="margin-top: 13px;margin-bottom: 18px;float: left;width: 257px;">
					<a href="">BUY TICKETS</a>
				</div>
			</div>

		</div>
		<?php  include('includes/coment.php'); ?>
	</div>
</div>
		 <script src="https://cancunfoodtours.com/aqui/js/jquery.min.js"></script>
	   <script defer src="https://cancunfoodtours.com/aqui/js/jquery.flexslider.js"></script>
	       <script type="text/javascript" src="https://cancunfoodtours.com/aqui/js/jquery.galleriffic.js"></script>

    <!-- Optionally include jquery.history.js for history support -->
    <script type="text/javascript" src="https://cancunfoodtours.com/aqui/js/jquery.history.js"></script>
    <script type="text/javascript" src="https://cancunfoodtours.com/aqui/js/jquery.opacityrollover.js"></script>
	   <script>window.jQuery || document.write('<script src="https://cancunfoodtours.com/aqui/js/libs/jquery-1.7.min.js">\x3C/script>')</script>
	<script type="text/javascript">
	$(document).ready(function() {
		// Initially set opacity on thumbs and add
		// additional styling for hover effect on thumbs
		var onMouseOutOpacity = 0.67;
		$('#thumbs ul.thumbs li').opacityrollover({
			mouseOutOpacity:   onMouseOutOpacity,
			mouseOverOpacity:  1.0,
			fadeSpeed:         'slow',
			exemptionSelector: '.selected'
		});

		// Initialize Advanced Galleriffic Gallery
		var gallery = $('#thumbs').galleriffic({
			delay:                     4000,
			numThumbs:                 24,
			preloadAhead:              12,
			enableTopPager:            true,
			enableBottomPager:         false,
			enableKeyboardNavigation:  false,
			imageContainerSel:         '#slideshow',
			captionContainerSel:       '#caption',
			loadingContainerSel:       '#loading',
			autoStart:                 false,
			syncTransitions:           true,
			defaultTransitionDuration: 900,
			onSlideChange:             function(prevIndex, nextIndex) {
				// 'this' refers to the gallery, which is an extension of jQuery('#thumbs')
				this.find('ul.thumbs').children()
					.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
					.eq(nextIndex).fadeTo('fast', 1.0);
			},
			onPageTransitionOut:       function(callback) {
				this.fadeTo('fast', 0.0, callback);
			},
			onPageTransitionIn:        function() {
				this.fadeTo('fast', 1.0);
			}
		});
	});
</script>
<?php include('includes/footer.php');  ?>
</body>
</html>