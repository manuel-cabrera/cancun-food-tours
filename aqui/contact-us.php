<!DOCTYPE html>
<html>
<head>
	<title>Email and Phone Contact for CancunFoodTour.com</title>
<meta name="description" content= "Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
<META NAME="Keywords" CONTENT="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
	<link rel="alternate" hreflang="x-default" href="https://cancunfoodtours.com/">	
	<link rel="canonical" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en-US" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
	<?php include('booking/lib/controllib.php'); ?>
	<script src="https://cancunfoodtours.com/aqui/js/modernizr.js"></script>
	<style type="text/css">
		.contactsd{
			float: left;
			width: 100%;
		}
		.contactsd h1{
		    float: left;
		    width: 100%;
		    font-family: free;
		    font-size: 21px;
		}
		.contform{
			float: left;
			width: 100%;
		}
		.contenidoqw{
			float: left;
			width: 450px;
		}
	</style>
</head>
<body>
<?php include('includes/menu.php');  ?>
<div class="todocont" >
	<div class="contenedor" >
			<div class="intdieter" style="margin-top: 109px;">
		<!--<div class="primerosse" id="neno"><h1>Welcome to Cancun Food Tours</h1></div>-->
			<div class="seo1">
				<div class="seoint" >
					<h2><b style="color: black;font-weight: 400;">Why eat with</b><br>Cancunfoodtours.com?</h2>
					<p>Welcome to Cancunfoodtour.com, the home to discover one of the most incredible mexican <strong>gastronomic experiences in Cancun</strong>. Cancunfoodtour.com is a professionaly guided tour of the local food and beverage scene which has become one of the best <strong>things to do in Cancun</strong>. Made for foodies & food enthusiasts (or everyone, really) that are looking to indulge in mexican flavors and cultural experiences.<br><br>

						Wonderful <strong>Cancun gastronomy</strong> is Mexico's gift to the world, and Mexicans take cooking very seriously, our goal is to provide <strong>Cancun’s local cuisine</strong> as part of your travel itinerary so you can experience mexican food as an insider, and to explore food safely and with convenience. Our restaurants have the highest standards of quality and service so you’re guaranteed the best <strong>dining experience in Cancun</strong>. Save your spot, Book Now!
					  </p>
				</div> 
				<div class="seoint1" >
					<img src="https://cancunfoodtours.com/img/zona-foot.jpg">
				</div>
			</div>
		<!-- fin incio -->

		</div>
		<div class="contenidoqw">
			<div class="contactsd" >
				<h1>Contact Us Cancun Food Tour</h1>
				<p>Be sure to review our Frequently Asked Questions. For additional support, please use the form below to send us a general inquiry.</p>
			</div>
			<div class="contform" >
				<form action="https://cancunfoodtours.com/mailer.php" method="POST" id="contactus" class="form-cont">
				   	<span class="title"></span>
				   	<input type="text" name="nombre" placeholder="Name" class="form-control" title="Introduce tu nombre porfavor" maxlength="50" required="">
				   	<input type="email" name="email" placeholder="Email" class="form-control" title="Introduce tu correo electronico" required="" style="margin-top: 19px;">
				   	<textarea name="mensaje" placeholder="Write your message" class="form-control" title="Introduce tu mensaje" required="" style="margin-top: 19px;height: 113px;"></textarea>
				   	
				   	<button class="btn btn-primary" name="mysubmit" type="submit" style="padding: 8px; margin-bottom: 17px;font-size: 17px; margin-top: 11px; background-color: #ff3d00; border-color: #ff3d00;">Processing to Question</button>
			    </form>
			</div>

		</div>
		<div class="imgmap" style="float:left;width: 450px;margin-left: 57px;margin-bottom: 40px;"  >
		<p><b>ADDRESS:</b> Cancun Food Tours Carretera a Punta Sam Mza 2 34, SM 86 Punta Sam, Cancun, C.P. 77520</p>
		<p><b>Cuntact Us:</b> Luis Garcia Jurado CEO, Dieter Ordonez CMO, support@cancunfoodtours.com</p>
				<img src="https://cancunfoodtours.com/aqui/img/mapa14.jpg">
				
			</div>
	</div>
</div>

<?php include('includes/footer.php');  ?>

</body>
</html>