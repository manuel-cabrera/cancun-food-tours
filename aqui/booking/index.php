<?php 

session_start();
include('lib/conexion.php');  ?>

<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Booking Information | CancunFoodTours.com</title>

	<link rel="alternate" hreflang="x-default" href="https://cancunfoodtours.com/">	
	<link rel="canonical" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en-US" href="https://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
	<?php include('lib/controllib.php'); ?>
	<script src="https://cancunfoodtours.com/aqui/js/modernizr.js"></script>
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<style type="text/css">
	.cont1{	width: 960px;margin: 0 auto;}
	.book{width: 600px;float: left;}
	.intform{float: left;width: 100%;}
	.intform1{float: left;}
	.numerot{float: left;width: 100%;margin-bottom: 13px;}
	.numeros{float: left;}
	.medio{float: left;	width: 250px;}
	label{	color: #31708f;    font-weight: 100;	}
	.titulo{ float: left; width: 100%;background-color: #e5e5e5;   margin-bottom: 15px;}
	.controlinput{float: left;width: 100%;    margin-bottom: 20px;}
	.peque{  float: left; width: 290px;}
	.book2{    float: left;  width: 350px; background: #e5e5e5; margin-left: 8px;}
	.bookcont{ float: left;   padding: 7px 15px;  width: 100%;  padding: 7px 6px;   background: #f2f2f2;  box-shadow: 0px 0px 3px 0px #7C7473; padding: 7px 17px;}
    .resumencontrol{float: left; width: 100%;}

    .contenido{	float: left;  	width: 100%;   	margin-bottom: 14px;   }
    .contieneotro{ 	float: left;  	width: 100%;   	margin-bottom: 20px;    }
    .control1{float: left;width: 100%;     margin-top: 25px;}
	</style>

<?php include('lib/controlscrip.php'); ?>
</head>
<body onload="timer()">
<?php include('../includes/menu.php');  ?>

	<div class="cont1" >
	<div class="contieneotro">
	<div class="book" >
		<div class="titulo" >
			<span style="float: left;width: 100%;text-align: center;font-size: 20px;padding: 5px 0px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;color: #000000;">New Booking</span>
		</div>
			<div class="intform" >
				<div class="inform1">
					
					<div class="controlinput" >
						<form action="lib/paypal.php" method="POST" id="pago-con-paypal">
							<div class="peque" style="    margin-right: 10px;">
								<label style="float:left; width:100%; font-size: 17px;">Select Date</label>
								<div class="medio" >	
									<input type="text" name="datosfecha" id="datepicker" class="form-control" required>
								</div>
							</div>
							<div class="peque" style="    margin-right: 10px;">
								<label style="float:left; width:100%; font-size: 17px;">Select Time</label>
								<div class="medio" >	
									<select name="tiempo" class="form-control" style=" width: 58%; float:left;" id="tiempo" onclick="timer()" required>
									<option>Select Time</option>
										<?php 
											$controltiempo = mysql_query("SELECT * FROM tiempo");
											while($tiempo = mysql_fetch_array($controltiempo)){
												$malo = $tiempo['hora'];

												echo "<option>".$malo."</option>";
											}

										?>
										
									</select>
								</div>
							</div>
						</div>


					<label style="font-size: 17px;" >Tickets</label>
						<?php 

							$result = mysql_query("SELECT * FROM tikect"); 
							while($row= mysql_fetch_array($result)){

								$cantidad = $row['numero'];
								$idtick = $row['idtikect'];
								$nombre = $row['nombre'];
								$precio = $row['precio'];
								$precio = number_format($precio, 2, '.', '');

							echo '<div class="numerot"> 
								<select name="numero'.$idtick.'" class="form-control" style=" width: 22%; float:left;" id="cantidad'.$idtick.'" onclick="controltotal()" onload="controltotal()" required>';
								echo "<option value=''>Select tikect</option>";
								for ($i=1; $i <= $cantidad ; $i++) { 
									echo "<option value='".$i."'>".$i."</option>";
								}
								echo "</select>";
								echo '<input type="hidden" value="'.$precio.'" id="precio'.$idtick.'" onload="controltotal()">';
								echo '<p style=" float: left; width: 300px; margin-left: 12px;     text-transform: capitalize; font-size: 17px; margin-top: 4px;">'.$nombre.' $<b id="calcula">'.$precio.'</b> USD </p></div>';
							}
						?>
							

						<div class="form2" >
								<div class="titulo" >
									<span style="float: left;width: 100%;text-align: center;font-size: 20px;padding: 5px 0px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;color: #000000;">Booking Information</span>
								</div>
								<div class="controlinput" >
									<div class="peque" style="    margin-right: 10px;">
									<label style="float:left; width:100%; font-size: 17px;">Firs Name</label>
										<input type="text" class="form-control" name="primernombre" id="primero" onclick="controlandoformulario()" required>
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">Last Name</label>
										<input type="text" class="form-control" name="segundonombre" id="segundo" onclick="controlandoformulario()" required>
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque" style="    margin-right: 10px;">
									<label style="float:left; width:100%; font-size: 17px;">Hotel Name</label>
										<input type="text" class="form-control" name="nombrehotel" id="nombreh" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">Room Number(Optional)</label>
										<input type="text" class="form-control" name="numerocuarto" id="numero4" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque" style="    margin-right: 10px;">
									<label style="float:left; width:100%; font-size: 17px;">Country</label>
										<input type="text" class="form-control" name="pais" id="pai" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
									<div class="peque" >
									<label style="float:left; width:100%; font-size: 17px;">State/Province</label>
										<input type="text" class="form-control" name="estado" id="esta" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque" style="    margin-right: 10px;">
										<label style="float:left; width:100%; font-size: 17px;">Address</label>
										<input type="text" class="form-control" name="address" id="dress" onclick="controlandoformulario()" >
									</div>
									<div class="peque" >
										<label style="float:left; width:100%; font-size: 17px;">Zip/Postal Code</label>
										<input type="text" class="form-control" name="codigopostal" id="codigos" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
								</div>

								<div class="controlinput" >
									<div class="peque" style="    margin-right: 10px;">
										<label style="float:left; width:100%; font-size: 17px;">Email</label>
										<input type="text" class="form-control" name="correo" id="email" onclick="controlandoformulario()" required>
									</div>
									<div class="peque" >
										<label style="float:left; width:100%; font-size: 17px;">Cell Phone</label>
										<input type="text" class="form-control" name="telefono" id="tele" onclick="controlandoformulario()" onload="controlandoformulario()">
									</div>
										
									<div class="control1" >
									<label>Extra Comments</label>
										<textarea class="form-control" name="extra" id="extra" onclick="controlandoformulario()" onload="controlandoformulario()"></textarea>
									</div>
								
								</div>
						</div>
				</div>
			</div>
		</div>
		<!-- Aqui esta otro -->

		<div class="book2" >
			<div class="bookcont" >
				<?php 

						$controlproduct = mysql_query("SELECT * FROM producto");
						while ($producto = mysql_fetch_array($controlproduct)) {
							# code...
							$nombre = $producto['nombre'];
						}

					  ?>
						<label style="font-size: 17px;width: 100%;text-align: center;">Payment Summary</label>

						<div class="resumencontrol" >
							<div class="contenido" >
								<span style="float: left;width: 100%;font-size: 18px;text-align: center;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;border-bottom: 1px solid #0e4c82;margin-bottom: 11px;"><?php echo $nombre; ?></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;" >Date: <b id="fecha">YYYY/MM/DD</b></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;">Time: <b id="vistatiempo">00:00 PM</b></span>
								<span style=" float: left; width: 100%;font-size: 15px; font-family: Helvetica Neue,Helvetica,Arial,sans-serif;    margin-bottom: 5px;">Tickets: <b id="adulto">0</b>x Adult, <b id="menor">0</b>x Kid</span>
								<span style="float: left;width: 100%;font-size: 15px;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;margin-top: 10px;text-align: right;font-weight: 700;">Total: $<b id="totalsuma">00.00</b> USD</span>
							</div>
						</div>
			</div>
		</div>
		<div class="book2" style="margin-top: 20px;" >
			<label style="font-size: 17px;width: 100%;text-align: center; margin-top: 15px;">You can pay with:</label>
			<img src="https://cancunfoodtours.com/aqui/img/icono.png" style=" float: left; width: 100%;padding: 9px 42px;">
		</div>

		<div class="book2" style="margin-top: 20px;" >
			<label style="font-size: 17px;width: 100%;text-align: center; margin-top: 15px;">Data Security:</label>
			<p style=" float: left; width: 100%; padding: 3px 20px;">Your info's safe with us. All data is encrypted and transmitted securely with an SSL protocol.<br><br>
				CancunFoodTours respects your privacy. We do not sell your personal information to anyone.</p>
		</div>

		<div class="book2" style="margin-top: 20px;" >
			<label style="font-size: 17px;width: 100%;text-align: center; margin-top: 15px;">Book with Confidence:</label>
			<p style=" float: left; width: 100%; padding: 3px 20px;">Peace of mind. Book ahead to skip the lines and reserve your spot.</p>
		</div>
		<div class="book2" style="margin-top: 20px;" >
			<label style="font-size: 17px;width: 100%;text-align: center;margin-top: 15px;color: #070d3a;font-weight: 700;">Total Price: $<b id="totalprice">00.00</b> USD</label>
			
				<input type="hidden" name="bookfecha" id="bookfecha" value="" >
				<input type="hidden" name="tiempo" id="tiempo" required >
				<input type="hidden" name="numerotikctsadulto" id="numerotikctsadulto" >
				<input type="hidden" name="numerotikctsmenor" id="numerotikctsmenor" >
				<input type="hidden" name="primernombre" id="primernombre">

				<input type="hidden" name="segundonombre" id="segundonombre">
				<input type="hidden" name="nombrehotel" id="nombrehotel">
				<input type="hidden" name="numerocuarto" id="numerocuarto">
				<input type="hidden" name="pais" id="pais">
				<input type="hidden" name="estado" id="estado">
				<input type="hidden" name="correo" id="correo">
				<input type="hidden" name="codigo" id="codigo">
				<input type="hidden" name="andres" id="andres">
				<input type="hidden" name="telefono" id="telefono">
				<input type="hidden" name="extra" id="extras">
				<input type="hidden" name="totalpri" id="totalpri">

				
				<input type="submit" class="btn btn-primary" style="padding: 8px; margin-bottom: 17px;margin-left: 74px;font-size: 17px; margin-top: 11px; background-color: #ff3d00; border-color: #ff3d00;" onclick="controlandoformulario()" value="Proceed to Payment">
			</form>
			<div id="messageBox"></div> 
			
		</div>
	</div>
	</div>

<?php include('../includes/footer.php');  ?>
<script src="https://cancunfoodtours.com/aqui/js/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#pago-con-paypal").validate({
		rules: {
		            timepo: { required: true },
		            primero: { required: true },
		            segundo: { required: true },
		            county: { required: true },
		            addres: { required: true },
		            phone: { required: true, digits:true },
		            mail: { required: true },
		            nombre: { required: true },
		            lastname: { required: true },
		            numerocard: { required: true, digits:true, maxlength:16 },
		            type: { required: true },
		            mes: { required: true, digits:true },
		            ano: { required: true, digits:true }, 
		            codigo: { required: true, digits:true, minlength:3, maxlength:3 }
		        },
		messages: {
		        	timepo:"This field is required: Country",
					primero: "This field is required: City",
					segundo:"This field is required: State",
					county:"This field is required: County", 
					zip: "This field is required: ZipCode",
					addres:"This field is required: Addres", 
					phone: "This field is required: Phone",
					mail:  "This field is required: E-mail",
					nombre: "This field is required: Name",
					lastname:"This field is required: Last Name",
					numerocard:"Invalid number Card",
					type:  "Slect Type Card",
					mes: "This field is required: Mes",
					ano: "This field is required: Year",
					codigo: "This field is required: CVV"
		        },
	errorLabelContainer: "#messageBox",
	wrapper: "p",
	submitHandler: function(form) {	
		var primernombre = $('#primernombre').val();
		var segundonombre = $('#segundonombre').val();
		var nombrehotel = $('#nombrehotel').val();
		var numerocuarto = $('#numerocuarto').val();
		var pais = $('#pais').val();
		var estado = $('#estado').val();
		var correo = $('#correo').val();
		var codigo = $('#codigo').val();
		var andres = $('#andres').val();
		var telefono = $('#telefono').val();
		var extra = $('#extras').val();
		var totalpri = $('#totalpri').val();
		var bookfecha = $('#bookfecha').val();
		var tiempo = $('#tiempo').val();
		var numerotikctsadulto = $('#numerotikctsadulto').val();
		var numerotikctsmenor = $('#numerotikctsmenor').val();
		//var email = $('#email').val();
		dataString = 'primernombre='+primernombre+'&segundonombre='+segundonombre+'&nombrehotel='+nombrehotel+'&numerocuarto='+numerocuarto+'&pais='+pais+'&estado='+estado+'&correo='+correo+'&codigo='+codigo+'&andres='+andres+'&telefono='+telefono+'&extra='+extra+'&totalpri='+totalpri+'&bookfecha='+bookfecha+'&tiempo='+tiempo+'&numerotikctsadulto='+numerotikctsadulto+'&numerotikctsmenor='+numerotikctsmenor;
		$.ajax({
			url: 'lib/paypal.php',
			data: dataString,
			dataType: 'json',
			type: 'POST',			
			success: function( response ){				
				$(location).attr('href', response.uri );
			},	
			/*beforeSend: function( html ){
			$('#messageBox').html("<li><img src='https://www.cancunrivieramaya.com/img/iconos/loading.gif' alt='Loading...' /></li>");
			},*/
			error: function( status ){
				alert('error');
			},	
		});	
	}
}); 
</script>

</body>
</html>