<?php
   session_start();
   
  
	
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Best Street Taco Tour in Cancun | CancunFoodTours.com</title>
    <meta name="description" content="Take our 3-hour guided Cancun Taco Tour to discover where people from Cancun go eat their tacos. The Tour is Ideal for taco street food lovers.">
    <meta NAME="Keywords" CONTENT="Taco tours, best tacos in cancun, street tacos in cancun, best taco stand in cancun, best tacos in hotel zone, taco factory cancun">

    <link rel="canonical" href="https://cancunfoodtours.com/cancun-taco-tour">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/cancun-taco-tour">
    <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css?ver=1.0">
    <link rel="stylesheet" type="text/css" href="./css/new_style.min.css?ver=1">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    <?php 
        include('includes/tagmanager.php');
        include('includes/schemas/productSchemaTacoTour.php');
        ?>

</head>

<body>
    
    <?php include('includes/menu.html');  ?>
    <div class="aux-pb-5"></div>
    <div class="body-background">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-8 col-md-8-fixed">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Tacotour/taco-tour-pastor.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Tacotour/taco-tour-cochinita.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Tacotour/taco-tour-arrachera.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Tacotour/taco-tour-suadero.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Tacotour/taco-tour.jpg" alt="Third slide">
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
                    
        <div class="container-fluid visible-sm" style="display:none;">
            <div class="highlight pt-2">
                <div class="col-12">
                    <h1 class="single-tour-h1">Cancun Taco Tour</h1>
                    <p><strong>Duration:</strong> 3.5-4.5 hrs</p>
                    <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                    <div class="row text-center pt-3 mb-3">
                        <div class="col-4">
                            <span class="labels">ADULT</span>
                            <br/>
                            <h3 class="mb-0" style="font-size:1.3rem;">$59 USD</h3>
                            <p class="text-center currency"></p>
                        </div>
<!--
                        <div class="col-4">
                            <span class="labels">Adult W/Alcohol</span>
                            <br/>
                            <h3 class="mb-0">$80</h3>
                            <p class="text-center currency"></p>
                        </div>
-->
                        <div class="col-4">
                            <span class="labels">WHEN</span>
                            <br />
                            <h3 class="mb-0" style="font-size:1.3rem;">Tuesday Thru Friday</h3>
<!--                            <p class="text-center currency">Age 12-</p>-->
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <p class="additional-pricing-info text-center">
                                <small>Tickets must be purchased before 10am for the selected date.<br/><br/> **Round trip transportation included from any Cancun Hotel or Airbnb.</small>
                            </p>
                        </div>
                    </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/1" class="btn btn-primary">BOOK NOW</a>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
            <div class="section text-left">
        <div class="row">
            <div class="col-lg-12 text-left">
                <div class="mb-3 tour-stops">
            <h2>With the Colors of Mexico Cancun Taco Tour you will try a variety of tacos at 5 taquerias locals keep coming day after day.</h2>
                <ul class="pl-20_bullets">
                    <li>Are you ready discover why taco bell is not Mexican food? This is for you.</li>
                    <li>Tacos are part of every Mexican diet, explore 5 taco joints that Cancun locals have claimed to be the best.</li>
                    <li>Our partnerships will get you an insider’s look at Cancun’s most traditional and varied taquerias.</li>
                    <li>With this tour, you will have the opportunity to explore more than just Cancun’s beaches. Downtown Cancun is full history and foodie gems that not all visitors are able to explore.</li>
                    <li>This experience is perfect for people who diversify their taco knowledge since there are 5 types of taco included in the tour.</li>
                    <li>Nothing pairs better with tacos than beer, so we included 3 national beers for you to taste.</li>
                    <li>Explore Downtown Cancun as an insider &amp; local perspective</li>
                </ul>
            </div>
                <hr>
                <div class="row whatknow">
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-hourglass fa-fw"></i><div class="icon-content" ><p>Tour times and availability vary per tour. Please ask us if you need a date opened.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-umbrella fa-fw"></i><div class="icon-content" ><p>Experiences happen rain or shine. Umbrellas are offered by our guides.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-users fa-fw"></i><div class="icon-content" ><p>Small groups of no more than 10 guests per guide.</p></div>                    
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-flag fa-fw"></i><div class="icon-content" ><p>Vegetarian substitutions are available. Please notify us of serious allergies.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-check-circle fa-fw"></i><div class="icon-content" ><p>Restrooms are available during the experience.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-heartbeat fa-fw"></i><div class="icon-content" ><p>Get exclusive discounts to the best places to eat in Cancun.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                <hr>
            <div class="section text-left">
                <div class="mb-3 tour-stops">
                    <h2>What to expect from the Colors of Mexico Cancun Taco Tour</h2>
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/what_to_expect_tacotour.jpg">
                    <p>Tacos are probably the thing that Mexicans export the most. We are sure that tacos are somewhere included in the Mexicans’ pyramid of nutrients. Cancun as home of people from all parts of the country due to the attractiveness of job opportunity, has been able to establish a very serious &amp; legit taco scene.</p>
                    <p>North, south, east or west, you can ask a Mexican and they will tell you that tacos are not alike in each region. That is why in our “Cancun Taco Tour” we decided to include a taco for each region, to take you to a journey across Mexico through your mouth.</p>
                    <p>Prepare yourself to devour delicious traditional tacos, enjoy refreshing local beers, &amp; delight yourself with the beautiful bright colors from the downtown Cancun. Also, **Spoiler alert** you will learn how to do hand-made tortillas &amp; will have the opportunity to listen an authentic “Mariachi” band live.</p>
                </div>
                <div class="mb-3 tour-stops">
                <h2>The glory of a good taco is worth its weight in gold, so dare to weight it yourself with the Colors of Mexico Taco Tour</h2>
                    </div>
                <div class="row location_gallery grid-third">
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_1.jpg">
                        <h4>Mayan food inspired taco</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_2.jpg">
                        <h4>Mexico City style taco</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_3.jpg">
                        <h4>"Surtido" will blow your mind</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_4.jpg">
                        <h4>World Famous Taco al pastor</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_5.jpg">
                        <h4>Dessert at a local park.</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/tacotour_thumb_6.jpg">
                        <h4>Guacamole, pico de gallo etc..</h4>
                    </div>
                    

                </div>
<!--
                <div class="locations mb-1">
                    <h4>Do-Rite Donuts</h4>
                    <p>Chef &amp; Co-owner, Francisco Brennan, uses his Michelin restaurant experience to create uniquely flavored, artisanal donuts in one of America's favorite donut shops.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Bonci Pizza</h4>
                    <p>Rome-based Italian import selling scissor-cut slices featuring distinctive toppings, sold by weight.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Kuma's Corner [Not on Fri-Sat Evening Tours]</h4>
                    <p>Owners Mike and Rick combined three of their favorite things: burgers, beer and metal. Their ethos is simple: Support Your Community. Eat Beef. Band Your Head.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Nonna's/Formento's</h4>
                    <p>Owner John Ross and Executive Chef Todd Stein bring a sense of warmth, family and unforgettable food that is reminiscent of Sunday family dinners at Ross' grandma's house.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Cemitas Puebla</h4>
                    <p>Family owned restaurant specializing in traditional Poblano foods, using recipes that can be traced back to Tony Anteliz’s family in Puebla, Mexico</p>
                </div>
-->
            </div>
            <hr/>
            </div>
                <div class="col-md-4 hidden-sm" style="z-index:9999;position:relative">
            <div id="sidebar-fx" class="position-fixed-sd fixed-sidebar affix-bottom" data-spy="affix" data-offset-top="160" data-offset-bottom="600">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="highlight bordered short">
                            <div class="col-md-12">
                                <h1 class="single-tour-h1">Cancun Taco Tour</h1>
                                <p><strong>Duration:</strong> 3.5-4.5 hrs </p>
                                <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                                <div class="row text-center pt-3 mb-3">
                                    <div class="col-md-4">
                                        <span class="labels">ADULT</span>
                                        <br/>
                                        <h3 class="mb-0" style="font-size:1.3rem;">$59 USD</h3>
                                        <p class="text-center currency"></p>
                                    </div>
<!--
                                    <div class="col-md-4">
                                        <span class="labels">Adult W/Alcohol</span>
                                        <br/>
                                        <h3 class="mb-0">$80</h3>
                                        <p class="text-center currency"></p>
                                    </div>
-->
                                    <div class="col-4">
                                        <span class="labels">WHEN</span>
                                        <br />
                                        <h3 class="mb-0" style="font-size:1.3rem;">Tuesday Thru Friday</h3>
<!--                                        <p class="text-center currency">Age 12-</p>-->
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-12">
                                        <p class="additional-pricing-info">
                                            <small>Tickets must be purchased by 11am for today's tour(s).<br/><br/> ** Round trip transportation included from any Cancun Hotel or Airbnb.</small>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/2" class="btn btn-primary">BOOK NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div id="testimonial" class="highlight testimonial bordered">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-8 col-md-10 col-sm-push-4 col-md-push-2 col-sm-height col-sm-middle order-sm-2">
                            <div class="quote">
                                <blockquote class="homepage-lead mb-2">
                                    <p>All previous reviews said it all. Must take this tour if you are in Cancun. Thanks to Luis for the unforgettable evening.I did not get the driver name, thanks to him too. When we go back to Cancun we will definitely do this tour again..</p>
                                </blockquote>
                                <span class="labels">VANNAQU</span>
                            </div>
                        </div>
                        <div class="col-4 col-sm-4 col-md-2 col-sm-pull-8 col-md-pull-10 col-sm-height col-sm-middle order-sm-1" style="padding-top:4%;">
                            <img class="img-responsive border-radius" src="https://cancunfoodtours.com/img/Tacotour/profile_2.jpg" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('includes/footer.html'); ?>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js?4.0"></script>
    <script src="./js/fixed.min.js?ver=1"></script>
    </body>
</html>