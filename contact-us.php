<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email and Phone Contact for CancunFoodTour.com</title>
    <meta name="description" content="Hungry yet? Get in touch with our CancunFoodTours Team for any questions & reservations. Find our information here.">
    <meta name="Keywords" content="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
    <link rel="canonical" href="https://cancunfoodtours.com/contact-us">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/contact-us">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
		<?php include('includes/tagmanager.php'); ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
    <?php include('includes/menu.html');  ?>

    <div class="container text-left body">
        <h1><strong>Contact</strong></h1>
        <div class="row align-items-center">

            <div class="col-12 order-md-1 ">
                <img src="https://cancunfoodtours.com/img/salsa.jpg" alt="Cancun Food Tours Private" class="img-responsive">
            </div>
            <div class="col-12 col-md-6 order-md-3 contact-left-sm">
                <h2 style="font-size:23px;"><strong>Cancun Food Tour Contact</strong></h2>
                <p>We're more than willing to answer all your questions &amp; inquiries. We kindly invite you to look at our F.A.Q.'s. Use the form below to ask us for any additional support you need.</p>
                <form id="contact-form" method="POST" action="mailer.php" role="form">
                    <div class="controls">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input id="form_name" type="text" name="nombre" class="form-control" placeholder="Name *" required="required" data-error="Firstname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <textarea id="form_message" name="mensaje" class="form-control contact-message" placeholder="Write your message *" rows="5" style="height:140px" required="required" data-error="Please, leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="g-recaptcha text-center" style="transform: scale(0.77); -webkit-transform: scale(0.77); margin-top:-15px;" data-sitekey="6Leef3UUAAAAAI3GGiJK4dKtaeCej7jV3krx7myb" data-callback="recaptchaCallback"></div>
                            </div>
                            <div class="col-md-6 submit-button">
                                <input id="btnSubmit" type="submit" class="btn btn-success btn-send disabled" value="SUBMIT">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-6 order-md-2 contact-right-sm">
                <div class="row">
                    <div class="col-12 contact-team-title">
                        <h2 style="font-size:23px;"><strong>The Team</strong></h2>
                    </div>
                    <div class="col-5 contact-right-text">
                        <p>Ticket and tour questions:</p>
                    </div>
                    <div class="col-7 contact-right-text">
                        <p style="-webkit-margin-after:0px">Email: <a href="mailto:taco@cancunfoodtours.com" style="text-decoration:none;color:inherit;"><strong>taco@cancunfoodtours.com</strong></a></p>
                        <p>Tel: <a href="tel:+52-1-998-887-4242" style="text-decoration:none;color:inherit;"><strong>+52 1 998 887 42 42</strong></a></p>
                    </div>
                    <div class="col-12 contact-image">
                        <!--                <div id="map" style="width:400px;height:400px"></div>-->
                        <img src="https://cancunfoodtours.com/img/mapa14.jpg" style="width:100%" alt="Location" />
                    </div>
                </div>
            </div>
            <div class="col-12 order-md-4">
                <p class="text-center" style="margin-top:30px">Address: Cancun Food Tours Carretera a Punta Sam Mza 2 34, SM 86 Punta Sam, Cancun, C.P. 77500</p>
            </div>
        </div>
    </div>

    <?php include('includes/footer.html'); ?>
    <script>
        function recaptchaCallback() {
            var response = grecaptcha.getResponse();
            if (response.lengh != 0) {
                $("#btnSubmit").removeClass("disabled");
                $("#btnSubmit").removeClass("disabled");
            }
        }
    </script>
</body>

</html>
