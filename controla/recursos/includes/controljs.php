    <!-- Jquery JS-->
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/slick/slick.min.js">
    </script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/wow/wow.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/animsition/animsition.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="https://cancunfoodtours.com/controla/recursos/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="https://cancunfoodtours.com/controla/recursos/js/main.js"></script>