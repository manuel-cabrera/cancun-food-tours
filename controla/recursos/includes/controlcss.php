    <!-- Fontfaces CSS-->
    <link href="https://cancunfoodtours.com/controla/recursos/css/font-face.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="https://cancunfoodtours.com/controla/recursos/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="https://cancunfoodtours.com/controla/recursos/css/theme.css" rel="stylesheet" media="all">