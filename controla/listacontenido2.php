<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <?php 
        include('recursos/includes/controlcss.php');
    ?>

</head>

<body class="animsition">
    <div class="page-wrapper">


        <?php include('recursos/includes/menu.php'); ?>

        <!-- PAGE CONTAINER-->
        <div class="page-container">

            <?php include('recursos/includes/menu-arriba.php'); ?>

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include('recursos/includes/controljs.php'); ?>

</body>

</html>
<!-- end document-->
