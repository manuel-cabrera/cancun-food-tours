<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>
    <?php 
        include('recursos/includes/controlcss.php');
    ?>
    <script type="text/javascript">
        function login(){
            var usuario = $('#usuario').val();
            var password = $('#password').val();
            $.ajax({
                type: "POST",
                url: "lib/login.php",
                data: {dato1: usuario, dato2: password},
                success: function(data) {
                    console.log(data);
                    if (data == 0) {

                        $('#Info').fadeIn(1000).html('Usuario o Contraseña Invalido');
                    }else if (data == 2){
                        $('#Info').fadeIn(1000).html('Usuario o Contraseña Invalido');
                    }else{

                        $(location).attr('href',data);

                    }
                    //$('#Info').fadeIn(1000).html(data);
                //alert(data);
                //$(location).attr('href',url);
                }
            });
        }

    </script>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <img src="https://cancunfoodtours.com/img/cancunfoot.png" alt="CoolAdmin">
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input class="au-input au-input--full" id="usuario" type="text" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" id="password" name="password" placeholder="Password">
                                </div>
                                <p id="Info"></p>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="button" onclick="login()">sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include('recursos/includes/controljs.php'); ?>

</body>

</html>
<!-- end document-->