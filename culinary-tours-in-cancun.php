<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Culinary Tours in Cancun | CancunFoodTours.com</title>
   <meta name="description" content= "A delicious culinary experience in Cancun. Book Your food Tour & Enjoy local dishes in authentic mexican restaurants on your Cancun Vacation. ">
	<meta name="Keywords" content="culinary food tours, culinary tasting cancun, food tasting in cancun, culinary vacations cancun, gastronomic tours.">
	<link rel="canonical" href="https://cancunfoodtours.com/culinary-tours-in-cancun">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/culinary-tours-in-cancun">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    	<?php include('includes/tagmanager.php'); ?>
</head>

<body>
    <?php include('includes/menu.html');  ?>
    <div class="container text-center body">
        <div class="title-subt">
            <h2 style="color:black"><strong>Welcome to Cancun Food Tours!</strong></h2>
            <div class="row align-items-center">
                <div class="col-12">
                    <p>
                        Take a journey through Mexican Culture, Folklore, Food &amp; Drink tradition of Cancun with one of our local food experts who will offer you a unique perspective of the city.
                    </p>
                </div>
            </div>
        </div>
        <div class="mobile-carousel">
            <?php include('includes/product-carousel-index.html') ?>
        </div>
        <div class="desktop-carousel">
            <?php include('includes/carousel-index-desk.html')?>
        </div>
                <div class="row align-items-center">
        <br />
            <?php include('includes/comments.html');?>
<!--        <div class="row align-items-center">-->
            <div class="col-12 order-md-1">
                <h1><strong>Why take a culinary tour with us?</strong></h1>
<!--                <p><span style="color:#E31B5C"> In Cancunfoodtours.com we have a passion for sharing and creating unique experiences.</span></p>-->
            </div>
            <div class="col-12 col-md-6 order-md-3">
                <iframe width="100%" height="280" class="ytbframe" src="https://www.youtube.com/embed/UYhRLqhnF-k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-6 order-md-2">
                <p>Cancun Food Tours offers you the most unique culinary tours in Cancun. We provide the most delicious selection of culinary tasting tours that have become one of the top activities in Cancun. In our tours, you’ll taste food from a variety of one-of-a-kind Mexican eateries, while receiving the locals’ view into the Mexico &amp; Cancun's Culinary scene.</p>
                
                <p>Our mission is to help you experience authentic flavors of Mexico. Our cancun culinary tours allows you to taste flavors of Mexican food safely and conviniently. Our restaurants are local trademarks in the gastronomic scene, so you will enjoy the best food and drinks. Enjoy one of our tours where the food is a mixture of cuisines from all over Mexico, an experience which has become one of the best things to do in Cancun. Don’t miss out, Book Now!</p>
            </div>
                <div class="col-12 order-md-4">
                    <i><strong>Eat, drink &amp; discover Cancun as you see the city’s sight from a local' s view.</strong></i>
                </div>
            <div class="col-12 order-md-5">
                <br/>
                <br/>
                <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-success btn-send">BOOK NOW</a>
                <br/>
                <br/>
                <img src="images/icon_1.png" alt="Image behind book button">
                <br/>
                <br/>
            </div>
            <div class="col-12 col-md-6 order-md-6">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <a href="flavors-of-mexico-food-tour">
                            <img class="card-single-tour" src="images/flavors_index_card_m.png">
                        </a>
                    </div>
                    <div class="col-12 col-sm-6">
                        <a href="taco-tour-in-cancun">
                            <img class="card-single-tour" src="images/taco_index_card_m.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 order-md-8" style="padding-top:20px;">
                <div class="row">
                <div class="col-12">
                <p><strong>Compare Tour highlights</strong></p>
                    </div>
                    <div class="col-12">
                <p>We’ve created several food tours for you to choose based on convenience, price, schedule and most importantly your desire to try a specific Mexican food.</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 order-md-7">
                <img src="images/cancunfoodtour2.jpg" style="width:100%">
            </div>
            <div class="col-12 col-md-6 order-md-9" style="padding-top:20px;">
                <div class="row">
                    <div class="col-6">
                        <p><strong>Group of 10 or more?</strong></p>
                    </div>
                    <div class="col-6">
                        <a href="cancun-food-tours-private-events" class="btn btn-send" style="background-color:#7DB03D; border:none; color:white">LEARN MORE</a>
                    </div>
                    <div class="col-12">
                        <p>Try our experience privately and exclusively for any number of group members. These private food tours are perfect for teams, corporate events or large friends outings. </p>
                    </div>
                </div>
            </div>
            <div class="order-md-11">
                    <?php include('includes/ta-logos.html') ?>
        </div>
        </div>

    </div>
    <?php include('includes/footer.html'); ?>
</body>

</html>
