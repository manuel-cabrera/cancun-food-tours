<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Cooking Class in Cancun, Aromas of Mexico | Cancunfoodtours.com</title>
    <meta name="description" content="Where to eat in Cancun? Let us help you decide, read our top 10 selection of Best Places to eat in Cancun stop wasting time getting to the wrong places!">
    <meta name="Keywords" content="Aromas of Mexico, Cooking Class in Cancun">

    <link rel="canonical" href="https://cancunfoodtours.com/cooking-class-in-cancun">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/cooking-class-in-cancun">
    <!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css?ver=1.0">
    <link rel="stylesheet" type="text/css" href="./css/new_style.min.css?ver=1">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <?php 
    include('includes/tagmanager.php'); 
        include('includes/schemas/productSchemaAromMexClass.php')
    ?>

</head>

<body>
   <?php include('includes/menu.html');  ?>
    <div class="aux-pb-5"></div>
    <div class="body-background">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-8 col-md-8-fixed">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-2.jpg" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-1.jpg" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-5.jpg" alt="Third slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-9.jpg" alt="Fourth slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-8.jpg" alt="Fifth slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-10.jpg" alt="Sixth slide">
                </div>
                  <div class="carousel-item">
                  <img class="d-block w-100" src="https://cancunfoodtours.com/img/Cclass/CClass-11.jpg" alt="Seventh slide">
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
                    
        <div class="container-fluid visible-sm" style="display:none;">
            <div class="highlight pt-2">
                <div class="col-12">
                    <h1 class="single-tour-h1">The Best Cooking Class in Cancun</h1>
                    <p><strong>Duration:</strong> 3.5-4.5 hrs</p>
                    <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                    <div class="row text-center pt-3 mb-3">
                        <div class="col-4">
                            <span class="labels">ADULT</span>
                            <br/>
                            <h3 class="mb-0" style="font-size:1.3rem;">$99 USD</h3>
                            <p class="text-center currency"></p>
                        </div>
<!--
                        <div class="col-4">
                            <span class="labels">Adult W/Alcohol</span>
                            <br/>
                            <h3 class="mb-0">$80</h3>
                            <p class="text-center currency"></p>
                        </div>
-->
                        <div class="col-4">
                            <span class="labels">WHEN</span>
                            <br />
                            <h3 class="mb-0" style="font-size:1.3rem;">Mon Through Thurs</h3>
<!--                            <p class="text-center currency">Age 12-</p>-->
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <p class="additional-pricing-info text-center">
                                <small>Tickets must be purchased before 10am for the selected date.<br/><br/> **Round trip transportation included from any Cancun Hotel or Airbnb.</small>
                            </p>
                        </div>
                    </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/3" class="btn btn-success btn-send">BOOK NOW</a>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
            <div class="section text-left">
        <div class="row">
            <div class="col-lg-12 text-left">
                <div class="mb-3 tour-stops">
            <h2>Learn how to cook several Mexican traditional dishes, a very tasty dessert &amp; the secret for a perfect margarita with our Cancun Cooking Class.</h2>
                <ul class="pl-20_bullets">
                    <li>Go hands in with our Cancun Cooking Class &amp; learn the secrets of Mexican cuisine under the directions of our expert chef.</li>
                    <li>Take a visit to a traditional “Mercado” &amp; learn to choose the best ingredients for Mexican recipes.</li>
                    <li>Cook your own hand-made tortillas &amp; sopes. Believe us, you haven’t tried a tortilla this flavorful.</li>
                    <li>The Molcajetes and Comales are traditional utensils used in Mexican cuisine, become an expert on how to use them.</li>
                    <li>Why is guacamole in Mexico different? Learn the secret for you to impress your friends back home.</li>
                    <li>Get exclusive discounts to the best places to eat in Cancun through our partnerships.</li>
                </ul>
            </div>
                <hr>
                <div class="row whatknow">
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-hourglass fa-fw"></i><div class="icon-content" ><p>Tour times and availability vary per tour. Please ask us if you need a date opened.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-umbrella fa-fw"></i><div class="icon-content" ><p>Experiences happen rain or shine. Umbrellas are offered by our guides.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-users fa-fw"></i><div class="icon-content" ><p>Small groups of no more than 10 guests per guide.</p></div>                    
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-flag fa-fw"></i><div class="icon-content" ><p>Vegetarian substitutions are available. Please notify us of serious allergies.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <i class="fa fa-check-circle fa-fw"></i><div class="icon-content" ><p>Restrooms are available during the experience.</p></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <i class="fa fa-heartbeat fa-fw"></i><div class="icon-content" ><p>Get exclusive discounts to the best places to eat in Cancun.</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                <hr>
            <div class="section text-left">
                <div class="mb-3 tour-stops">
                    <h2>What to expect from the Aromas of Mexico Cooking Class in Cancun</h2>
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/what_to_expect_flavors.jpg">
                    <p>Taking cooking classes in Cancun will definitely be one of the best things to do in Cancun. After your hotel pick up our chef will take you straight to one of the most visited local markets in Cancun. Get ready to delight your senses with the colors, sounds and scents the market morning activity. Stroll around through different food sections and immerse yourself in the smell of different herbs, vegetables, meat and sea food. Our chef will tell show you how to choose freshest items for our cooking class.</p>
                    <p>Once you arrive to our venue, refreshments, fresh fruit, water/coffee will be provided. Our venue offers an intimate setting at one of the best Mexican restaurants in Cancun which opened more than 40 years ago. Spend the next 3 hours learning the tricks behind great Mexican traditional cooking and listening about the origins behind each dish and how they came to be. Experience the real warmth of a Mexican kitchen using traditional tools like Molcajete (stone mortar), Comal (flat griddle) and clay pots.</p>
                </div>
                <div class="mb-3 tour-stops">
                <h2>Discover the best recipes from restaurants that Cancun locals have continued to love for the last 40 years</h2>
                    </div>
                <div class="row location_gallery grid-third">
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/cclass_thumb_1.jpg">
                        <h4>Intimate settings</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                    <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/cclass_thumb_2.jpg">
                        <h4>Learn about ingredients</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/cclass_thumb_3.jpg">
                        <h4>Visit a local market</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/cclass_thumb_4.jpg">
                        <h4>Learn how to prepare margaritas</h4>
                    </div>
                    <div class="item col-sm-6 col-md-4 mb-4">
                        <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/cclass_thumb_5.jpg">
                        <h4>Eat &amp; indulge on your creation</h4>
                    </div>
                </div>
<!--
                <div class="locations mb-1">
                    <h4>Do-Rite Donuts</h4>
                    <p>Chef &amp; Co-owner, Francisco Brennan, uses his Michelin restaurant experience to create uniquely flavored, artisanal donuts in one of America's favorite donut shops.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Bonci Pizza</h4>
                    <p>Rome-based Italian import selling scissor-cut slices featuring distinctive toppings, sold by weight.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Kuma's Corner [Not on Fri-Sat Evening Tours]</h4>
                    <p>Owners Mike and Rick combined three of their favorite things: burgers, beer and metal. Their ethos is simple: Support Your Community. Eat Beef. Band Your Head.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Nonna's/Formento's</h4>
                    <p>Owner John Ross and Executive Chef Todd Stein bring a sense of warmth, family and unforgettable food that is reminiscent of Sunday family dinners at Ross' grandma's house.</p>
                </div>
                <div class="locations mb-1">
                    <h4>Cemitas Puebla</h4>
                    <p>Family owned restaurant specializing in traditional Poblano foods, using recipes that can be traced back to Tony Anteliz’s family in Puebla, Mexico</p>
                </div>
-->
            </div>
            <hr/>
            </div>
                <div class="col-md-4 hidden-sm" style="z-index:9999;position:relative">
            <div id="sidebar-fx" class="position-fixed-sd fixed-sidebar affix-bottom" data-spy="affix" data-offset-top="160" data-offset-bottom="600">
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="highlight bordered short">
                            <div class="col-md-12">
                                <h1 class="single-tour-h1">The Best Cooking Class in Cancun</h1>
                                <p><strong>Duration:</strong> 3.5-4.5 hrs </p>
                                <p><strong>Neighborhoods:</strong> Downtown Cancun</p>
                                <div class="row text-center pt-3 mb-3">
                                    <div class="col-md-4">
                                        <span class="labels">ADULT</span>
                                        <br/>
                                        <h3 class="mb-0" style="font-size:1.3rem;">$99 USD</h3>
                                        <p class="text-center currency"></p>
                                    </div>
<!--
                                    <div class="col-md-4">
                                        <span class="labels">Adult W/Alcohol</span>
                                        <br/>
                                        <h3 class="mb-0">$80</h3>
                                        <p class="text-center currency"></p>
                                    </div>
-->
                                    <div class="col-4">
                                        <span class="labels">WHEN</span>
                                        <br />
                                        <h3 class="mb-0" style="font-size:1.3rem;">Mon Through Thurs</h3>
<!--                                        <p class="text-center currency">Age 12-</p>-->
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-sm-12">
                                        <p class="additional-pricing-info">
                                            <small>Tickets must be purchased by 11am for today's tour(s).<br/><br/> ** Round trip transportation included from any Cancun Hotel or Airbnb</small>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <a href="https://cancunfoodtours.com/bookings/3" class="btn btn-success btn-send">BOOK NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div id="testimonial" class="highlight testimonial bordered">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-8 col-md-10 col-sm-push-4 col-md-push-2 col-sm-height col-sm-middle order-sm-2">
                            <div class="quote">
                                <blockquote class="homepage-lead mb-2">
                                    <p>All I can say is that this fun evening of food and drink was the highlight of my Cancun visit. These guys are friendly, prepared and professional. They took care of everything. All you need to do is bring your hearty appetite because you will leave full :).</p>
                                </blockquote>
                                <span class="labels">Explorer751521</span>
                            </div>
                        </div>
                        <div class="col-4 col-sm-4 col-md-2 col-sm-pull-8 col-md-pull-10 col-sm-height col-sm-middle order-sm-1" style="padding-top:4%;">
                            <img class="img-responsive border-radius" src="https://cancunfoodtours.com/img/Flavors/profile_1.jpg" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include('includes/footer.html'); ?>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="./js/bootstrap.min.js?4.0"></script>
    <script src="./js/fixed.min.js?ver=1"></script>
</body>

</html>
