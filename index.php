<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
    <meta name="description" content="Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
    <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
<link rel="canonical" href="https://cancunfoodtours.com/">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    	<?php 
        include('includes/tagmanager.php'); 
        include('includes/schemas/organizationSchema.php')
        ?>
</head>

<body>
    <?php include('includes/menu.html');  ?>
    <div class="container text-left body">
        <div class="title-subt text-center">
            <h2 style="color:black"><strong>Welcome to Cancun Food Tours!</strong></h2>
            <div class="row align-items-center">
                <div class="col-12">
                    <p>
                        Take a journey through Mexican Culture, Folklore, Food &amp; Drink tradition of Cancun with one of our local food experts who will offer you a unique perspective of the city.
                    </p>
                </div>
            </div>
        </div>
        <div class="mobile-carousel text-center">
            <?php include('includes/product-carousel-index.html') ?>
        </div>
        <div class="desktop-carousel text-center">
            <?php include('includes/carousel-index-desk.html')?>
        </div>
                <div class="row align-items-center">
        <br />
                    <div class="order-md-10 text-center ">
            <?php include('includes/comments.html');?>
                    </div>
<!--        <div class="row align-items-center">-->
            <div class="col-12 order-md-1 pink-text text-center" >
                <h1 style="margin-bottom:-.5rem"><strong style="color:#E31B5C">Amazing Food Tours in Cancun Created for Every Traveler</strong></h1>
                <p><span style="color:#E31B5C; font-size:1rem;"> In Cancunfoodtours.com we have a passion for sharing and creating unique experiences.</span></p>
            </div>
            <div class="col-12 col-md-6 order-md-3">
                <iframe width="100%" height="280" class="ytbframe" src="https://www.youtube.com/embed/UYhRLqhnF-k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-6 order-md-2">
                <p class="index-text-md">We believe that food allows people to connect, that is why we created a set of culinary tours to take travelers to the best places to eat in Cancun.</p>
                <p>Our goal is to provide every traveler, a local’s perspective of the city while they enjoy very diverse Mexican food and beverages from all over the country in local Cancun restaurants.
                </p>
                <p>Mexican Food is more than tacos and Cancun is much more than a beach, so if you’re into trying new things and enjoying cultural experiences, you’re at the right place. Explore everything we have to offer with our Food Tours in Cancun.
                </p>
            </div>
                <div class="col-12 order-md-4 text-center">
                    <i><p class="index-text-md">Eat, drink &amp; discover Cancun as you see the city’s sight from a local' s view.</p></i>
                </div>
            <div class="col-12 order-md-5 text-center">
                <br/>
                <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-success btn-send">BOOK NOW</a>
                <br/>
                <br/>
                <img src="images/icon_1.png" alt="Image behind book button">
                <br/>
                <br/>
            </div>
            <div class="col-12 col-md-6 order-md-6">
                <div class="row">
                    <div class="col-12 col-sm-6 text-center">
                        <a href="flavors-of-mexico-food-tour">
                            <img class="card-single-tour" src="images/flavors_index_card_m.png">
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 text-center">
                        <a href="taco-tour-in-cancun">
                            <img class="card-single-tour" src="images/taco_index_card_m.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 order-md-8" style="padding-top:20px;">
                <div class="row">
                <div class="col-12">
                <p class="index-text-md"><strong>Compare Tour highlights</strong></p>
                    </div>
                    <div class="col-12">
                <p>We’ve created several food tours for you to choose based on convenience, price, schedule and most importantly your desire to try a specific Mexican food.</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 order-md-7">
                <img src="images/cancunfoodtour2.jpg" style="width:100%">
            </div>
            <div class="col-12 col-md-6 order-md-9" style="padding-top:20px;">
                <div class="row">
                    <div class="col-7">
                        <p class="index-text-md"><strong>Group of 10 or more?</strong></p>
                    </div>
                    <div class="col-5">
                        <a href="cancun-food-tours-private-events" class="btn" style="background-color:#7DB03D; width:100%; border:none; color:white">LEARN MORE</a>
                    </div>
                    <div class="col-12">
                        <p>Try our experience privately and exclusively for any number of group members. These private food tours are perfect for teams, corporate events or large friends outings. </p>
                    </div>
                </div>
            </div>
            <div class="order-md-11 text-center">
                    <?php include('includes/ta-logos.html') ?>
        </div>
        </div>

    </div>
    <?php include('includes/footer.html'); ?>
</body>

</html>
