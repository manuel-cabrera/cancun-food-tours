<?php 

$transactionIds =  $_GET['transid'];
$skus = $_GET['producto'];
$transactionTotals = $_GET['total'];
$names = $_GET['nombreproducto'];
$prices = $_GET['precioproducto'];
$categorys = $_GET['categoria'];
$quantitys = $_GET['cantidad'];
$transactionAffiliations = $_GET['websitio'];
$currencyCodes = $_GET['moneda'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php echo '<meta name="description" content="Thank you paypal">' ?>	
	<meta name="robots" content="NOINDEX, NOFOLLOW">
	<title>Thanks for your purchase! | cancunfoodtours.com</title>

	<link rel="shortcut icon" href="https://cancunfoodtours.com/img/cancunfoodtour.ico">
	<link rel="author" type="text/plain" href="https://cancunfoodtours.com/humans.txt">
	<?php include('aqui/booking/lib/controllib.php'); ?>
	<script src="https://cancunfoodtours.com/aqui/js/modernizr.js"></script>

	<style type="text/css">
		.cont1{	width: 960px;margin: 0 auto;}
		 .contieneotro{ 	float: left;  	width: 100%;   	margin-bottom: 20px;    }
	</style>
<?php include('includes/tagmanager.php'); ?>
</head>
 
<body>
<?php include('includes/menu.php');  ?>

	<div class="cont1" >
		<div class="contieneotro">
			<section class="content thank_you" style=" float: left; width: 100%; text-align: center;">

			<h1>CONGRATULATIONS!</h1>

			<p>Your payment was succesful!<br>
			We´ve sent your purchase receipt to your email address, please review this confirmation carefully as it includes important and helpful information about your tour.</p>
			<p>Best regards,<br>
			CancunFoodTours.com.

			 </p><hr><p>Remember you can always contact us: <br> | <br> info@cancunfoodtours.com</p></section>
				
		
		</div>
	</div>
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || []
		dataLayer.push({
			'transactionId': '<?php echo $transactionIds ?>',
			'transactionAffiliation': '<?php echo $transactionAffiliations ?>',
			'transactionTotal': <?php echo $transactionTotals ?>,
			'transactionTax': 0,
			'transactionShipping': 0, 
			'currencyCode' : '<?php echo $currencyCodes ?>',
			'transactionProducts': [{
			    'sku': '<?php echo $skus ?>', 
			    'name': '<?php echo $names ?>',
			    'category': '<?php echo $categorys ?>',
			    'price': <?php echo $prices ?>,
			    'quantity': <?php echo $quantitys ?>
			}]
		});
</script>
</body>
</html>
