<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
        <meta name="description" content= "Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
        <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">
        
        <link rel="canonical" href="https://cancunfoodtours.com/">
        <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
        <script src="./js/bootstrap.min.js?4.0"></script>
        <link rel="stylesheet" type="text/css" href="./css/extra.css">
<!--        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/flexslider.min.css">-->
        <link rel="stylesheet" type="text/css" href="css/flexslider.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider-min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="js/extra.js"></script>
    </head>
    <body>
        <?php include('includes/menu.html');  ?>
        
        <div class="container body">
            
            <div class="banner">
             <div class="flexslider">
                <ul class="slides">
                    <li>
                      <img src="https://cancunfoodtours.com/img/salsa.jpg" />
                    </li>
                    <li>
                      <img src="https://cancunfoodtours.com/img/banner-1.jpg" />
                    </li>
                    <li>
                      <img src="https://cancunfoodtours.com/img/salsa.jpg" />
                    </li>
                    <li>
                        <img src="https://cancunfoodtours.com/img/Cclass/CClass-2-banner.jpg" style="float:left;"/>
                        <div style="margin:  0px;padding:  0px;position: absolute;">
                            <div style="width: 950px;margin: 0 auto;">
                                <div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
                                    <b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Aromas of Mexico Cooking Class in Cancun</b>
                                    <p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Mexican food is by far one of the best in the world, learn the secrets of its cuisine &amp; prepare 4 course-meal representing different parts of Mexico. Learn how to choose the ingredients while visiting a local market and be ready to feast on your deliciously “out of the oven” recipes on this intimate experience.</p>
                                    <div style="float:  left;width:  94%;margin-top:  14px;margin-bottom:  20px;text-align:  right;">
                                        <a href="https://cancunfoodtours.com/cooking-class-in-cancun" style="padding:  10px 60px;background:  green;color:  white;font-size:  22px;text-decoration:  none;-webkit-border-radius: 3px 3px 3px p3x;">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row justify-content-center align-items-center banner-cards">

        <div class="col-lg-3 col-md-4 card-background">
          <div class="profile-card text-center">

            <img class="img-responsive" src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-tostada-home.jpg">
            <div class="profile-info">


              <span class="hvr-underline-from-center">Flavors of Mexico</span>
              <div style="text-align: -webkit-left; padding: 0 10px">
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">When: Wed &amp; Sat</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Time: 3.5 - 4 hrs</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Stops: 4</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;"><strong>Price: $79.00 USD</strong></p>
                </div>
              <div class="button" style="padding:  10px 0 10px 0;">
              <a href="#" style="padding: 10px 20% 10px 20%;" onclick="window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour' ">BOOK NOW</a>
                </div>
            </div>

          </div>
        </div>
        <div class="col-lg-3 col-md-4 card-background">
          <div class="profile-card text-center">

            <img class="img-responsive" src="https://cancunfoodtours.com/img/Tacotour/taco-tour-pastor-home.jpg">
            <div class="profile-info">


              <span class="hvr-underline-from-center">Taco Tour</span>
              <div style="text-align: -webkit-left; padding: 0 10px">
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">When: Wed &amp; Sat</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Time: 3.5 - 4 hrs</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Stops: 4</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;"><strong>Price: $79.00 USD</strong></p></div>
              <div class="button" style="padding:  10px 0 10px 0;">
              <a href="#" style="padding: 10px 20% 10px 20%;" onclick="window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour' ">BOOK NOW</a>
                </div>
            </div>

          </div>
        </div>
        <div class="col-lg-3 col-md-4 card-background">
          <div class="profile-card text-center">

            <img class="img-responsive" src="https://cancunfoodtours.com/img/Cclass/CClass-2-home.jpg">
            <div class="profile-info">


              <span class="hvr-underline-from-center">Cooking Class</span>
              <div style="text-align: -webkit-left; padding: 0 10px">
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">When: Wed &amp; Sat</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Time: 3.5 - 4 hrs</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;">Stops: 4</p>
                  <p style="line-height:  1.6;margin: 0;font-size:  16px;margin-bottom:  2px;margin-top:  2px;"><strong>Price: $79.00 USD</strong></p></div>
                <div class="button" style="padding:  10px 0 10px 0;">
              <a href="#" style="padding: 10px 20% 10px 20%;" onclick="window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour' ">BOOK NOW</a>
                </div>
            </div>

          </div>
        </div>
      </div>
            </div>
            
            
            <div class="contenido">
                <h1 style="width:  100%;text-align:  center;font-size:  25px;"> <strong style="font-weight:400">Amazing  Food Tours in Cancun Created for Every Traveler</strong></h1>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <p>In Cancunfoodtours.com we have a passion for sharing and creating unique experiences. We believe that food allows people to connect, that is why we created a set of culinary tours in Cancun to take travelers to the best places to eat.</p>
                    <br />
                    <p>Our goal is to provide everyone on a cancun vacation, a local's perspective on delicious Mexican food in the best local restaurants. For a culinary trip around Mexico, choose our "Flavors of Mexico Food Tour", if you're taco lover enjoy the cooking class. Mexican Food is much more than tacos so if you're into trying new things and enjoying cultural experiences, you're at the right place. Explore everything we have to offer with our Food Tours in Cancun.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <iframe width="560" height="315" class="ytbframe" src="https://www.youtube.com/embed/UYhRLqhnF-k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div style="  float: left;  padding: 7px;">
				<div style="margin-right: 5px; background: #eddaa0">
					<div style="margin-left:20px;">
						<h3 style="font-size: 16px;letter-spacing: 1px;font-weight: initial;"> Flavors of Mexico</h3>
					</div>
					<div class="contenido1">
						<div class="trio">
							<p>7 Traditional Dishes</p>
							<p>Mexican Spirit Tasting</p>
							<p>4 Stops</p>
							<p>Price: $79 Usd</p>
							<!-- <p>When: Wednesdays & Saturdays</p>
					  	   	<p>Time: 3.5 – 4 hrs</p>
					  	    <p>Stops: 4 </p>
					  	    <b>Price: $79.00 usd</b> -->
					  	    <div class="boton">
					  	    	<a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour"> BOOK NOW</a>
					  	    </div>
					  	</div>
				  	</div>
				</div>
				<!-- Uno -->
				<div style="margin-right: 5px; background: #eddaa0">
					<div style="margin-left:20px;">
						<h3 style="font-size: 16px;letter-spacing: 1px;font-weight: initial;">Taco Tour in Cancun</h3>
					</div>
					<div class="contenido1">
						<div class="trio">
							<p>5 Different Tacos</p>
							<p>Local Beer Tasting</p>
							<p>5 Stops</p>
							<p>Price: $60 Usd</p>
							<!-- <p>When: Tuesday & Saturday</p>
					  	   	<p>Time: 3.5 - 4 hrs</p>
					  	    <p>Stops: 4 </p>
					  	    <b>Price: $60.00 usd</b> -->
					  	    <div class="boton">
					  	    	<a href="https://cancunfoodtours.com/taco-tour-in-cancun"> BOOK NOW</a>
					  	    </div>
					  	</div>
				  	</div>
				</div>
			</div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    </div>
                </div>
            </div>
            
        </div>
        
        <?php include('includes/footer.html'); ?>
    </body>
</html>