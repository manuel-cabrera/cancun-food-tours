<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us for CancunFoodTour.com</title>
	<meta name="description" content= "Cancunfoodtours.com is a mexican company created by and for love for food. Discover why our team decided to embark in providing food tours in Cancun.">
	<meta name="Keywords" content="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
	<link rel="canonical" href="https://cancunfoodtours.com/about-us">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/about-us">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
		<?php include('includes/tagmanager.php'); ?>
    <style type="text/css">
        body{
            font-size: 16px;
        }
    </style>
</head>

<body>
    <?php include('includes/menu.html');  ?>
    <div class="container text-left body">
        <!--            <br />-->
        <h1 class="about-us-h1"><strong>About us</strong></h1>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 img-mobile">
                <img src="https://cancunfoodtours.com/img/imagen-foot.png" class="img-responsive">
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 h2-title">
                <h2 class="about-us-h2"><strong>Food and tradition is in our heart...</strong></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <p>
                    Meet Luis, Javier &amp; Dieter, a group of three Mexican friends from Cancun each dedicated to their craft, Food, Tourism and Marketing. Growing up in Cancun and other places provided them the opportunity to work in the hospitality industry.
                </p>
                <p>
                    When comparing and sharing their experiences they decided to come together and share what each had learned to create CancunFoodTours.com, a project which intention is to share one big thing they have in common, the love for Mexican Food.
                </p>
                <p>As a team we know that Mexican food is a gift provided by our ancestors</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <p> that have passed from generation to generation. We believe that this gift has served as a universal language that has shaped the way Mexican culture is today.
                </p>
                <p>
                    As receivers of this gift, our duty is to present it to the world &amp; use it as a bridge to connect with people and other cultures a using the same language our ancestors used. All our tours and experiences have been created to connect you with the greatest food, drinks, tastes and experiences in Cancun &amp; in Mexico. Our goal is to introduce the Mexican Food in Cancun as one of the greatest in the world.
                </p>
                <p class="author"><strong>-Luis, Javier &amp; Dieter</strong></p>
            </div>
        </div>
    </div>
    <?php include('includes/footer.html'); ?>
</body>

</html>
