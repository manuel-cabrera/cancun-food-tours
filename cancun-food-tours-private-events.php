<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Private Events &amp; Groups</title>
<meta name="description" content= "">
<meta name="Keywords" content="">
	<link rel="canonical" href="https://cancunfoodtours.com/cancun-food-tours-private-events">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/cancun-food-tours-private-events">
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
		<?php include('includes/tagmanager.php'); ?>
    <style type="text/css">
        body{
            font-size: 16px;
        }
    </style>
</head>

<body>
    <?php include('includes/menu.html');  ?>
    <div class="container text-left body">
        <h1 class="about-us-h1"><strong>Private Events &amp; Groups</strong></h1><!--style="margin:1rem auto;"-->
        <div class="row align-items-center">
            <div class="col-sm col-lg-7 order-lg-1">
<!--                <img src="https://cancunfoodtours.com/img/cancun-food-tours-private-events.jpg" alt="Cancun Food Tours Private" 
class="img-responsive">-->
                <img src="https://cancunfoodtours.com/images/banner-1m.jpg" alt="Cancun Food Tours Private" class="img-responsive">
            </div>
            <div class="col-sm-12 col-lg-12 order-lg-5 col-md-12 order-md-5">



                <form id="contact-form" method="post" action="mailer.php" role="form">

                    <div class="messages"></div>

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">

                                <p class="about-us-h2" style="color:#EA5B2D"><strong>Contact:</strong></p>
                                <div class="form-group">
                                    <input id="form_name" type="text" name="nombre" class="form-control" placeholder="Name *" required data-error="Firstname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Last Name *" required="required" data-error="Lastname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_company" type="text" name="company" class="form-control" placeholder="Company *" required data-error="Company is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Phone Number *" required data-error="Phone Number is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="about-us-h2" style="color:#EA5B2D"><strong>Date desired:</strong></p>
                                <div class="form-group">
                                    <input id="form_date" type="text" name="date" class="form-control" placeholder="MM/DD/YY *" required data-error="Date is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <!--
                    <select id="form_need" name="need" class="form-control" required="required" data-error="Please specify your need.">
                        <option value=""></option>
                        <option value="Request quotation">Request quotation</option>
                        <option value="Request order status">Request order status</option>
                        <option value="Request copy of an invoice">Request copy of an invoice</option>
                        <option value="Other">Other</option>
                    </select>
-->
                                    <div class="form-group">
                                        <input id="form_attendees" type="text" name="attendees" class="form-control" placeholder="Number of attendees *" required="required" data-error="Number of attendees is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <textarea id="form_message" name="mensaje" class="form-control" placeholder="Please provide start time preference, tour route desired, nature of event and any other pertinent details we should know. *" rows="5" style="height:140px" required="required" data-error="Please, leave us a message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12 submit-button">
                                <input type="submit" class="btn btn-success btn-send" value="SUBMIT">
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">

                            </div>
                        </div>
                        <div class="row">


                        </div>
                    </div>

                </form>


            </div>
            <br />
            <br />
            <div class="col-sm-12 col-lg-12 order-lg-3 col-md-12 order-md-3" style="padding-top:20px">
                <h2 class="about-us-h2" style="color:#EA5B2D;display: -webkit-box;"><strong>Private Events &amp; Groups</strong></h2>
                <div class="row">
                    <div class="col-lg-6">
                        <p>Mexico is a country which is very rich in terms of culture. Food is one of the best &amp; most exciting ways of getting to know Mexico since most of our traditions &amp; ways of life are strictly related to it.</p>

                        <p>If you wish to get to know Mexican culture from a deeper perspective we can help you set a tour up that will cater for your specific needs. Private Transportation, tour guide and other services are at your disposal to make your experience something to remember. </p>

<!--                        <p>Private Transportation, tour guide and other services are at your disposal to make your experience something to remember. </p>-->
                    </div>
                    <div class="col-lg-6">
                        <p> Food Tours are a great experience that are good for celebrations, family bonding, company outings, incentives or maybe just the idea of sharing and enjoying the love for Mexican food is enough.</p>

                        <p>Whatever the reason, contact us to quote and we will accommodate you. It doesn't matter if you're a group from 11 - 100, don't hesitate to contact us.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-5 order-lg-2 col-md-6 order-md-2 benefits-list">
                <h2 class="about-us-h2"><strong>Benefits</strong></h2> <!-- style="color:#EA5B2D;display: -webkit-box;" -->
                <ul style="    -webkit-padding-start: 0px;">
                    <li><p style="margin:10px 0px"><strong>Private Group Tours</strong> are perfect for team bulding functions, corporate outings, networking events and happy hour socials.</p></li>
                    <li><p style="margin:10px 0px"><strong>Accommodate</strong> allergies, aversions and dietary restrinctions of your group.</p></li>
                    <li><p style="margin:10px 0px"><strong>Incorporate additional food and drink</strong> pairings to maximize the fun.</p></li>
                    <li><p style="margin:10px 0px"><strong>Custom start and end times</strong> outside of normal hours.</p></li>
                </ul>
<!--
                <p><strong>Private Group Tours</strong> are perfect for team bulding functions, corporate outings, networking events and happy hour socials.</p>

                <p><strong>Accommodate</strong> allergies, aversions and dietary restrinctions of your group.</p>

                <p><strong>Incorporate additional food and drink</strong> pairings to maximize the fun.</p>

                <p><strong>Custom start and end times</strong> outside of normal hours.</p>
-->
            </div>
            <br />
            <div class="col-sm-12 col-lg-12 order-lg-4 col-md-12 order-md-4 text-center" style="padding-top:20px;">
                <a href="tel:+1-800-481-4603" style="background: #7DB03D;padding: 7px 24px;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;border: 0px solid #e20808;color:white !important; text-decoration:none;" class="info-questions-priv-events">INFO &amp; QUESTIONS CALL +1 800 481 4603</a><br><p class="show-md" style="margin-top:15px;color:#7DB03D; visibility:hidden"><i>or fill this contact below:</i></p>
            </div>
            <br />
        </div>
    </div>
    <?php include('includes/footer.html'); ?>
</body>

</html>
