<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Privacy Policies | Cancun Food Tours</title>
<meta name="description" content= "Privacy policies">
<META NAME="Keywords" CONTENT="Taco tours cancun, food tours in cancun, where to eat dinner in cancun, where to eat in cancun, best places to eat in cancun, best places for foodies in Cancun.">
	<link rel="alternate" hreflang="x-default" href="https://cancunfoodtours.com/privacy">	
	<link rel="canonical" href="https://cancunfoodtours.com/privacy">
	<link rel="alternate" hreflang="en-US" href="https://cancunfoodtours.com/privacy">
	<link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/privacy">
	<link rel="stylesheet" type="text/css" href="https://cancunfoodtours.com/css/estilo.css?1.33.16101">
	<link rel="stylesheet" href="https://cancunfoodtours.com/css/flexslider.css?1.1.110" type="text/css" media="screen" />
	<script src="https://cancunfoodtours.com/js/modernizr.js"></script>
	<style type="text/css">
	body{
		font-size: 16px;
	}
		.privacy{
			float: left;
			width: 100%;
		}
		.primeroh1{
			float: left;width: 100%;
		}
		.primeroh1 h1{
			font-size: 22px;
		}
		.contfaq{
			float: left;width: 100%;
		}

		details{
    		/* border: 1px solid #CC0; */
    		padding: 7px;
    		/* font-family: Arial; */
    		margin-bottom: 5px;
    		/* width: 500px; */
    		background-color: #EEE;
		}
		summary{
		  cursor: pointer;
		}
		p, a, span{
			color: black;
    		/*padding-left: 22px;*/
    		/*margin-top: 10px;*/
		}
		details summary::-webkit-details-marker {
		  color: #CC0;
		}
	</style>
		<?php include('includes/tagmanager.php'); ?>
</head>
<body>
<?php include('includes/menu.php');  ?>
<div class="todocont" >
	<div class="contenedor" >
		<div class="privacy">
			<div class="primeroh1" style="margin-top:-2rem"><h1 style="font-size:28px">Privacy Policies</h1>
<p>Cancun Food Tours S.A de C.V undertakes to ensure the privacy of personal information obtained
through its on-line services. Cancun Food Tours S.A de C.V in Carretera a Punta Sam Manzana 2
No. 34 CP 77520 en Cancún, Quintana Roo. And our Phone number is: 9981-47-43-90 and/or our
email taco@cancunfoodtours.com for any clarification.</p><br>

<p>It is suggested to read the rules described below to understand the treatment of the data provided.
Type of Information will be compiled in different forms and from different areas of the web site
www.cancunfoodtours.com All the personal data voluntarily entered by the user in the electronic
    address will be subjected to security and privacy standard Information to be requested:</p>
                <br>
<p>
    1. Name (s) and surname.
    <br>
    2. E-mail.
    <br>
    3. Telephone.
    <br>
    4. Tax information to issue invoices.
</p>
    <br>
<p>The requested information will enable to contact customers when necessary. Users can be
contacted by telephone or e-mail in the event additional information is required to complete a
transaction.</p>
<br>
            </div>
			<div class="contfaq">
				<details>
				   <summary>Use</summary>
				   <p>The information provided during the registration process, enrollment to any program and promotions is used to conduct internal studies on demographics, interests and behavior of the users for providing products, services, content and advertising commensurate with their needs.</p>
				</details>

				<details>
				   <summary>Protection</summary>
				   <p>The security and confidentiality of the data that users provide to contract a service or buy a product on-line will be protected by a secure server under the Secure Socket Layer (SSL) protocol so that the data sent will be transmitted encrypted to ensure it remains safeguarded. To verify that you are in a protected environment make sure that an S appears in the navigation bar. Example: https://. However, and despite counting with more secure tools each day, protection of data transmitted via Internet cannot be guaranteed 100%; so that once received, we will do everything possible to safeguard the information.</p>
				</details>

								<details>
				   <summary>Cookies</summary>
				   <p>Cookies are small pieces of information that are sent by the web site to the browser. They are stored in the hard drive of computers and are used to determine the preferences of users accessing the electronic portal (www.cancunfoodtours.com) as well as to track determined behaviors or activities. The cookies allow recognizing users, detecting the bandwidth they have selected, identify the most outstanding information, calculate the size of the audience and measure certain traffic parameters.</p>
				</details>

								<details>
				   <summary>Confidentiality</summary>
				   <p>When the user is on the web site (www.cancunfoodtours.com), he will be sharing this information with Cancun Food Tours, S.A de C.V, unless otherwise specified. Cancun Food Tours S.A de C.V has no obligation to keep confidential any other information that the user provides in newsletters and on-line talks(chats), as well as through the cookies. The foregoing, pursuant to the terms set forth in article 109 of the Federal Copyright Law and article 76 bis, fraction I, of the Federal Consumer Protection Law. Information may be disseminated in special cases only, such as identifying, locating or carry out legal actions against those persons who breach the conditions of service of the web site (www.cancunfoodtours.com), cause damages or interfere with Cancun Food Tours S.A de C.V, its properties, other portal users or any person who could be affected by such actions. Cancun Food Tours S.A de C.V will NOT sell, giveaway, furnish or rent confidential information to third parties. If the user does not want his data to be shared, he may dispense with a specific service or refrain from participating in some promotions or contests. Cancun Food Tours S.A de C.V. will NOT share confidential information with third parties, unless it receives express authorizations from its subscribers or when a court order is required to comply with certain procedural regulations.</p>
				</details>

								<details>
				   <summary>Amendments to Privacy Policy</summary>
				   <p>Cancun Food Tours S.A de C.V reserves the right to perform at any time, changes in the present PRIVACY POLICIES and adapt them to legislative developments, case law, as well as market practices. It is the user’s responsibility to read the PRIVACY POLICIES regularly to be aware of such changes, which as soon as they are introduced on the web site will come into force automatically.</p>
				</details>

				<details>
				   <summary>Cancelation policies</summary>
				   <p>When contracting Cancun Food Tours S.A de C.V Cancelation policies are as follows:</p>
                    
                        <ul>
                            <li style="margin-left:3rem">Cancelling 48 hours in advance will result in a 100% refund.</li>
                            <li style="margin-left:3rem">Cancelling 28-24 hours in advance will on result in 50% refund.</li>
                            <li style="margin-left:3rem">Cancelling the day of the activity will result in a “No Show” and non-refund will be performed.</li>
                            <li style="margin-left:3rem">Reschedule has no cost if made 24 hours before your activity, a reschedule can’t be performed the same day of your activity.</li>
                        </ul>
                    
				</details>				
			</div>

            <div class="primeroh1"><div style="font-size:22px;color:black;font-weight:bold;">Aviso de privacidad</div>
                <p>Cancun Food Tours S.A de C.V, (en adelante Cancun Food Tours), estamos ubicados en Carretera a Punta Sam Manzana 2 No. 34 CP 77520 en Cancún, Quintana Roo. Nuestro teléfono siendo: 998-167-9544. El responsable de la Protección de Datos personales es Cancun Food Tours S.A de C.V y se ubica en el mismo domicilio, usted podrá contactarnos en el correo electrónico: taco@cancunfoodtours.com Sus datos personales serán tratados por Cancun Food Tours. Este Aviso de Privacidad forma parte del sitio cancunfoodtours.com.</p><br>

<p>La información solicitada al usuario es::</p>
                <br>
<p>
    1.-Nombre(s)<br />
    2.-Apellidos<br />
    3.-Dirección de correo electrónico.<br />
    4.-Número telefónico.<br />
</p>
    <br>
<p>La información deberá ser veraz y completa. El usuario responderá en todo momento por la veracidad de los datos proporcionados y en ningún caso Cancun Food Tours será responsable de la veracidad de los mismos.</p>
<br>
            </div>
			<div class="contfaq">
				<details>
				   <summary>Uso de la información</summary>
				   <p>La información solicitada permite a Cancun Food Tours cumplir con la finalidad primaria de generar la reservación correspondiente; así como para enviar boletines siempre y cuando el usuario lo deseé.</p>
				</details>

				<details>
				   <summary>Limitación de uso y divulgación de información</summary>
				   <p>En nuestro programa de notificaciones, promociones y ofertas a través del correo electrónico, solo Cancun Food Tours tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales solo serán enviados a usted y aquellos contactos registrados para tal propósito, esta indicación podrá usted modificarla en cualquier momento. En el correo electrónico taco@cancunfoodtours.com En el caso de empleo de cookies, el botón de " ayuda " se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá como evitar aceptar nuevas cookies, como hacer que el navegador le notifique cuando recibe una nueva cookie o como deshabilitar todas las cookies. </p><br />
                    <p>
                        Derechos ARCO(Acceso, Rectificación, Cancelación y Oposición) 
Los datos personales proporcionados por el usuario formarán parte de un archivo que contendrá su perfil. El usuario puede contactarse a nuestro teléfono 9981-47-43-90 para acceder o modificar su perfil en cualquier momento o enviándonos un correo a taco@cancunfoodtours.com Así mismo podrá notificarnos la cancelación de su expediente o la oposición al tratamiento de información por los mismos medios. Cancun Food Tours aconseja al usuario que actualice sus datos cada vez que éstos sufran alguna modificación, ya que esto permitirá brindarle un servicio más eficiente y personalizado. En todo momento usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales. Para ello, es necesario que presente su petición mediante un correo electrónico a taco@cancunfoodtours.com Su petición deberá ir acompañada de los fundamentos por los que solicita dicha revocación y un documento oficial de identificación personal. En un plazo máximo de 20(veinte) días hábiles atenderemos su solicitud y le informaremos sobre la procedencia de la misma a través del correo electrónico del que provenga la petición.

                    </p>
				</details>

								<details>
				   <summary>Transferencias de información con terceros</summary>
				   <p>Cancun Food Tours no realiza transferencias de información con terceros.</p>
				</details>

								<details>
				   <summary>Datos personales sensibles</summary>
				   <p>Cancun Food Tours no solicita datos personales sensibles en su sitio web.</p>
				</details>

								<details>
				   <summary>Qué son los cookies y cómo se utilizan</summary>
				   <p>Los cookies son pequeñas piezas de información que son enviadas por el sitio Web a su navegador y se almacenan en el disco duro de su equipo y se utilizan para determinar sus preferencias cuando se conecta a los servicios de nuestros sitios, así como para rastrear determinados comportamientos o actividades llevadas a cabo por usted dentro de nuestros sitios.
La utilización de cookies no será empleada para identificar a los usuarios, con excepción de los casos en que se investiguen posibles actividades fraudulentas.</p>
				</details>

                <details>
                    <summary>Confidencialidad de la información </summary>
                    <p>Cancun Food Tours no vende ni alquila la información de los usuarios.</p>
                </details>
                
                <details>
                    <summary>Protección de la información personal </summary>
                    <p>Le recomendamos que no revele su información personal a nadie. En todo momento, el usuario es el responsable único y final de los datos proporcionados en este sitio web. 
La seguridad y confidencialidad de los datos que los usuarios proporcionen al contratar un servicio en línea, estarán protegidos por un servidor seguro bajo el protocolo secure socket layer(SSL), de tal forma que los datos enviados se transmitirán encriptados para asegurar su resguardo 
Para verificar que se encuentra en un entorno protegido asegúrese de que aparezca una S en la barra de navegación. Ejemplo: https:// Sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los datos enviados a través de internet no se puede garantizar al 100%; por lo que una vez recibido, se hará todo lo posible por salvaguardar la información
</p>
                </details>
                
                <details>
                    <summary>Cambios en el aviso de privacidad </summary>
                    <p>Este aviso puede tener cambios en el futuro, de ser así se le avisará oportunamente en la sección de Aviso de Privacidad de este sitio web.</p>
                </details>
                
                <details>
                    <summary>Aceptación de los términos </summary>
                    <p>Si el usuario utiliza los servicios de este sitio, significa que ha leído, entendido y acordado los términos antes expuestos. Si no está de acuerdo con ellos, el usuario no deberá proporcionar ninguna información personal, ni utilizar los servicios de este sitio web.</p>
                </details>
                
				<details>
				   <summary>Politicas de Cancelación</summary>
				   <p>When contracting Cancun Food Tours S.A de C.V Cancelation policies are as follows:</p>
                    
                        <ul>
                            <li style="margin-left:3rem">Cancelar con 48 horas de anticipación resultara en una devolución del 100%.</li>
                            <li style="margin-left:3rem">Cancelando de 24 a 48 horas de anticipación resultara en una devolución del 50%.</li>
                            <li style="margin-left:3rem">Cancelando el día del evento resultara en un “No Show” el cual no tiene derecho a devolución.</li>
                            <li style="margin-left:3rem">Para re-agendar no hay ningún costo, y no se puede re agendar el mismo dia del tour.</li>
                        </ul>
                    
				</details>				
			</div>
            
		</div>
	</div>
</div>
	 <script src="https://cancunfoodtours.com/js/jquery.min.js"></script>
	   <script defer src="https://cancunfoodtours.com/js/jquery.flexslider.js"></script>
<?php include('includes/footer.php');  ?>
</body>
</html>