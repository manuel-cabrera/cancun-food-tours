 <link rel="stylesheet" href="https://cancunfoodtours.com/css/swiper.css">
<style type="text/css">
	.swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 15px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
</style>
<div style="float: left;width: 100%;margin-bottom: 10px;margin-top: 30px;">
	<div style="float: left;width: 100%;margin-bottom: 20px;">
		<p style="float: left;width: 100%;text-align: center;color: black;font-weight: 400;font-size: 16px;">Still not sure to book your Cancun Food Tour? See what our 8 customers are saying about us...</p>
	</div>
	<div class="swiper-container">
	    <div class="swiper-wrapper">
		      <div class="swiper-slide">

			      	<p style="float: left;    width: 883px;">
				      	<span style="float: left;width: 100%;font-weight: 600;">Valentina A</span>
						<span style="float: left;width: 100%;">"A must!"</span>
						<span  style="float: left;width: 100%; margin-bottom: 10px;">Reviewed January 26, 2018</span>
						<span style="float: left;">Highly recommended! It’s definitely a must if you want to get off the beaten track. The food is amazing! The tour is run by super friendly and knowledgeable, English-speaking, local guides who take you to authentic places around downtown Cancun. They offer prompt transportation and great service. Don’t miss out on this one!<a href="https://www.tripadvisor.com.mx/ShowUserReviews-g150807-d12301219-r556581612-Cancun_Food_Tours-Cancun_Yucatan_Peninsula.html" rel="nofollow"><img src="https://cancunfoodtours.com/img/tripadvisor.png" style="margin-left: 5px;width: 100px;"></a></span>
					</p>
				</div>
		      <div class="swiper-slide">
		      	
			      	<p style="float: left;    width: 883px;">
				      	<span style="float: left;width: 100%;font-weight: 600;">Mariana V</span>
						<span style="float: left;width: 100%;">"Mexican Foodie tour"</span>
						<span  style="float: left;width: 100%; margin-bottom: 10px;">Reviewed January 19, 2018</span>
						<span style="float: left;">Great activity to get out of the typical resort stuff. The staff were friendly and knew all about the history of the delicious local dishes we tried. Each restaurant visited was better than the last, very much off the classic tourist scene. I even got reccomendations of places to visit on my own! <br>Definiteley will reccomend to friends and family.<a href="https://www.tripadvisor.com.mx/ShowUserReviews-g150807-d12301219-r554778858-Cancun_Food_Tours-Cancun_Yucatan_Peninsula.html" rel="nofollow"><img src="https://cancunfoodtours.com/img/tripadvisor.png" style="margin-left: 5px;width: 100px;"></a></span>
					</p>
		      </div>
		      <div class="swiper-slide">
		      		<p style="float: left;    width: 883px;">
				      	<span style="float: left;width: 100%;font-weight: 600;">TU914</span>
						<span style="float: left;width: 100%;">"Awesome"</span>
						<span  style="float: left;width: 100%; margin-bottom: 10px;">Reviewed January 18, 2018</span>
						<span style="float: left;">I highly recommend Cancun Food Tours. The staff are extremely friendly and knowledgeable and the food was delicious. I’m a big foodie and wanted to learn about the diverse food scene in Mexico and the tour did just that! Not only do you get to try a number of local dishes, but you also learn about the history behind them. The highlight for me was eating marquesitas in the park!<br>If you want to step away from the typical beach activities or get a night out of the resort, definitely give these guys a try.<a href="https://www.tripadvisor.com.mx/ShowUserReviews-g150807-d12301219-r554633780-Cancun_Food_Tours-Cancun_Yucatan_Peninsula.html" rel="nofollow"><img src="https://cancunfoodtours.com/img/tripadvisor.png" style="margin-left: 5px;width: 100px;"></a></span>
					</p>
		      </div>
	    </div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>
  	</div>
</div>
<script src="https://cancunfoodtours.com/js/swiper.min.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>