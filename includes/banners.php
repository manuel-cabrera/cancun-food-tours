


<div class="todocont" >
	<div class="banner1" >
		<div id="main" role="main">
	      	      <section class="slider" style="background: #f3eedb;height: 731px;">
	        <div id="slider" class="flexslider" style="height: 314px;">
	          <ul class="slides">
	          	  	    	<li>
	  	    	    <img src="https://cancunfoodtours.com/img/banner-1.jpg" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Welcome to Cancun Food Tours</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Take a journey through Mexican Culture, Folklore, Food & Drink tradition of Cancun with one of our local food experts who will offer you a unique perspective of the city. Choose one of our experiences below.</p>
	  	    	    			<!-- <div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div> -->
	  	    	    		</div> 
	  	    	    	</div>
					</div>
	  	    	</li>
	          	  	    	<li>
	  	    	    <img src="https://cancunfoodtours.com/img/salsa.jpg" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Discover Flavors of Mexico in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 20px;letter-spacing: 0px;">North, South, Center. No Food is the same in Mexico, recipes and ingredients change per location so be ready to travel around Mexico through your palate. Explore Food & Culture in Downtown Cancun.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li>
	          	<li>
	  	    	    <img src="https://cancunfoodtours.com/img/Cclass/CClass-2-banner.jpg" style=" float: left; width: 100%;" />
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Aromas of Mexico Cooking Class in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Mexican food is by far one of the best in the world, learn the secrets of its cuisine &amp; prepare 4 course-meal representing different parts of Mexico. Learn how to choose the ingredients while visiting a local market and be ready to feast on your deliciously “out of the oven” recipes on this intimate experience.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/cooking-class-in-cancun">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li>
				<!-- <li>
	  	    	    <img src="https://cancunfoodtours.com/img/grupo1.jpg" style="  float: left; width: 100%;"/>
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Discover Flavors of Mexico in Cancun</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">Mexico’s variety of food changes per region. Nothing is the same in the North, South, and center of Mexico. If your thing is to have the opportunity to test a lot whole of Mexico on one sit just book your spot in this amazing food tour.</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li> -->
	  	    	 <!--<li>
	  	    	    <img src="https://cancunfoodtours.com/img/grupo1.jpg" style="  float: left; width: 100%;"/>
					<div style=" position: absolute;">
	  	    	    	<div class="contenedor2" style="width: 950px;margin: 0 auto;">
	  	    	    		<div style="float: right;width: 604px;background: rgba(0, 0, 0, 0.58);margin-top: 89px;margin-right: 73px;">
	  	    	    		<b style=" color: white; padding-left: 22px;  margin-bottom: 0px;  width: 92%;">Are you ready for the best Mexican Food?</b>
	  	    	    			<p style="float: left;width: 95%;background: none;color: white!important;padding: 15px;line-height: 24px;letter-spacing: 1px;">CancunFoodTours.com is your gateway to the most authentic and traditional flavors from Mexico you will be able to find in Cancun and the Riviera Maya. Allow us to show you our culture and society through good food...</p>
	  	    	    			<div class="venado"><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">LEARN MORE</a></div>
	  	    	    		</div>
	  	    	    	</div>
					</div>
	  	    	</li>-->


	          </ul>
	          <!--<div style="float:left; width:100%;">
			<p style="float: left;margin-top: 27px;font-size: 21px;width: 100%;font-weight: bold;letter-spacing: 0px;text-transform: uppercase;font-family: free;color: #cc3300;">Mexican food is not just tacos, prepare to get mind blown by influences of different cultures in mexican gastronomy.</p>
		</div>-->
	        </div>
	       
	       <div id="carousel" class="flexslider dos">
	          <ul class="slides men" >

		            <li  onclick="window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour'">
		         		
			            <div class="todo12">
			            <div class="todo13">
			           		
				  	    	    <img src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-tostada-home.jpg" />
				  	    	    <div class="tito" >
				  	    	    	<span >Flavors of Mexico</span>
				  	    	    </div>
								<script>
  									window.location = $("#mylink").attr("href");
								</script>
				  	    	    <div class="dieter">
					  	    	    <div class="tito2">
					  	    	    	<div class="contenido1" >
						  	    	    	<p>When: Wed &amp; Sat</p>
						  	    	    	<p>Time: 3.5 - 4 hrs</p>
						  	    	    	<p>Stops: 4 </p>
						  	    	    	<b>Price: $79.00 usd</b>
						  	    	    	<div class="boton" style="">
						  	    	    		<a href="#"  onClick="window.location.href='https://cancunfoodtours.com/flavors-of-mexico-food-tour' "> BOOK NOW</a>
						  	    	    	</div>
						  	    	    </div>
					  	    	    </div>
				  	    	    </div>
				  	    	
			  	    	</div>
						</div>

		  	    	</li>

		  	    	<li onClick="window.location.href='https://cancunfoodtours.com/taco-tour-in-cancun'">
		  	    	 <div class="todo12">
		  	    	 <div class="todo13">
		  	    	    	<img src="https://cancunfoodtours.com/img/Tacotour/taco-tour-pastor-home.jpg" />
			  	    	    <div class="tito" >
			  	    	    	<span>Taco Tour Local</span>
			  	    	    </div>
			  	    	    <div class="dieter">
			  	    	    <div class="tito2">
			  	    	    	<div class="contenido1" >
				  	    	    	<p>When: Tue &amp; Fri</p>
				  	    	    	<p>Time: 3.5 - 4 hrs</p>
				  	    	    	<p>Stops: 5 </p>
				  	    	    	<b>Price: $60.00 usd</b>
				  	    	    	<div class="boton">
				  	    	    		<a href="#" onClick="window.location.href='https://cancunfoodtours.com/taco-tour-in-cancun'"> BOOK NOW</a>
				  	    	    	</div>
				  	    	    </div>
			  	    	    </div>
			  	    	    </div>
		  	    	    </div>
		  	    	    </div>
		  	    	</li>

		  	    	<li>
		  	    	 <div class="todo12">
		  	    	 <div class="todo13">
			  	    	    <img src="https://cancunfoodtours.com/img/Cclass/CClass-2-home.jpg" />
			  	    	    <div class="tito" >
			  	    	    	<span>Cooking Class</span>
			  	    	    </div>
			  	    	    <div class="dieter">
			  	    	    <div class="tito2">
			  	    	    	<div class="contenido1" >
				  	    	    	<p>When: Tue - Thurs</p>
				  	    	    	<p>Time: 4.5 - 6 hrs</p>
				  	    	    	<p>Stops: 3 </p>
				  	    	    	<b>Price: $99.00 usd</b>
				  	    	    	<div class="boton" >
				  	    	    		<a href="#" onclick="window.location.href='https://cancunfoodtours.com/cooking-class-in-cancun'" > BOOK NOW</a>
				  	    	    	</div>
				  	    	    </div>
				  	    	</div>
			  	    	    </div>
			  	    	    </div>
		  	    	    </div>
		  	    	</li>
		  	    	<!--<li>
			  	    	<div class="todo12">
			  	    	<div class="todo13">
				  	    	    <img src="https://cancunfoodtours.com/img/mexico-2.jpg" />
				  	    	     <div class="tito" >
				  	    	    	<span>COMING SOON</span>
				  	    	    </div>
				  	    	    <div class="dieter">
				  	    	    <div class="tito2">
				  	    	    	<div class="contenido1" >
					  	    	    	<p>When: Everyday</p>
					  	    	    	<p>Time: 2h</p>
					  	    	    	<p>Stops: 3 </p>
					  	    	    	<b>Price: $60.00 usd</b>
					  	    	    	<div class="boton" >
					  	    	    		<a href="" style="    background: #999999;"> BOOK NOW</a>
					  	    	    	</div>
					  	    	    </div>
				  	    	    </div>
				  	    	    </div>
			  	    	   </div>
			  	    	   </div>
		  	    	</li>-->
		       
	          </ul>
	        </div>

	      </section>
	   </div>
</div>