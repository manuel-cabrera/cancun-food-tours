<script type="text/javascript">    
	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};

	if (isMobile.any()) {

		var url = 'https://m.cancunfoodtours.com';

		window.location.replace(url);

	}


$(document).ready(
    
    function () {
    $("ul.top li").hover(function () { //When trigger is hovered...
        $(this).children("ul.sub").slideDown('fast').show();
    }, function () {
        $(this).children("ul.sub").slideUp('slow');
    });
        
        var landings =  ["flavors-of-mexico", "cooking-class", "taco-tour-in-cancun"];
        for (i=0;i<landings.length;i++){
            if(window.location.pathname.includes(landings[i])){ 
               $("#lnkbuytickets").hide();
                break;
            }
        }
    }
);
    
    

</script>

	<style type="text/css">
		.noimporta{float: left;width: 37px;margin-top: -3px;}
		.noimporta1{float: left;width: 37px;margin-top: -3px;}
		.noimporta2{float: left;width: 37px;margin-top: -3px;}
		.sub, .subsub {
		    display:none;
		}
	</style>
	<div class="menu1" >
		<nav>
			<ul class="arriba" style="border-bottom: 2px solid #666666;" >
				<li style=" margin-bottom: 7px;"><a href="https://cancunfoodtours.com/" style="padding: 1px 25px;margin-top: 7px;"> <img src="https://cancunfoodtours.com/img/cancunfoot.png" style="float: left;width: 185px;"></a></li>
				
				<!--<li style="margin-left: 101px; margin-top: 30px;"><a href="#" style="padding: 4px 25px !important;"></li>-->
				<!--<li style=" margin-top: 16px;  margin-left: 142px;"><a href="#" style=" background: #f1592a;background: #f15c2d; color: white; padding: 7px 24px;"><span>BOOK NOW</span></a></li>-->
				<!--<li style="margin-top: 28px;margin-left: 156px;"><a href="#" style="background: #f1592a;background: #f15c2d;color: white;padding: 7px 24px;box-shadow: 1px 1px 1px 1px saddlebrown;"><i class="fa fa-tag"></i><span style=" font-size: 20px;">BOOK NOW</span></a></li>-->
				<!-- <div class="boton"> -->
				<li style="margin-top: 16px;margin-left: 89px;"><a id="lnkbuytickets" data-toggle="modal" href="#example" class="btn" style="background:#cc3300;color: white;padding: 7px 24px;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;border: 0px solid #e20808;">BUY TICKETS</a></li>
				<!-- </div> -->
				<!--<li style="margin-top: 5px;width: 40%;margin-left: 328px;"><a href="#" style="padding: 4px 0px !important;font-size: 31px;">Toll Free USA/Canada: 1-800-818-9821 </a><a href="#" style="    padding: 1px 45px !important;"><img class="noimporta2" src="https://cancunfoodtours.com/img/tres3.jpg">When in Mexico (998)-887-4242</a></li>-->
				<li style="margin-top: 5px;width: 46%;margin-left: 1px;margin-bottom:  5px;float: right;"><a href="#" style="padding: 4px 0px !important;font-size: 26px;float: right;"><img class="noimporta2" src="https://cancunfoodtours.com/img/tres3.jpg"><span style="float: left;font-size: 16px;margin-top: 3px;">Toll Free USA/Canada:</span><span style="float: left;margin-left:  4px;font-weight: bold;"> 1-800-481-4603</span></a><a href="#" style="padding: 1px 0px !important;float: right;"><img class="noimporta2" src="https://cancunfoodtours.com/img/tres3.jpg"><span style="float: left;font-size: 15px;margin-top: 3px;">When in Mexico:</span><span style="float: left;margin-left: 4px;font-weight: bold;"> 9981-47-43-90</span></a></li>
				<!--<li style="margin-top: 19px;margin-left: 77px;"><a href="#" style="padding: 4px 25px !important;"><img class="noimporta2" src="img/tres3.jpg">When in Mexico (998)-887-4242</a></li>-->
			</ul>
			<ul style="border-bottom: 2px solid #999999;" id="menu">				
				<li><a href="#">Food Tours</a>
				  <ul>
				    <li><a href="https://cancunfoodtours.com/flavors-of-mexico-food-tour">Flavors of Mexico</a></li>
				    <li><a href="https://cancunfoodtours.com/taco-tour-in-cancun">Taco Tour Local</a></li>
                      <li><a href="https://cancunfoodtours.com/cooking-class-in-cancun">Cooking Class in Cancun</a></li>
				  </ul>
				 </li>
				<li><a href="https://cancunfoodtours.com/about-us">About us</a></li>
				<li><a data-toggle="modal" href="https://cancunfoodtours.com/cancun-food-tours-private-events" class="btn" style="color: black;border-radius: 3px 3px 3px 3px;-moz-border-radius: 3px 3px 3px 3px;-webkit-border-radius: 3px 3px 3px 3px;border: 0px solid #e20808;">Private Groups</a></li>
				<!--<li>
					<a href="#" class="top">TOURS</a>
					<ul class="sub">
						<li>
							<a href="#">TOURS 1</a>
						</li>
					</ul>
				</li>-->
				<li><a href="https://cancunfoodtours.com/faq">F.A.Q</a></li>
				<li><a href="https://cancunfoodtours.com/contact-us">Contact Us</a></li>
				<!--<li>
					<a href="#" style="padding: 0px 0px !important;font-size: 23px;">Toll Free USA/Canada: 1-800-481-4603 </a>
					<a href="#" style="padding: 0px 54px !important;">When in Mexico: 9981-47-43-90</a>
				</li>-->
			</ul>
		</nav>	
	</div>
	
	
	