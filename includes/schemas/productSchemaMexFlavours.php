<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "additionalType": "TouristAttraction",
  "name": "Flavors of Mexico Cancun Food Tour",
  "image": "https://cancunfoodtours.com/images/flavors-of-mexico-card-mobile.jpg",
  "description": "Taste 7 traditional dishes visiting 4 different local spots.",
  "mpn": "1",
  "aggregateRating": {
    "@type": "AggregateRating",
    "bestRating":"5",
    "worstRating":"1",
    "ratingValue": "5",
    "reviewCount": "8"
  },
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "price": "79.00",
    "priceCurrency": "USD"
}
}
</script>