<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "additionalType": "TouristAttraction",
  "name": "Aromas of Mexico Cooking Class in Cancun",
  "image": "https://cancunfoodtours.com/images/cooking-class-card-mobile.jpg",
  "description": "oin a small group of people in our Cancun Cooking Class and learn the secrets of Mexican Cuisine at one of the best Cancun Restaurants.",
  "mpn": "3",
  "aggregateRating": {
    "@type": "AggregateRating",
    "bestRating":"5",
    "worstRating":"1",
    "ratingValue": "5",
    "reviewCount": "8"
  },
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "price": "99.00",
    "priceCurrency": "USD"
}
}
</script>