<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "CancunFoodTours.",
  "url": "https://cancunfoodtours.com",
  "logo":"https://cancunfoodtours.com/img/cancunfoot.png",
  "telephone":"+1-800-481-4603",
  "address":{
    "@type":"PostalAddress",
    "streetAddress":"Carretera a Punta Sam",
    "addressLocality":"Benito Juarez",
    "postalCode":"77520",
    "addressCountry":{
        "@type":"Country",
        "name":"Mexico"
    }
  },
  "contactPoint": [
      {
        "@type": "ContactPoint",
        "telephone": "+1-800-481-4603",
        "contactType": "Customer service",
        "contactOption":"TollFree",
        "email":"taco@cancunfoodtours.com",
        "areaServed":"US",
        "availableLanguage":[
            {
                "@type":"Language",
                "name":"English"
            },{
                "@type":"Language",
                "name":"Spanish"
            }
        ]
      },
      {
        "@type": "ContactPoint",
        "telephone": "+52998-147-4390",
        "contactType": "Customer service",
        "contactOption":"TollFree",
        "email":"taco@cancunfoodtours.com",
        "areaServed":"MX",
        "availableLanguage":[
            {
                "@type":"Language",
                "name":"English"
            },{
                "@type":"Language",
                "name":"Spanish"
            }
            ]
    }
    ]
  }
</script>