<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "additionalType": "TouristAttraction",
  "name": "The Most Authentic Taco Tour in Cancun",
  "image": "https://cancunfoodtours.com/images/taco-tour-card-mobile.jpg",
  "description": "Savour a variety of tacos at 5 different local gems!.",
  "mpn": "2",
  "aggregateRating": {
    "@type": "AggregateRating",
    "bestRating":"5",
    "worstRating":"1",
    "ratingValue": "5",
    "reviewCount": "8"
  },
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "price": "60.00",
    "priceCurrency": "USD"
}
}
</script>