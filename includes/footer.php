<script src="https://cancunfoodtours.com/js/modal.js"></script>
<style type="text/css">
    a{
        color:#428bca;
        text-decoration: none;
    }
	    .booking_error{
    	float: left;
    	font-size: 19px;
    	color: red;
    }
</style>
<div id="example" class="modal hide fade in" style="display: none;">
    <div class="modal-header">
        <h3>Tours</h3>
     </div>
     <div class="modal-body">
	     <div style="float:left;width:100%; border-top:1px solid black;">
	     	<div style="float:left;width:100%;margin-top:10px;">
		     	<div style=" float: left; width: 141px;"><img src="https://cancunfoodtours.com/img/Flavors/flavors-of-mexico-tostada-buy-tickets.jpg" style="float: left; width: 100%;"></div>
			<div style="float:left;width: 515px;margin-left: 5px;">
		        	 <h4 style=" float: left; width: 100%; color: black;  font-size: 18px;">Flavors of Mexico Premium Experience</h4>
			        <p style="float: left; font-size: 14px;font-family: 'Roboto', sans-serif;">We will take you through Cancun’s Local dining scene for you to experience how Mexicans really eat. You will have the opportunity to try unique Mexican experiences on each bite. Our tour is designed for you to get to know the history and background behind each dish & drink that people from Cancun are used to eating…</p> 
			       <a href="https://cancunfoodtours.com/bookings/1" style="float: right;text-decoration: none;"><span style="background: #f15c2d;padding: 9px 28px;color: white;text-decoration: none; border-radius: 3px 3px 3px 3px; -moz-border-radius: 3px 3px 3px 3px; -webkit-border-radius: 3px 3px 3px 3px;">BOOK NOW</span></a>             
		        </div>
		     </div>   
 	     	<div style="float:left;width:100%;margin-top:10px;">
	     	<div style=" float: left; width: 141px;"><img src="https://cancunfoodtours.com/img/Tacotour/taco-tour-pastor-buy-tickets.jpg" style="float: left; width: 100%;"></div>
			<div style="float:left;width: 515px;margin-left: 5px;">
	        	 <h4 style=" float: left; width: 100%; color: black;  font-size: 18px;">Taco Tour in Cancun</h4>
		        <p style="float: left; font-size: 14px;font-family: 'Roboto', sans-serif;">Forget about hard shells, cheddar cheese and sour cream. Deep dive into what has definitely shaped one of the most iconic dishes from Mexico, “The Taco”. Get to know why the tortillas pair up with almost everything when trying the best tacos from the best taco joints.</p> 
		       <a href="https://cancunfoodtours.com/bookings/2" style="float: right;text-decoration: none;"><span style="background: #f15c2d;padding: 9px 28px;color: white;text-decoration: none; border-radius: 3px 3px 3px 3px; -moz-border-radius: 3px 3px 3px 3px; -webkit-border-radius: 3px 3px 3px 3px;">BOOK NOW</span></a>             
	        </div>
	     </div> 
             <div style="float:left;width:100%;margin-top:10px;">
	     	<div style=" float: left; width: 141px;"><img src="https://cancunfoodtours.com/img/Cclass/CClass-2-buy.jpg" style="float: left; width: 100%;"></div>
			<div style="float:left;width: 515px;margin-left: 5px;">
	        	 <h4 style=" float: left; width: 100%; color: black;  font-size: 18px;">Cooking Class</h4>
		        <p style="float: left; font-size: 14px;font-family: 'Roboto', sans-serif;">Learn everything from how to choose the ingredients and preparing a 4 course-meal representing different parts of Mexico. After your class delight on your own prepared "out of the oven" recipes on this intimate experience.</p> 
		       <a href="https://cancunfoodtours.com/bookings/3" style="float: right;text-decoration: none;"><span style="background: #f15c2d;padding: 9px 28px;color: white;text-decoration: none; border-radius: 3px 3px 3px 3px; -moz-border-radius: 3px 3px 3px 3px; -webkit-border-radius: 3px 3px 3px 3px;">BOOK NOW</span></a>             
	        </div>
	     </div> 
	     </div>
	     
    </div>
    <div class="modal-footer">
        <a href="#" data-dismiss="modal" class="btn">Close</a>
    </div>
</div>


<div class="footert1" style="border-top: 1px solid white;" >
	<div class="trios" >
		<div class="trioscr" >
				<div style="float:left;width: 100%;">
					<p style="padding: 15px 2px;">Cancun Food Tours © All Rights Reserved 2018</p>
				</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function enviaremail(){

		var emailsend = $('#correo').val();
		
		if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(emailsend)){

			if (emailsend == '') {
				$("#errorcorreo").fadeIn("slow").delay(2000).fadeOut('slow');
	            $('#errorcorreo').text('empty email').addClass('booking_error'); 
	            return false;
	         }else{

				$('#espere').html('<p style="float:left;">wait a moment please...</p>');

		        $.post("https://cancunfoodtours.com/includes/notificacionemail.php", {"emailsend":emailsend }, function(data){ $('#espere').html('<p style="font-size: 22px;color: #ff6633;">'+data+'</p>'); });
			}
		} else {
				$("#errorcorreo").fadeIn("slow").delay(2000).fadeOut('slow');
	            $('#errorcorreo').text('Email Incorrect').addClass('booking_error'); 
  		}
	}

</script>