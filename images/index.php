<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cancun Food Tour | Cancunfoodtour.com</title>
	<meta name="description" content= "Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun">
	
	<META NAME="Keywords" CONTENT="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun">
					
	<link rel="alternate" hreflang="x-default" href="http://cancunfoodtours.com/">	
	<link rel="canonical" href="http://cancunfoodtours.com/">
	<link rel="alternate" hreflang="en-US" href="http://cancunfoodtours.com/">

	<style type="text/css">
	html{
		font-size: 16px;
		 font-family: arial;
		 background: white;

	}
	*{
		 margin: 0px;
		 padding: 0px;
	}
		.banner1 {
    		width: 960px;
    		margin: 0 auto;
		}
		.banner2{
			float: left;
			width: 100%;
			margin-bottom: 10px;
		}
		.banner2 img{
		    float: left;
		    width: 100%;
		    height: 290px;
		}
		.form-cont input {
		    float: left;
		    width: 100%;
		    height: 30px;
		    margin-bottom: 11px;
		    border-radius: 4px;
		    -moz-border-radius: 4px;
		    -webkit-border-radius: 4px;
		    border: 1px solid #345a98;
		}
		.form-cont textarea {
		    width: 100%;
		    height: 100px;
		    margin-bottom: 14px;
		    border-radius: 4px;
		    -moz-border-radius: 4px;
		    -webkit-border-radius: 4px;
		    border: 1px solid #345a98;
		}
		.conter{
			float: left;
			width: 100%;
			margin-bottom: 10px;
					}
		.conter h1{
			    font-size: 26px;
    margin-bottom: 9px;
		}
		 .yhn{
		 	float: left;
    		width: 100%;
		}
		.yhn h2{
			float: left;
			width: 100%;
			margin-bottom: 10px;
		}
		
	</style>
</head>
<body>
<div class="banner1">
	<div class="banner2" >
		<img src="http://cancunfoodtours.com/img/ruta2.jpg">
	</div>
	<div class="conter">
	<h1>Welcome to Cancun Food Tours</h1>
		<p>Welcome to Cancunfoodtour.com, the home to discover one of the most incredible mexican culinary experiences in Cancun. Cancunfoodtour.com provides you with a selection of food tasting tours which has become one of the best things to do in Cancun. Made for foodies & food enthusiasts (or everyone, really) that are looking to indulge in mexican flavors and experiences <br><br>Wonderful Cancun dining is Mexico's gift to the world, and Mexicans take cooking very seriously, our goal is to provide Cancun’s local cuisine as part of your travel experience as an insider, and to explore food safely and with convenience. Our restaurants have the highest standards of quality and service so you’re guaranteed the best dining experience in Cancun.</p>
	</div>
	<div class="yhn">
	<h2>Why eat with Cancunfoodtours.com?</h2>
		<form action="http://cancunfoodtours.com/mailer.php" method="POST" id="contactus" class="form-cont">
			<span class="title"></span>
			<input type="text" name="nombre" placeholder="Name" title="Introduce tu nombre porfavor" maxlength="50" required="">
			<input type="email" name="email" placeholder="Email" title="Introduce tu correo electronico" required="">
			<textarea name="mensaje" placeholder="Write your message" title="Introduce tu mensaje" required=""></textarea>
			<input class="submit" type="submit" id="submit" name="submit" value="Enviar">
		</form>
	</div>
</div>
</body>
</html>