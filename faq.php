<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home of the Official Cancun Food Tours | CancunFoodTours.com</title>
    <meta name="description" content="Indulge in authentic mexican food in Cancun, make local cuisine a part of your travel experience as an insider. Try the best restaurants in Cancun. ">
    <meta name="Keywords" content="cancun food tours, dining experiences in cancun, eating experiences in cancun, what to eat in cancun, where to eat in cancun, cancun best restaurants, best places to eat in cancun, best mexican restaurants in cancun, best local restaurants in cancun, gastronomic tours in cancun.">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css?4.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css">
    <link rel="canonical" href="https://cancunfoodtours.com/faq">
    <link rel="alternate" hreflang="en" href="https://cancunfoodtours.com/faq">
    <script src="./js/bootstrap.min.js?4.0"></script>
    <link rel="stylesheet" type="text/css" href="./css/extra.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />	
    <?php include('includes/tagmanager.php'); ?>
</head>

<body>
    <?php include('includes/menu.html');  ?>
    <div class="container text-center body">
        <!--         <br />-->
        <h1><strong>Frecuently Asked Questions</strong></h1>
        <!--class="title-desk"-->
        <br />
        <!--            <section accordion>-->
        <div id="accordion" class="accordion">
            <div>
                <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                    <a class="card-title">
                    1. Is transportation included in the price of the tour?
                </a>
                </div>
                <div id="collapseOne" class="card-body collapse" data-parent="#accordion">
                    <p>Yes, all our food tours include pick up/drop off from your hotel to the different restaurants that will be included in your tour. We provide private vans with A/C, they’re comfortable enough for the whole party, and are driven by a certified driver.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    <a class="card-title">
                    2. Can I cancel/reschedule my food tour?
                </a>
                </div>
                <div id="collapseTwo" class="card-body collapse" data-parent="#accordion">
                    <p>We allow to schedule changes only if <a href="https://cancunfoodtours.com">Cancunfoodtours.com</a> is provided with a 24-hour notice before the date of your tour. For full refund cancellations, we require <a href="https://cancunfoodtours.com">Cancunfoodtours.com</a> to be notified 7 days in advance. If the tour is cancelled before a 24-hour notice, only a 50% refund will take place. Unfortunately, if the tour is cancelled after that 24-hour mark, we can’t provide a refund.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    <a class="card-title">
                  3. Is alcohol served during any of the Cancun Food Tours?
                </a>
                </div>
                <div id="collapseThree" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Yes, alcohol is part of Mexican gastronomy and we feel it should be considered as part of the experience. However, for underage and people who do not wish to drink alcohol, we have non-alcoholic options on all the beverages. Additionally, water is served throughout all our tours.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                    <a class="card-title">
                    4. Are kids allowed to go to the tours?
                </a>
                </div>
                <div id="collapseFour" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Our tours are mostly made for adults 18+, kids 10+ and we strongly recommend to respect the restriction since we want the tour to be pleasant to all our guests.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                    <a class="card-title">
                    5. Are some of the plates spicy?
                </a>
                </div>
                <div id="collapseFive" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Mexican food is full of flavor and some of our plates are made with some spices that might be considered strong. However, our chefs have prepared less-mild versions of each plate to avoid any unpalatable flavors that can be enjoyable for everyone without any risk.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                    <a class="card-title">
                    6. Are there any vegan options?
                </a>
                </div>
                <div id="collapseSix" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Yes, when booking your tour, please select how many vegan menus you would like to book so the plates are considered in the mix.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                    <a class="card-title">
                    7. What allergies and preferences are to be considered in the cancun food tours?
                </a>
                </div>
                <div id="collapseSeven" class="card-body collapse" data-parent="#accordion">
                    <p>
                        We can provide substitutions for vegetarians &amp; spicy food considerations as we’ve explained. However, we cannot provide substitutions for any other allergies or dietary preferences. Allergies and preferences must be indicated at the time of ticket purchase. Please notify us of any serious allergies so we can tell you which food(s) to avoid.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                    <a class="card-title">
                    8. Are the restaurants within Walking Distance from each other?
                </a>
                </div>
                <div id="collapseEight" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Some of the restaurants are between a 15-min walking distance of each other. Which is why we providing the transportation for the entirety of the tour. This will make the experience more comfortable.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                    <a class="card-title">
                    9. What are the current payment methods?
                </a>
                </div>
                <div id="collapseNine" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Currently we accept Paypal payments, money requests, and credit cards
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                    <a class="card-title">
                    10. How much food is provided in the tour?
                </a>
                </div>
                <div id="collapseTen" class="card-body collapse" data-parent="#accordion">
                    <p>
                        All our tours are designed and tested to provide a complete meal for an average adult. Most participants will feel full after our combination of drinks &amp; food tastings along with breaks and introductions from our tour guide.
                    </p>
                </div>
                <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
                    <a class="card-title">
                    11. Are cameras allowed on the tour?
                </a>
                </div>
                <div id="collapseEleven" class="card-body collapse" data-parent="#accordion">
                    <p>
                        Yes, cameras are allowed on the tour. We encourage foodies to share the experience and take photos during our tour. We would love you sharing the experience on networks as TripAdvisor, Facebook or Yelp.
                    </p>
                </div>

            </div>
        </div>
        <!--            </section>-->

    </div>
    <?php include('includes/footer.html'); ?>
</body>

</html>
